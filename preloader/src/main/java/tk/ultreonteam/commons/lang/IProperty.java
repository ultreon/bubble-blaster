package tk.ultreonteam.commons.lang;

public interface IProperty<T> {
    T get();

    void set(T value);
}
