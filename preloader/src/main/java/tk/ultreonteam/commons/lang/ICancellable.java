package tk.ultreonteam.commons.lang;

public interface ICancellable {
    void cancel();

    boolean isCancelled();
}
