package tk.ultreonteam.commons.lang;

public interface Logger {
    void log(String message);
}
