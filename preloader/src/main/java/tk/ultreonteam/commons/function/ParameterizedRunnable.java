package tk.ultreonteam.commons.function;

@FunctionalInterface
public interface ParameterizedRunnable<T> {
    void run(T t);
}
