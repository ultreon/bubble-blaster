package tk.ultreonteam.commons.function;

@Deprecated
@FunctionalInterface
public interface EventHandler<T> {
    void run(T evt);
}
