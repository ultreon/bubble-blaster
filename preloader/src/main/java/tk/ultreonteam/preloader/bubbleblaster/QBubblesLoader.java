package tk.ultreonteam.preloader.bubbleblaster;

import tk.ultreonteam.preloader.IGameLoader;

@SuppressWarnings("unused")
public class QBubblesLoader implements IGameLoader {
    @Override
    public String getLoadingTarget() {
        return "tk.ultreonteam.bubbles.Main";
    }

}
