# [Bubble Blaster: Java Edition](https://github.com/Ultreon/bubble-blaster) by [Ultreon Team](https://ultreonteam.tk/)

[Bubble Blaster: Java Edition](https://github.com/Ultreon/bubble-blaster) is a completely revamped version of the original [Bubble Blaster](https://github.com/Qboi123/Qplay-BubblesOLD). Starting in the [Python](https://www.python.org/)  
programming languageOld with almost no knowledge of coding, but now completely rewritten for the third time: I present to
you: [Bubble Blaster: Java Edition](https://github.com/Ultreon/bubble-blaster), now written in [Java](https://www.oracle.com/java/).

## Detailed Information
The first project [I've](https://github.com/Qboi123) ever made. A game where you need to pop bubbles for getting points, and getting higher levels
how further you go, each bubble has its own functionality and can act different by getting different points per bubble.
The bubbles have its own properties like: attack damage; defense; score; effect given, and more. Originally made in
[Python](https://www.python.org/), first made with [tkinter](https://docs.python.org/3/library/tkinter.html), then using [pyglet](http://pyglet.org/). And now made in [Java](https://www.oracle.com/java/) with the [Java AWT](https://en.wikipedia.org/wiki/Abstract_Window_Toolkit) api.

## Making a mod
*Coming Soon*
