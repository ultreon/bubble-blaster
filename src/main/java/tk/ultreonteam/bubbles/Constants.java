package tk.ultreonteam.bubbles;

import com.google.gson.Gson;

/**
 * The game constants.
 *
 * @author Qboi
 * @since 0.1.0
 */
public class Constants {
    public static final Gson GSON = new Gson();

    public static final double BUBBLE_SCORE_REDUCTION = 16.0;
    public static final int LEVEL_THRESHOLD = 50_000;
    public static final double BUBBLE_SPEED_MODIFIER = 1.0;
}
