package tk.ultreonteam.bubbles.mod;

import tk.ultreonteam.bubbles.common.interfaces.NamespaceHolder;
import tk.ultreonteam.bubbles.event.bus.ModEvents;
import tk.ultreonteam.bubbles.mod.loader.ModManager;
import org.checkerframework.checker.nullness.qual.NonNull;


@SuppressWarnings("unused")
public class ModObject implements NamespaceHolder {
    private final Class<?> modClass;
    private final Mod annotation;
    private String namespace;
    private Object instance;
    private final ModEvents eventBus;
    private final ModContainer container;

    public ModObject(String namespace, ModContainer container, Mod annotation, Class<?> clazz) {
        this.annotation = annotation;
        this.setNamespace(namespace);
        this.modClass = clazz;
        this.eventBus = new ModEvents(this);
        this.container = container;
    }

    @Override
    public final String getId() {
        return namespace;
    }

    @Override
    public final void setNamespace(String namespace) {
        if (this.namespace == null) {
            this.namespace = namespace;
        } else {
            throw new IllegalStateException("String can only set once.");
        }
    }

    public Class<?> getModClass() {
        return modClass;
    }

    public Mod getAnnotation() {
        return annotation;
    }

    @NonNull
    public ModEvents getEventBus() {
        return eventBus;
    }

    @SuppressWarnings("unchecked")
    public Object getMod() {
        return ModManager.instance().getModInstance(namespace);
    }

    public ModContainer getContainer() {
        return container;
    }
}
