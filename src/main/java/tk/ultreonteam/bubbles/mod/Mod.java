package tk.ultreonteam.bubbles.mod;

import tk.ultreonteam.bubbles.event.bus.EventManagers;
import tk.ultreonteam.bubbles.event.bus.GameEvents;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Mod {
    String modId();

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface AutoEventSubscriber {
        String modId() default "";

        Bus bus() default Bus.BUBBLE_BLASTER;

        enum Bus {
            /**
             * The main Forge Event Bus.
             *
             * @see GameEvents#get()
             */
            BUBBLE_BLASTER(),

            /**
             * The mod specific Event bus.
             *
             * @see EventManagers#modEvents(String)
             */
            MOD()
        }
    }
}
