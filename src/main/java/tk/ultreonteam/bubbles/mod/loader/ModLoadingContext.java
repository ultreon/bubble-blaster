package tk.ultreonteam.bubbles.mod.loader;

import tk.ultreonteam.bubbles.event.bus.ModEvents;
import tk.ultreonteam.bubbles.mod.ModContainer;

@SuppressWarnings("ClassCanBeRecord")
public class ModLoadingContext {
    static ModLoadingContext value;

    public static ModLoadingContext get() {
        return value;
    }

    private final ModEvents modEvents;
    private final ModContainer container;

    public ModLoadingContext(ModEvents modEvents, ModContainer container) {
        this.modEvents = modEvents;
        this.container = container;
    }

    public ModEvents getEvents() {
        return modEvents;
    }

    public ModContainer getContainer() {
        return container;
    }
}
