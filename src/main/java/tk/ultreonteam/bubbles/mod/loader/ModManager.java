package tk.ultreonteam.bubbles.mod.loader;

import tk.ultreonteam.bubbles.mod.ModObject;
import tk.ultreonteam.bubbles.core.AntiMod;
import tk.ultreonteam.bubbles.mod.ModContainer;
import tk.ultreonteam.commons.map.OrderedHashMap;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.*;

@SuppressWarnings("unused")
@AntiMod
public class ModManager {
    private static final ModManager instance = new ModManager();

    private static final List<ModContainer> containers = new ArrayList<>();
    private static final List<String> modIds = new ArrayList<>();

    private static final OrderedHashMap<String, ModObject> modObjects = new OrderedHashMap<>();
    private static final OrderedHashMap<String, Object> mods = new OrderedHashMap<>();
    private final Map<Object, ModContainer> instance2container = new HashMap<>();

    private ModManager() {

    }

    public static ModManager instance() {
        return instance;
    }

    @Nullable
    public ModContainer getContainerFromId(String modId) {
        for (ModContainer container : containers) {
            if (container.getModId().equals(modId)) {
                return container;
            }
        }
        return null;
    }

    static void registerContainer(ModContainer container) {
        if (modIds.contains(container.getModId())) {
            throw new IllegalArgumentException("Mod id already used in other mod: " + container.getModId());
        }
        modIds.add(container.getModId());
        modObjects.put(container.getModId(), container.getModObject());
        containers.add(container);
    }

    public List<ModContainer> getContainers() {
        return containers;
    }

    public ModObject getModObject(String id) {
        if (!modObjects.containsKey(id)) {
            return null;
        }

        return modObjects.get(id);
    }

    public Object getModInstance(String id) {
        if (!mods.containsKey(id)) {
            return null;
        }

        return mods.get(id);
    }

    public void registerModObject(ModObject modObject) {
        modObjects.put(modObject.getId(), modObject);
    }

    public void registerMod(Object mod, ModObject object) {
        mods.put(object.getId(), mod);
        instance2container.put(object, object.getContainer());
    }

    public @NonNull
    Collection<ModObject> getModObjects() {
        return modObjects.values();
    }

    public @NonNull
    Collection<Object> getMods() {
        return mods.values();
    }

    public List<String> getModIds() {
        return modIds;
    }

    public ModContainer getContainerFromInstance(Object instance) {
        return instance2container.get(instance);
    }
}
