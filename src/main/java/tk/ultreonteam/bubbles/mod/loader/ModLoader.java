package tk.ultreonteam.bubbles.mod.loader;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.References;
import tk.ultreonteam.bubbles.mod.Mod;
import tk.ultreonteam.bubbles.mod.ModObject;
import tk.ultreonteam.bubbles.common.text.translation.LanguageManager;
import tk.ultreonteam.bubbles.core.AntiMod;
import tk.ultreonteam.bubbles.core.InternalClassLoader;
import tk.ultreonteam.bubbles.core.ModClassLoader;
import tk.ultreonteam.bubbles.event.bus.ModEvents;
import tk.ultreonteam.bubbles.event.load.ModSetupEvent;
import tk.ultreonteam.bubbles.mod.ModContainer;
import tk.ultreonteam.bubbles.mod.ModInformation;
import tk.ultreonteam.bubbles.LoadingGui;
import tk.ultreonteam.commons.crash.CrashCategory;
import tk.ultreonteam.commons.crash.CrashLog;
import tk.ultreonteam.dev.GameDevMain;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@AntiMod
@SuppressWarnings("unused")
public class ModLoader {
    public static final File MODS_DIR = References.MODS_DIR;
    private final ArrayList<Object> mods = new ArrayList<>();
    private final LoadingGui loadingGui;
    private static final Logger LOGGER = LogManager.getLogger("Mod-Loader");
    private final HashMap<File, tk.ultreonteam.bubbles.mod.loader.Scanner.Result> scanResults = new HashMap<>();
    private final Map<String, ModEvents> modEvents = new HashMap<>();
    private final Set<String> missing = new HashSet<>();
//    private final List<ModContainer> containers = new ArrayList<>();

    @SuppressWarnings("resource")
    public ModLoader(LoadingGui loadingGui) {
        this.loadingGui = loadingGui;

        if (!MODS_DIR.exists()) {
            boolean flag = MODS_DIR.mkdirs();
            if (!flag) {
                throw new IllegalStateException("Mods directory wasn't created. (" + MODS_DIR.getPath() + ")");
            }
        } else if (MODS_DIR.isFile()) {
            boolean flag1 = MODS_DIR.delete();
            boolean flag2 = MODS_DIR.mkdir();

            if (!flag1) {
                throw new IllegalStateException("Mods directory wasn't deleted. (" + MODS_DIR.getPath() + ")");
            }
            if (!flag2) {
                throw new IllegalStateException("Mods directory wasn't created. (" + MODS_DIR.getPath() + ")");
            }
        }

        InternalClassLoader.get().addMod(BubbleBlaster.CODE_SOURCE_FILE);
        loadJar(BubbleBlaster.CODE_SOURCE_FILE.getAbsolutePath(), loadingGui);

        if (GameDevMain.isRanFromHere()) {
            for (File file : GameDevMain.getClassPath().getFiles()) {
                if (file.isDirectory()) {
                    ModClassLoader modClassLoader = InternalClassLoader.get().addMod(file);

                    loadDir(file.getAbsolutePath(), loadingGui);
                    BubbleBlaster.getInstance().getResourceManager().importResources(file);
                } else {
                    LOGGER.info(String.format("Found non-mod file: %s (path is not a directory)", file.getAbsolutePath()));
                }
            }
        }
        for (File file : Objects.requireNonNull(MODS_DIR.listFiles())) {
            if (file.getName().endsWith(".jar")) {
                ModClassLoader modClassLoader = InternalClassLoader.get().addMod(file);

                if (modClassLoader == null) {
                    continue;
                }

                LOGGER.debug("Loading jar file: " + file.getName());
                boolean flag = loadJar(file.getPath(), loadingGui);
                if (!flag) {
                    LOGGER.info("No flag.");
                    LOGGER.info(String.format("Found non-mod file: %s (flag is false)", file.getName()));
                } else {
                    BubbleBlaster.getInstance().getResourceManager().importResources(file);
                }
            } else {
                LOGGER.info(String.format("Found non-mod file: %s (isn't a jar file)", file.getName()));
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String s : missing) {
            sb.append(s);
            sb.append(", ");
        }

        if (sb.length() > 0) {
            throw new IllegalStateException("Missing mod classes with ids: " + sb.substring(0, sb.length() - 2));
        }
    }

    public void constructMods() {
        if (ModManager.instance().getModIds().isEmpty()) {
            return;
        }

        ModManager modManager = ModManager.instance();

        ArrayList<ModObject> modObjects = new ArrayList<>(ModManager.instance().getModObjects());

        LOGGER.info(String.format("Constructing Mods: %s", StringUtils.join(modObjects.stream().map(ModObject::getId).toList(), ", ")));

        ModObject modObject;
        for (ModObject object : modObjects) {
            loadingGui.logInfo(object.getId());
            LOGGER.info("Currently constructing mod " + object.getId());
            try {
                ModLoadingContext.value = new ModLoadingContext(modEvents.get(object.getId()), object.getContainer());

                // Create eventbus and logger for mod.
                Logger logger = LogManager.getLogger(object.getAnnotation().modId());

                // Register mod object.
                modManager.registerModObject(object);

                // Create java mod instance.
                Class<?> modClass = object.getModClass();

                for (Field field : modClass.getDeclaredFields()) {
                    int modifiers = field.getModifiers();
                    if (!Modifier.isFinal(modifiers) &&
                            Modifier.isStatic(modifiers) &&
                            Modifier.isPublic(modifiers) &&
                            Logger.class.isAssignableFrom(field.getType()) &&
                            (field.getName().equals("logger") || field.getName().equals("LOGGER"))) {
                        field.set(null, LogManager.getLogger(modClass));
                    }
                }

                Constructor<?> constructor = modClass.getConstructor();
                constructor.setAccessible(true);
                Object mod = constructor.newInstance();

                // Register java mod.
                modManager.registerMod(mod, object);

                mods.add(mod);

                ModLoadingContext.value = null;
            } catch (Throwable t) {
                t.printStackTrace();

                CrashLog crashLog = new CrashLog("Constructing mod", t);
                CrashCategory modCategory = new CrashCategory("Mod was constructing");
                modCategory.add("Mod ID", object);
                modCategory.add("File", object.getContainer().getSource().getPath());

                BubbleBlaster.getInstance().crash(crashLog.createCrash());
            }
        }
    }

    public void modSetup() {
        LanguageManager.INSTANCE.register(new Locale("af"), "African");
        LanguageManager.INSTANCE.register(new Locale("el"), "Greek");
        LanguageManager.INSTANCE.register(new Locale("it"), "Italian");
        LanguageManager.INSTANCE.register(new Locale("en"), "English");
        LanguageManager.INSTANCE.register(new Locale("es"), "Spanish");
        LanguageManager.INSTANCE.register(new Locale("nl"), "Dutch");
        LanguageManager.INSTANCE.register(new Locale("fy"), "Frisk");
        LanguageManager.INSTANCE.register(new Locale("zh"), "Chinese");

        for (ModContainer container : ModManager.instance().getContainers()) {
            ModEvents events = modEvents.get(container.getModId());
            events.publish(new ModSetupEvent(container, BubbleBlaster.getInstance()));
        }

        Set<Locale> locales = LanguageManager.INSTANCE.getLocales();
        for (Locale locale : locales) {
            LanguageManager.INSTANCE.load(locale, LanguageManager.INSTANCE.getLanguageID(locale), BubbleBlaster.getInstance().getResourceManager());
        }
    }

    private boolean loadDir(String pathToDir, @Nullable LoadingGui loadingGui) {
        int internalMods = 0;

        File modFile = new File(pathToDir);

        InternalClassLoader loader = InternalClassLoader.get();
        String modFileId = loader.getModFileId(modFile);
        LOGGER.info("Loading mod file with id: {}", modFileId);
        loader.scan(modFileId);

        InputStream inputStream;
        File file1 = null;
        try {
            file1 = new File(modFile, "META-INF/mods.json").getAbsoluteFile().getCanonicalFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (!file1.exists()) {
            file1 = new File(modFile, "../../../resources/main/META-INF/mods.json");
        }
        try {
            inputStream = new FileInputStream(file1);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        JsonReader jsonReader = new JsonReader(inputStreamReader);
        JsonObject jsonObject = new Gson().fromJson(jsonReader, JsonObject.class);
        JsonArray mods = jsonObject.getAsJsonArray("mods");
        if (mods == null) {
            throw new IllegalStateException("No mods were not found in mod metadata of " + modFile.getAbsolutePath());
        }

        HashSet<String> fromJson = new HashSet<>();
        HashSet<String> fromScan = new HashSet<>();
        HashSet<String> current = new HashSet<>();
        HashMap<String, JsonObject> modJsons = new HashMap<>();

        for (JsonElement element : mods) {
            if (element.isJsonObject()) {
                JsonObject modJson = element.getAsJsonObject();
                String modId = modJson.getAsJsonPrimitive("modId").getAsString();
                Identifier.testLocation(modId);

                missing.add(modId);
                fromJson.add(modId);

                System.out.println("modJson = " + modJson);

                modJsons.put(modId, modJson);
            }
        }

        System.out.println("modJsons = " + modJsons);

        return load(internalMods, modFile, loader, modFileId, fromJson, modJsons);
    }

    private boolean load(int internalMods, File modFile, InternalClassLoader loader, String modFileId, HashSet<String> fromJson, HashMap<String, JsonObject> modJsons) {
        tk.ultreonteam.bubbles.mod.loader.Scanner.Result result = loader.getResult(modFileId);
        scanResults.put(modFile, result);

        List<Class<?>> classes = result.getClasses(Mod.class);
        for (Class<?> clazz : classes) {
            Mod mod = clazz.getDeclaredAnnotation(Mod.class);
            if (mod != null) {
                boolean flag = false;
                for (Constructor<?> constructor : clazz.getConstructors()) {
                    if (constructor.getParameterCount() == 0) {
                        flag = true;
                        break;
                    }
                }

                if (!flag) {
                    throw new IllegalArgumentException("Mod has no constructor with 3 parameters. (" + clazz.getName() + ")");
                }

                Identifier.testLocation(mod.modId());

                internalMods++;

                ModContainer modContainer = new ModContainer() {
                    private final ModObject modObject;
                    private final JsonObject modJson = modJsons.get(mod.modId());
                    private final ModInformation modInfo = new ModInformation(this);

                    {
                        modObject = new ModObject(mod.modId(), this, mod, clazz);
                    }

                    @Override
                    public String getModId() {
                        return mod.modId();
                    }

                    @Override
                    public String getModFileId() {
                        return modFileId;
                    }

                    @Override
                    public JsonObject getModProperties() {
                        return modJson;
                    }

                    @Override
                    public File getSource() {
                        return modFile;
                    }

                    @Nullable
                    @Override
                    public JarFile getJarFile() {
                        return null;
                    }

                    @Override
                    public ModInformation getModInfo() {
                        return modInfo;
                    }

                    @Override
                    public Class<?> getModClass() {
                        return clazz;
                    }

                    @Override
                    public ModObject getModObject() {
                        return modObject;
                    }

                    @Override
                    public Object getModInstance() {
                        return getModObject().getMod();
                    }
                };

                modEvents.put(mod.modId(), new ModEvents(modContainer.getModObject()));

                ModManager.registerContainer(modContainer);
                ModManager.instance().registerModObject(modContainer.getModObject());

                if (!fromJson.contains(mod.modId())) {
                    throw new IllegalArgumentException("Missing mod with ID " + mod.modId() + " in the mod metadata.");
                }
                missing.remove(mod.modId());
            }
        }

        if (internalMods == 0) {
            LOGGER.warn("Mod has no annotated classes.");
        }

        return internalMods != 0;
    }

    @SuppressWarnings("resource")
    public boolean loadJar(String pathToJar, @Nullable LoadingGui loadingGui) {
        try {
            Enumeration<JarEntry> entryEnumeration;
            int internalMods = 0;

            File modFile = new File(pathToJar);

            InternalClassLoader loader = InternalClassLoader.get();
            String modFileId = loader.getModFileId(modFile);
            LOGGER.info("Loading mod file with id: {}", modFileId);
            loader.scan(modFileId);

            JarFile jarFile = null;
            File dir = null;
            if (modFile.isFile()) {
                jarFile = new JarFile(modFile);
            } else {
                dir = modFile;
            }

            InputStream inputStream;
            if (jarFile == null) {
                File file1 = new File(dir, "../../../resources/main/META-INF/mods.json").getCanonicalFile();
                inputStream = new FileInputStream(file1);
            } else {
                JarEntry modMetaEntry = jarFile.getJarEntry("META-INF/mods.json");
                inputStream = jarFile.getInputStream(modMetaEntry);
            }
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            JsonReader jsonReader = new JsonReader(inputStreamReader);
            JsonObject jsonObject = new Gson().fromJson(jsonReader, JsonObject.class);
            JsonArray mods = jsonObject.getAsJsonArray("mods");
            if (mods == null) {
                throw new IllegalStateException("No mods were not found in mod metadata of " + modFile.getAbsolutePath());
            }

            HashSet<String> fromJson = new HashSet<>();
            HashSet<String> fromScan = new HashSet<>();
            HashSet<String> current = new HashSet<>();
            HashMap<String, JsonObject> modJsons = new HashMap<>();

            for (JsonElement element : mods) {
                if (element.isJsonObject()) {
                    JsonObject modJson = element.getAsJsonObject();
                    String modId = modJson.getAsJsonPrimitive("modId").getAsString();
                    Identifier.testLocation(modId);

                    missing.add(modId);
                    fromJson.add(modId);

                    modJsons.put(modId, modJson);
                }
            }

            return load(internalMods, modFile, loader, modFileId, fromJson, modJsons);
        } catch (Throwable t) {
            CrashLog crashLog = new CrashLog("Loading mods", t);
            CrashCategory modCategory = new CrashCategory("Mod Loading");
            modCategory.add("File", pathToJar);
            crashLog.addCategory(modCategory);

            BubbleBlaster.getInstance().crash(crashLog.createCrash());
            System.exit(1);
            return false;
        }
    }

    public Scanner.Result getScanResult(File file) {
        return scanResults.get(file);
    }

    public static File getModsDir() {
        return MODS_DIR;
    }

    public ArrayList<Object> getMods() {
        return mods;
    }

    public ModEvents getModEvents(String modId) {
        return modEvents.get(modId);
    }
}
