package tk.ultreonteam.bubbles.mod;

import tk.ultreonteam.bubbles.event.bus.ModEvents;
import org.apache.logging.log4j.Logger;

public abstract class ModInstance {
    public final Logger logger;
    private final String modId;
    private final ModObject modObject;

    public ModInstance(Logger logger, String modId, ModObject modObject) {
        this.logger = logger;
        this.modId = modId;
        this.modObject = modObject;
    }

    // * See comment on cast
    public ModEvents getEventBus() {
        return modObject.getEventBus();
    }

    public Logger getLogger() {
        return logger;
    }

    public String getModId() {
        return modId;
    }

    public ModObject getModObject() {
        return modObject;
    }
}
