package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.common.text.translation.Language;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.gui.OptionsButton;
import tk.ultreonteam.bubbles.render.gui.OptionsNumberInput;
import tk.ultreonteam.bubbles.settings.GameSettings;
import tk.ultreonteam.bubbles.util.Util;

import java.awt.*;
import java.util.Objects;

@SuppressWarnings("unused")
public class OptionsScreen extends Screen {
    private static OptionsScreen INSTANCE;
    private OptionsNumberInput maxBubblesOption;
    private OptionsButton languageButton;
    private OptionsButton cancelButton;
    private OptionsButton saveButton;
    private Screen back;

    public OptionsScreen(Screen back) {
        super();

        OptionsScreen.INSTANCE = this;

        this.back = back;
    }

    public static OptionsScreen instance() {
        return INSTANCE;
    }

    private void save() {
        int maxBubbles = maxBubblesOption.getValue();

        GameSettings settings = GameSettings.instance();
        settings.setMaxBubbles(maxBubbles);
        settings.save();
    }

    private void showLanguages() {
        Objects.requireNonNull(Util.getSceneManager()).displayScreen(new LanguageScreen(this));
    }

    private void back() {
        Objects.requireNonNull(Util.getSceneManager()).displayScreen(back);
    }

    @Override
    public void init() {
        clearWidgets();

        this.maxBubblesOption = add(new OptionsNumberInput(width / 2 - 305, 3 * height / 5 - 40 - 5, 300, 40, GameSettings.instance().getMaxBubbles(), 400, 2000));
        this.languageButton = add(new OptionsButton.Builder().bounds(width / 2 + 5, 3 * height / 5 - 40 - 5, 300, 40).command(this::showLanguages).build());
        this.cancelButton = add(new OptionsButton.Builder().bounds(width / 2 - 305, 3 * height / 5 + 5, 300, 40).command(this::back).build());
        this.saveButton = add(new OptionsButton.Builder().bounds(width / 2 + 5, 3 * height / 5 + 5, 300, 40).command(this::save).build());
    }

    @Override
    public boolean onClose(Screen to) {
        BubbleBlaster.getEventBus().unsubscribe(this);

        clearWidgets();

        if (to == back) {
            back = null;
        }
        return super.onClose(to);
    }

    @Override
    public void render(BubbleBlaster game, Renderer renderer, float partialTicks) {
//        maxBubblesOption.setX((int) BubbleBlaster.getMiddleX() - 322);
//        maxBubblesOption.setY((int) BubbleBlaster.getMiddleY() + 101);
//        maxBubblesOption.setWidth(321);
//
//        languageButton.setX((int) BubbleBlaster.getMiddleX() + 1);
//        languageButton.setY((int) BubbleBlaster.getMiddleY() + 101);
//        languageButton.setWidth(321);
//
//        cancelButton.setX((int) BubbleBlaster.getMiddleX() - 322);
//        cancelButton.setY((int) BubbleBlaster.getMiddleY() + 151);
//        cancelButton.setWidth(321);
//
//        saveButton.setX((int) BubbleBlaster.getMiddleX() + 1);
//        saveButton.setY((int) BubbleBlaster.getMiddleY() + 151);
//        saveButton.setWidth(321);

        renderBackground(game, renderer);

        cancelButton.setText(Language.translate("tk.ultreonteam.bubbles/Other/Cancel"));
        cancelButton.render(renderer);

        languageButton.setText(Language.translate("tk.ultreonteam.bubbles/Screen/Options/Language"));
        languageButton.render(renderer);

        maxBubblesOption.render(renderer);

        saveButton.render(renderer);
        saveButton.setText(Language.translate("tk.ultreonteam.bubbles/Other/Save"));
    }

    public void renderBackground(BubbleBlaster game, Renderer gg) {
        gg.color(new Color(96, 96, 96));
        gg.rect(0, 0, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight());
    }
}
