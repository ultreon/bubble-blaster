package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.gui.FullscreenGui;
import tk.ultreonteam.bubbles.render.screen.gui.InputWidget;
import org.checkerframework.common.value.qual.IntRange;

import java.awt.*;
import java.util.concurrent.CopyOnWriteArrayList;

@SuppressWarnings("unused")
public abstract class Screen extends FullscreenGui {
    @IntRange(from = 0)
    private int focusIndex = 0;

    public Screen() {
        super();
    }

    private boolean valid;
    protected final BubbleBlaster game = BubbleBlaster.getInstance();

    public final void resize(int width, int height) {
        onResize(width, height);
        init(width, height);
    }

    protected void onResize(int width, int height) {

    }

    public void init(int width, int height) {
        this.width = width;
        this.height = height;

        this.init();
    }

    /**
     * Show Scene
     *
     * @author Qboi
     */
    public abstract void init();

    /**
     * Hide scene, unbind events.
     *
     * @param to the next scene to go.
     * @return true to cancel change screen.
     * @author Qboi
     */
    public boolean onClose(Screen to) {
        return true;
    }

    public void forceClose() {

    }

    @Override
    public void make() {
        super.make();
        valid = true;
    }

    @Override
    public void destroy() {
        valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public void onMouseExit() {
        if (this.hovered != null) {
            this.hovered.onMouseExit();
            this.hovered = null;
        }
    }

    @Override
    public boolean onKeyPress(int keyCode, char character) {
        if (keyCode == KeyInput.Map.KEY_ESCAPE) {
            game.showScreen(null);
            return true;
        }

        if (super.onKeyPress(keyCode, character)) return true;

        if (keyCode == KeyInput.Map.KEY_TAB) {
            this.focusIndex++;
            onChildFocusChanged();
            return true;
        }

        return this.focused != null && this.focused.onKeyPress(keyCode, character);
    }

    @Override
    public boolean onKeyRelease(int keyCode, char character) {
        if (keyCode == KeyInput.Map.KEY_TAB) return true;

        return this.focused != null && this.focused.onKeyRelease(keyCode, character);
    }

    @Override
    public boolean onKeyType(int keyCode, char character) {
        if (keyCode == KeyInput.Map.KEY_TAB) return true;

        return this.focused != null && this.focused.onKeyType(keyCode, character);
    }

    @Override
    public boolean onMousePress(int x, int y, int button) {
        InputWidget inputWidget = getWidgetAt(x, y);
        if (inputWidget != null) {
            focused = inputWidget;
            return true;
        }
        return super.onMousePress(x, y, button);
    }

    public void onChildFocusChanged() {
        CopyOnWriteArrayList<InputWidget> clone = new CopyOnWriteArrayList<>(children);
        if (this.focusIndex >= clone.size()) {
            this.focusIndex = 0;
        }

        this.focused = clone.get(this.focusIndex);
    }

    /**
     * @return the currently focused widget.
     */
    public InputWidget getFocusedWidget() {
        return focused;
    }

    /**
     * Renders the screen.<br>
     * <b>Note: <i>super calls are recommended to make the gui work correctly.</i></b>
     *
     * @param game         the game's instance.
     * @param renderer     the renderer.
     * @param partialTicks partial ticks / frame time.
     */
    public void render(BubbleBlaster game, Renderer renderer, float partialTicks) {
        renderChildren(renderer);
    }

    /**
     * Get the default cursor for the screen.
     *
     * @return the default cursor.
     */
    public Cursor getDefaultCursor() {
        return BubbleBlaster.getInstance().getDefaultCursor();
    }

    /**
     * Returns if the screen should pause the game or not.
     *
     * @return true to pause the game.
     * @see BubbleBlaster#isPaused()
     */
    public boolean doesPauseGame() {
        return true;
    }
}
