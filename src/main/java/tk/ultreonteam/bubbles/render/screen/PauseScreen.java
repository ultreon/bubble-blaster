package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.LoadedGame;
import tk.ultreonteam.bubbles.common.text.CommonTexts;
import tk.ultreonteam.bubbles.common.text.TranslationText;
import tk.ultreonteam.bubbles.common.text.translation.Language;
import tk.ultreonteam.bubbles.debug.Debugger;
import tk.ultreonteam.bubbles.ingame.EnvironmentRenderer;
import tk.ultreonteam.bubbles.ingame.bubble.AbstractBubble;
import tk.ultreonteam.bubbles.ingame.entity.bubble.BubbleSystem;
import tk.ultreonteam.bubbles.init.Sounds;
import tk.ultreonteam.bubbles.registry.Registry;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.gui.IngameButton;
import tk.ultreonteam.bubbles.util.GraphicsUtils;
import tk.ultreonteam.bubbles.util.Util;
import tk.ultreonteam.bubbles.util.helpers.MathHelper;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class PauseScreen extends Screen {
    private IngameButton exitButton;
    private IngameButton prevButton;
    private IngameButton nextButton;

    private static final TranslationText MIN_RADIUS = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/MinRadius");
    private static final TranslationText MAX_RADIUS = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/MaxRadius");
    private static final TranslationText MIN_SPEED = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/MinSpeed");
    private static final TranslationText MAX_SPEED = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/MaxSpeed");
    private static final TranslationText DEF_CHANCE = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/DefaultChance");
    private static final TranslationText CUR_CHANCE = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/CurrentChance");
    private static final TranslationText DEF_PRIORITY = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/DefaultPriority");
    private static final TranslationText CUR_PRIORITY = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/CurrentPriority");
    private static final TranslationText DEF_TOT_PRIORITY = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/DefaultTotalPriority");
    private static final TranslationText CUR_TOT_PRIORITY = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/CurrentTotalPriority");
    private static final TranslationText SCORE_MOD = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/ScoreModifier");
    private static final TranslationText ATTACK_MOD = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/AttackModifier");
    private static final TranslationText DEFENSE_MOD = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/DefenseModifier");
    private static final TranslationText CAN_SPAWN = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/CanSpawn");
    private static final TranslationText DESCRIPTION = new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/Description");
    private static final TranslationText RANDOM = new TranslationText("tk.ultreonteam.bubbles/Other/Random");

    private final Font bubbleTitleFont = new Font(BubbleBlaster.getInstance().getSansFontName(), Font.BOLD, 32);
    private final Font bubbleValueFont = new Font(BubbleBlaster.getInstance().getSansFontName(), Font.BOLD + Font.ITALIC, 16);
    private final Font bubbleInfoFont = new Font(BubbleBlaster.getInstance().getSansFontName(), Font.BOLD, 16);
    private final Font fallbackTitleFont = new Font(BubbleBlaster.getInstance().getFont().getFontName(), Font.BOLD, 32);
    private final Font fallbackValueFont = new Font(BubbleBlaster.getInstance().getFont().getFontName(), Font.BOLD + Font.ITALIC, 16);
    private final Font fallbackInfoFont = new Font(BubbleBlaster.getInstance().getFont().getFontName(), Font.BOLD, 16);

    private final Font pauseFont = new Font(BubbleBlaster.getInstance().getGameFontName(), Font.PLAIN, 75);
    private final int differentBubbles;
    private static int helpIndex = 0;
    private AbstractBubble bubble;

    public PauseScreen() {
        super();

        differentBubbles = Registry.getRegistry(AbstractBubble.class).values().size();
        tickPage();
    }

    private void previousPage() {
        if (helpIndex > 0) {
            Sounds.focusChangeSFX.setVolume(0.2d);
            Sounds.focusChangeSFX.play();
        }

        Debugger.verbose("Previous page");

        helpIndex = MathHelper.clamp(helpIndex - 1, 0, differentBubbles - 1);

        invalidateNav();
        tickPage();
    }

    private void nextPage() {
        if (helpIndex < differentBubbles - 1) {
            Sounds.focusChangeSFX.setVolume(0.2d);
            Sounds.focusChangeSFX.play();
        }

        Debugger.verbose("Next page");

        helpIndex = MathHelper.clamp(helpIndex + 1, 0, differentBubbles - 1);

        invalidateNav();
        tickPage();
    }

    private void invalidateNav() {
        prevButton.setVisible(helpIndex > 0);
        nextButton.setVisible(helpIndex < differentBubbles - 1);
    }

    private void tickPage() {
        bubble = new ArrayList<>(Registry.getRegistry(AbstractBubble.class).values()).get(helpIndex);
    }

    @Override
    public void init() {
        clearWidgets();


        exitButton = add(new IngameButton.Builder()
                .bounds((int) (BubbleBlaster.getMiddleX() - 128), 200, 256, 48)
                .text(new TranslationText("tk.ultreonteam.bubbles/Screen/Pause/Exit"))
                .command(BubbleBlaster.getInstance()::shutdown)
                .build());
        prevButton = add(new IngameButton.Builder()
                .bounds((int) (BubbleBlaster.getMiddleX() - 480), 250, 96, 48)
                .text(new TranslationText("tk.ultreonteam.bubbles/Other/Prev"))
                .command(this::previousPage)
                .build());
        nextButton = add(new IngameButton.Builder()
                .bounds((int) (BubbleBlaster.getMiddleX() + 480 - 95), 250, 96, 48)
                .text(new TranslationText("tk.ultreonteam.bubbles/Other/Next"))
                .command(this::nextPage)
                .build());

        invalidateNav();

        BubbleBlaster.getEventBus().subscribe(this);

        if (!BubbleBlaster.getInstance().isInGame()) {
            return;
        }

        Util.setCursor(BubbleBlaster.getInstance().getDefaultCursor());
    }

    @Override
    public boolean onClose(Screen to) {
        clearWidgets();

        BubbleBlaster.getEventBus().unsubscribe(this);

        Util.setCursor(BubbleBlaster.getInstance().getBlankCursor());
        return super.onClose(to);
    }

    @Override
    public void render(BubbleBlaster game, Renderer renderer, float partialTicks) {
        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();
        if (loadedGame == null) {
            return;
        }

        Renderer ngg = new Renderer(renderer);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //     Darkened background     //
        /////////////////////////////////
        ngg.color(new Color(0, 0, 0, 192));
        ngg.rect(0, 0, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight());
        Font oldFont = ngg.getFont();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //     Pause text     //
        ////////////////////////
        ngg.color(new Color(255, 255, 255, 128));
        ngg.font(pauseFont);
        GraphicsUtils.drawCenteredString(ngg, Language.translate("tk.ultreonteam.bubbles/Screen/Pause/Text"), new Rectangle2D.Double(0, 90, BubbleBlaster.getInstance().getWidth(), ngg.fontMetrics(pauseFont).getHeight()), pauseFont);

        ngg.font(oldFont);

        super.render(game, renderer, partialTicks);

        // Border
        ngg.color(new Color(255, 255, 255, 128));
        ngg.rectLine((int) (BubbleBlaster.getMiddleX() - 480), 300, 960, 300);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //     Bubble     //
        ////////////////////

        // Bubble name.
        ngg.color(new Color(255, 255, 255, 192));
        ngg.font(bubbleTitleFont);
        ngg.fallbackFont(fallbackTitleFont);
        ngg.text(Language.translate(bubble.getTranslationPath()), (int) BubbleBlaster.getMiddleX() - 470, 332);

        // Bubble icon.
        EnvironmentRenderer.drawBubble(ngg, (int) (BubbleBlaster.getMiddleX() - 470), 350, 122, bubble.colors);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //     Info Names     //
        ////////////////////////

        // Set color & font.
        ngg.font(bubbleInfoFont);
        ngg.fallbackFont(fallbackInfoFont);
        ngg.color(new Color(255, 255, 255, 192));

        // Left data.
        ngg.text(MIN_RADIUS, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 362);
        ngg.text(MAX_RADIUS, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 382);
        ngg.text(MIN_SPEED, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 402);
        ngg.text(MAX_SPEED, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 422);
        ngg.text(DEF_CHANCE, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 442);
        ngg.text(CUR_CHANCE, (int) (BubbleBlaster.getMiddleX() - 326) + 10, 462);

        // Right data.
        ngg.text(DEF_TOT_PRIORITY, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 322);
        ngg.text(CUR_TOT_PRIORITY, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 342);
        ngg.text(DEF_PRIORITY, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 362);
        ngg.text(CUR_PRIORITY, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 382);
        ngg.text(SCORE_MOD, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 402);
        ngg.text(ATTACK_MOD, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 422);
        ngg.text(DEFENSE_MOD, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 442);
        ngg.text(CAN_SPAWN, (int) (BubbleBlaster.getMiddleX() + 72) + 10, 462);

        // Description
        ngg.text(DESCRIPTION, (int) BubbleBlaster.getMiddleX() - 470, 502);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //     Values     //
        ////////////////////

        // Set color & font.
        ngg.font(bubbleValueFont);
        ngg.fallbackFont(fallbackValueFont);
        ngg.color(new Color(255, 255, 255, 128));

        // Left data.
        ngg.text(Integer.toString(bubble.getMinRadius()), (int) (BubbleBlaster.getMiddleX() - 326) + 200, 362);
        ngg.text(Integer.toString(bubble.getMaxRadius()), (int) (BubbleBlaster.getMiddleX() - 326) + 200, 382);
        ngg.text(Double.toString(MathHelper.round(bubble.getMinSpeed(), 5)), (int) (BubbleBlaster.getMiddleX() - 326) + 200, 402);
        ngg.text(Double.toString(MathHelper.round(bubble.getMaxSpeed(), 5)), (int) (BubbleBlaster.getMiddleX() - 326) + 200, 422);
        ngg.text(MathHelper.round((double) 100 * BubbleSystem.getDefaultPercentageChance(bubble), 5) + "%", (int) (BubbleBlaster.getMiddleX() - 326) + 200, 442);
        ngg.text(MathHelper.round((double) 100 * BubbleSystem.getPercentageChance(bubble), 5) + "%", (int) (BubbleBlaster.getMiddleX() - 326) + 200, 462);
        ngg.text(compress(BubbleSystem.getDefaultTotalPriority()), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 322);
        ngg.text(compress(BubbleSystem.getTotalPriority()), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 342);
        ngg.text(compress(BubbleSystem.getDefaultPriority(bubble)), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 362);
        ngg.text(compress(BubbleSystem.getPriority(bubble)), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 382);

        // Right data
        if (bubble.isScoreRandom()) {
            ngg.text(RANDOM, (int) (BubbleBlaster.getMiddleX() + 72) + 200, 402);
        } else {
            ngg.text(Double.toString(MathHelper.round(bubble.getScore(), 5)), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 402);
        }
        if (bubble.isAttackRandom()) {
            ngg.text(RANDOM, (int) (BubbleBlaster.getMiddleX() + 72) + 200, 422);
        } else {
            ngg.text(Double.toString(MathHelper.round(bubble.getAttack(), 5)), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 422);
        }
        if (bubble.isDefenseRandom()) {
            ngg.text(RANDOM, (int) (BubbleBlaster.getMiddleX() + 72) + 200, 442);
        } else {
            ngg.text(Double.toString(MathHelper.round(bubble.getDefense(), 5)), (int) (BubbleBlaster.getMiddleX() + 72) + 200, 442);
        }
        ngg.text(bubble.canSpawn(loadedGame.getEnvironment()) ? CommonTexts.TRUE : CommonTexts.FALSE, (int) (BubbleBlaster.getMiddleX() + 72) + 200, 462);

        // Description
        ngg.wrappedText(Language.translate(bubble.getDescriptionTranslationPath()).replaceAll("\\\\n", "\n"), (int) BubbleBlaster.getMiddleX() - 470, 522, 940);

        ngg.font(oldFont);
    }

    private String compress(double totalPriority) {
        if (totalPriority >= 0d && totalPriority < 1_000d) {
            return Double.toString(totalPriority);
        }
        if (totalPriority >= 1_000d && totalPriority < 1_000_000d) {
            return MathHelper.round(totalPriority / 1_000d, 1) + "K";
        }
        if (totalPriority >= 1_000_000d && totalPriority < 1_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000d, 1) + "M";
        }
        if (totalPriority >= 1_000_000_000d && totalPriority < 1_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000d, 1) + "B";
        }
        if (totalPriority >= 1_000_000_000_000d && totalPriority < 1_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000d, 1) + "T";
        }
        if (totalPriority >= 1_000_000_000_000_000d && totalPriority < 1_000_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000_000d, 1) + "QD";
        }
        if (totalPriority >= 1_000_000_000_000_000_000d && totalPriority < 1_000_000_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000_000_000d, 1) + "QT";
        }
        if (totalPriority >= 1_000_000_000_000_000_000_000d && totalPriority < 1_000_000_000_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000_000_000_000d, 1) + "S";
        }
        if (totalPriority >= 1_000_000_000_000_000_000_000_000d && totalPriority < 1_000_000_000_000_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000_000_000_000_000d, 1) + "SX";
        }
        if (totalPriority >= 1_000_000_000_000_000_000_000_000_000d && totalPriority < 1_000_000_000_000_000_000_000_000_000_000d) {
            return MathHelper.round(totalPriority / 1_000_000_000_000_000_000_000_000_000d, 1) + "C";
        }
        return Double.toString(totalPriority);
    }

    @Override
    public boolean doesPauseGame() {
        return true;
    }
}
