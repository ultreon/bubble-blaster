package tk.ultreonteam.bubbles.render.screen.gui;

import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.vector.Vec2i;

/**
 * Static widget, a widget that only has boundaries and something that will be drawn.
 * This is like an image, or text label. The {@link InputWidget} class extends this and has input handling support.
 *
 * @author Qboi
 * @see InputWidget
 */
public interface StaticWidget extends GuiElement {
    /**
     * @return the x position of the widget.
     */
    int getX();

    /**
     * @return the y position of the widget.
     */
    int getY();

    /**
     * @return the width of the widget.
     */
    int getWidth();

    /**
     * @return the height of the widget.
     */
    int getHeight();

    /**
     * Rendering method, should not be called if you don't know what you are doing.
     *
     * @param renderer renderer to draw/render with.
     */
    void render(Renderer renderer);

    /**
     * @return the position of the widget.
     */
    default Vec2i getPos() {
        return new Vec2i(getX(), getY());
    }

    /**
     * @return the size of the widget.
     */
    default Vec2i getSize() {
        return new Vec2i(getWidth(), getHeight());
    }

    /**
     * @return the boundaries of the widget.
     */
    default Rectangle getBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }
}
