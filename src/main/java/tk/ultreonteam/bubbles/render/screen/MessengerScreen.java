package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.settings.GameSettings;
import tk.ultreonteam.bubbles.util.GraphicsUtils;
import tk.ultreonteam.commons.lang.Messenger;

import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Environment loading scene.
 * When showing this scene, a new thread will be created and in that thread the loading will be done.
 * The thread is located in the method {@link #init()}.
 * <p>
 * Todo: update docstring.
 *
 * @author Qboi
 * @since 1.0.615
 */
public class MessengerScreen extends Screen {
    private final Messenger messenger = new Messenger(this::setDescription);
    private String description = "";
    protected BubbleBlaster game = BubbleBlaster.getInstance();

    public MessengerScreen() {

    }

    public MessengerScreen(String description) {
        this.description = description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void init() {

    }

    public Messenger getMessenger() {
        return messenger;
    }

    /**
     * Renders the environment loading scene.<br>
     * Shows the title in the blue accent color (#00b0ff), and the description in a 50% black color (#7f7f7f).
     *
     * @param game         the game launched.
     * @param renderer     the graphics 2D processor.
     * @param partialTicks game frame time.
     */
    @Override
    public void render(BubbleBlaster game, Renderer renderer, float partialTicks) {
        renderer.color(new Color(64, 64, 64));
        renderer.rect(0, 0, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight());
        if (GameSettings.instance().isTextAntialiasEnabled())
            renderer.hint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        renderer.color(new Color(0, 192, 255));
        GraphicsUtils.drawCenteredString(renderer, "Loading Environment...", new Rectangle2D.Double(0, ((double) BubbleBlaster.getInstance().getHeight() / 2) - 24, BubbleBlaster.getInstance().getWidth(), 64d), new Font("Helvetica", Font.PLAIN, 48));
        renderer.color(new Color(127, 127, 127));
        GraphicsUtils.drawCenteredString(renderer, this.description, new Rectangle2D.Double(0, ((double) BubbleBlaster.getInstance().getHeight() / 2) + 40, BubbleBlaster.getInstance().getWidth(), 50d), new Font("Helvetica", Font.PLAIN, 20));
        if (GameSettings.instance().isTextAntialiasEnabled())
            renderer.hint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    }

    public String getDescription() {
        return description;
    }
}
