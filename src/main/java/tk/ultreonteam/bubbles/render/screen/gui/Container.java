package tk.ultreonteam.bubbles.render.screen.gui;

import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.Screen;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.common.value.qual.IntRange;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Container widget, this will contain other widgets.
 * This is mainly used for screens.
 *
 * @author Qboi
 * @see Screen
 */
public abstract class Container extends InputWidget {
    protected final List<InputWidget> children = new CopyOnWriteArrayList<>();
    protected final List<StaticWidget> statics = new CopyOnWriteArrayList<>();
    protected InputWidget hovered;
    protected InputWidget focused = null;

    /**
     * Constructs a container.
     *
     * @param x      the x position of the container.
     * @param y      the y position of the container.
     * @param width  the width of the container.
     * @param height the height of the container.
     */
    public Container(int x, int y, @IntRange(from = 0) int width, @IntRange(from = 0) int height) {
        super(x, y, width, height);
    }

    /**
     * Render the container.
     *
     * @param renderer the renderer to render with.
     */
    @Override
    public void render(Renderer renderer) {
        Renderer containment = renderer.frame(this.x, this.y, this.width, this.height);
        renderChildren(containment);
    }

    /**
     * Render all child widgets.
     *
     * @param renderer the renderer to render with.
     */
    protected void renderChildren(Renderer renderer) {
        for (InputWidget child : this.children) {
            if (child.isVisible()) {
                child.render(renderer);
            }
        }
    }

    /**
     * Adds a {@link InputWidget} to the screen, including initializing it with {@link GuiElement#make()}.
     *
     * @param widget the widget to add.
     * @param <T>    the widget's type.
     * @return the same widget as the parameter.
     */
    public <T extends InputWidget> T add(T widget) {
        this.children.add(widget);
        this.statics.add(widget);
        widget.make();
        return widget;
    }

    /**
     * Adds a {@link StaticWidget} to the screen, including initializing it with {@link GuiElement#make()}.
     *
     * @param widget the widget to add.
     * @param <T>    the widget's type.
     * @return the same widget as the parameter.
     */
    public <T extends StaticWidget> T add(T widget) {
        this.statics.add(widget);
        widget.make();
        return widget;
    }

    /**
     * Removes an input widget from the container.
     *
     * @param widget widget to remove.
     */
    public void remove(InputWidget widget) {
        this.children.remove(widget);
        this.statics.remove(widget);
    }

    /**
     * Removes a static widget from the container.
     *
     * @param widget widget to remove.
     */
    public void remove(StaticWidget widget) {
        if (widget instanceof InputWidget) {
            this.children.remove(widget);
        }
        this.statics.remove(widget);
    }

    /**
     * Get a widget at a specific location.
     *
     * @param x the x position
     * @param y the y position
     * @return the widget at that location.
     */
    @Nullable
    public InputWidget getWidgetAt(int x, int y) {
        for (InputWidget child : this.children) {
            if (child.isVisible() && child.isWithinBounds(x, y)) return child;
        }
        return null;
    }

    @Override
    public boolean onMouseClick(int x, int y, int button, int count) {
        InputWidget clicked = getWidgetAt(x, y);
        if (clicked != null) {
            clicked.click();
        }
        return clicked != null && clicked.onMouseClick(x, y, button, count);
    }

    @Override
    public boolean onMousePress(int x, int y, int button) {
        InputWidget pressed = getWidgetAt(x, y);
        if (focused != pressed) {
            if (focused != null) focused.removeFocus();
            focused = pressed;
            if (pressed != null) pressed.focus();
        }
        return pressed != null && pressed.onMousePress(x, y, button);
    }

    @Override
    public boolean onMouseRelease(int x, int y, int button) {
        InputWidget released = getWidgetAt(x, y);
        return released != null && released.onMouseRelease(x, y, button);
    }

    @Override
    public void onMouseMove(int x, int y) {
        boolean hovered = false;
        if (this.hovered != null && !this.hovered.isWithinBounds(x, y)) {
            this.hovered.onMouseExit();
        }

        InputWidget hoveredWidget = this.getWidgetAt(x, y);
        if (hoveredWidget != this.hovered) hovered = true;
        this.hovered = hoveredWidget;

        if (this.hovered != null) {
            this.hovered.onMouseMove(x, y);

            if (hovered) {
                this.hovered.onMouseEnter(x, y);
            }
        }
    }

    @Override
    public void onMouseDrag(int x, int y, int nx, int ny, int button) {
        InputWidget dragged = getWidgetAt(x, y);
        if (dragged != null) dragged.onMouseDrag(x, y, nx, ny, button);
    }

    @Override
    public void onMouseExit() {
        if (this.hovered != null) {
            this.hovered.onMouseExit();
            this.hovered = null;
        }
    }

    @Override
    public void onMouseWheel(int x, int y, double rotation, int amount, int units) {
        InputWidget scrolled = getWidgetAt(x, y);
        if (scrolled != null) scrolled.onMouseWheel(x, y, rotation, amount, units);
    }

    /**
     * Clear all widgets from the container.
     */
    protected final void clearWidgets() {
        for (InputWidget widget : children) {
            widget.destroy();
            statics.remove(widget);
        }
        for (StaticWidget widget : statics) {
            widget.destroy();
        }
        children.clear();
        statics.clear();
    }

    /**
     * Get the widget that's currently being hovered by the user.
     *
     * @return the hovered widget.
     */
    public InputWidget getHoveredWidget() {
        return hovered;
    }
}
