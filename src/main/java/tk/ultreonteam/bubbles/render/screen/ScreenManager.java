package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Scene manager, used for change between scenes.
 *
 * @see Screen
 */
@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
public class ScreenManager {
    private static final Logger LOGGER = LogManager.getLogger("Screen-Manager");
    private final Screen startScreen;
    private final BubbleBlaster game;

    @Nullable
    private Screen currentScreen;
    private boolean initialized = false;
    private final Object lock = new Object();

    private ScreenManager(@Nullable Screen startScreen, BubbleBlaster game) {
        this.game = game;
        this.currentScreen = this.startScreen = startScreen;
    }

    public static ScreenManager create(@Nullable Screen start, BubbleBlaster game) {
        return new ScreenManager(start, game);
    }

    /**
     * Display a new scene.
     *
     * @param scene the scene to display
     * @return if changing the scene was successful.
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean displayScreen(@Nullable Screen scene) {
        return displayScreen(scene, false);
    }

    /**
     * Display a new scene.
     *
     * @param screen the scene to display
     * @return if changing the scene was successful.
     */
    public boolean displayScreen(@Nullable Screen screen, boolean force) {
        synchronized (lock) {
            if (currentScreen != null) {
                LOGGER.debug("Hiding " + currentScreen.getClass().getSimpleName());
                if (currentScreen.onClose(screen) || force) {
                    return initNextScreen(screen);
                }
                LOGGER.debug("Hiding " + currentScreen.getClass().getSimpleName() + " canceled.");
            } else {
                LOGGER.debug("Hiding <<NO-SCENE>>");
                return initNextScreen(screen);
            }
        }
        return false;
    }

    private boolean initNextScreen(@Nullable Screen screen) {
        if (screen == null && game.isInMainMenus()) {
            screen = new TitleScreen();
        }
        if (screen != null) {
            game.getGameWindow().setCursor(screen.getDefaultCursor());
            LOGGER.debug("Showing " + screen.getClass().getSimpleName());
            screen.init();
        } else {
            game.getGameWindow().setCursor(game.getDefaultCursor());
            LOGGER.debug("Showing <<NO-SCENE>>");
        }

        this.currentScreen = screen;
        return true;
    }

    public synchronized void start() {
        synchronized (lock) {
            if (initialized) {
                throw new IllegalStateException("SceneManager already initialized.");
            }
            this.initialized = true;

            this.currentScreen = this.startScreen;
            if (currentScreen != null) {
                LOGGER.debug("Showing " + currentScreen.getClass().getSimpleName());
                game.getGameWindow().setCursor(this.startScreen.getDefaultCursor());
                this.startScreen.init();
            }
        }
    }

    @Nullable
    public Screen getCurrentScreen() {
        synchronized (lock) {
            return currentScreen;
        }
    }

    public boolean isInitialized() {
        synchronized (lock) {
            return initialized;
        }
    }
}
