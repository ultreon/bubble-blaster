package tk.ultreonteam.bubbles.render.screen.gui;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.render.Renderer;

public class FullscreenGui extends Container {
    private boolean valid;

    public FullscreenGui() {
        super(0, 0, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight());
    }

    @Override
    public void render(Renderer renderer) {
        super.render(renderer);
    }

    @Override
    public void make() {
        super.make();
        valid = true;
    }

    @Override
    public void destroy() {
        valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }
}
