package tk.ultreonteam.bubbles.render.screen;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.gui.OptionsButton;
import tk.ultreonteam.bubbles.render.gui.OptionsNumberInput;
import tk.ultreonteam.bubbles.util.GraphicsUtils;

import java.awt.*;
import java.util.Random;

public class StartOptionsScreen extends Screen {
    private final Screen back;
    private OptionsNumberInput seedInput;
    private OptionsButton startBtn;
    private int seed;

    public StartOptionsScreen(Screen back) {
        this.back = back;
    }

    @Override
    public void init() {
        clearWidgets();

        seed = new Random().nextInt();
        seedInput = add(new OptionsNumberInput(width / 2 - 150, height / 2 - 35, 300, 30, seed, Integer.MIN_VALUE, Integer.MAX_VALUE));
        seedInput.setResponder(text -> {
            try {
                seed = Integer.parseInt(text);
            } catch (Exception ignored) {
                {

                }
            }
        });
        startBtn = add(new OptionsButton.Builder().bounds(width / 2 - 150, height / 2 + 5, 300, 30).command(this::start).build());
    }

    private void start() {
        int value = seed;
        BubbleBlaster.getInstance().createGame(value);
    }

    @Override
    public void render(BubbleBlaster game, Renderer renderer, float partialTicks) {
        renderer.color(0xff333333);
        renderer.fill(game.getBounds());

        super.render(game, renderer, partialTicks);

        GraphicsUtils.drawRightAnchoredString(renderer, "Seed:", new Point(width / 2 - 110, height / 2 - 35), 30, new Font("Helvetica", Font.PLAIN, 12));
    }

    public void back() {
        game.showScreen(back);
    }
}
