package tk.ultreonteam.bubbles.render.screen.gui;

import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.vector.Vec2i;
import org.checkerframework.common.value.qual.IntRange;

import java.util.Objects;

/**
 * Controllable widget, a widget that can be controlled by the user.
 * This widget contains input event handlers like {@link #onKeyPress(int, char)} and {@link #onMouseClick(int, int, int, int)}
 *
 * @author Qboi
 */
@SuppressWarnings("unused")
public abstract class InputWidget implements GuiElement, StaticWidget {
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    private final long hash;
    private boolean focused;
    private boolean visible;

    /**
     * @param x      position of the widget
     * @param y      position of the widget
     * @param width  size of the widget
     * @param height size of the widget
     */
    public InputWidget(int x, int y, @IntRange(from = 0) int width, @IntRange(from = 0) int height) {
        if (width < 0) throw new IllegalArgumentException("Width is negative");
        if (height < 0) throw new IllegalArgumentException("Height is negative");

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.hash = System.nanoTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputWidget that = (InputWidget) o;
        return hash == that.hash;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash);
    }

    /**
     * Rendering method, should not be called unless you know what you are doing.
     * Override is fine.
     *
     * @param renderer the renderer to render with.
     */
    public abstract void render(Renderer renderer);

    /**
     * Handler for mouse clicking.<br>
     * Should only be overridden, and not called unless you know what you are doing.
     *
     * @param x      the x position when clicked.
     * @param y      the y position when clicked.
     * @param button the button used.
     * @param count  the amount of sequential clicks.
     */
    public boolean onMouseClick(int x, int y, int button, int count) {
        return false;
    }

    /**
     * Handler for mouse button press.<br>
     * Should only be overridden, and not called unless you know what you are doing.
     *
     * @param x      the x position when pressed.
     * @param y      the y position when pressed.
     * @param button the button used.
     */
    public boolean onMousePress(int x, int y, int button) {
        return false;
    }

    /**
     * Handler for mouse button release.<br>
     * Should only be overridden, and not called unless you know what you are doing.
     *
     * @param x      the x position when released.
     * @param y      the y position when released.
     * @param button the button used.
     */
    public boolean onMouseRelease(int x, int y, int button) {
        return false;
    }

    /**
     * Handler for mouse motion.<br>
     * Should only be overridden, and not called unless you know what you are doing.
     *
     * @param x the x position where the mouse moved to.
     * @param y the y position where the mouse moved to.
     */
    public void onMouseMove(int x, int y) {

    }

    /**
     * Handler for mouse pressing.<br>
     * Should only be overridden, and not called unless you know what you are doing.
     *
     * @param x      the x position when pressed.
     * @param y      the y position when pressed.
     * @param nx     the x position dragged to.
     * @param ny     the y position dragged to.
     * @param button the button used.
     */
    public void onMouseDrag(int x, int y, int nx, int ny, int button) {

    }

    /**
     * Called when the mouse exits the widget.
     */
    public void onMouseExit() {

    }

    /**
     * Called when the mouse enters the widget.
     *
     * @param x x position where it entered.
     * @param y y position where it entered.
     */
    public void onMouseEnter(int x, int y) {

    }

    public void onMouseWheel(int x, int y, double rotation, int amount, int units) {

    }

    /**
     * Key press handler.
     * Match a constant {@link KeyInput.Map} with the {@code keyCode} parameter for checking which key is pressed.
     *
     * @param keyCode   the code for the key pressed.
     * @param character the character pressed.
     * @return to cancel out other usage of this method.
     */
    public boolean onKeyPress(int keyCode, char character) {
        return false;
    }

    /**
     * Key release handler.
     * Match a constant {@link KeyInput.Map} with the {@code keyCode} parameter for checking which key is released.
     *
     * @param keyCode   the code for the key released.
     * @param character the character released.
     * @return to cancel out other usage of this method.
     */
    public boolean onKeyRelease(int keyCode, char character) {
        return false;
    }

    /**
     * Key type handler.
     * Match a constant {@link KeyInput.Map} with the {@code keyCode} parameter for checking which key is typed.
     *
     * @param keyCode   the code for the key typed.
     * @param character the character typed.
     * @return to cancel out other usage of this method.
     */
    public boolean onKeyType(int keyCode, char character) {
        return false;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width < 0) {
            throw new IllegalArgumentException("Width should be positive.");
        }
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (width < 0) {
            throw new IllegalArgumentException("Height should be positive.");
        }
        this.height = height;
    }

    public Vec2i getPos() {
        return new Vec2i(x, y);
    }

    public void setPos(int x, int y) {
        setX(x);
        setY(y);
    }

    public Vec2i getSize() {
        return new Vec2i(width, height);
    }

    public void setSize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    public void setBounds(int x, int y, int width, int height) {
        setPos(x, y);
        setSize(width, height);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    /**
     * Check if a position is withing the bounds of the widget
     *
     * @param x position to check for.
     * @param y position to check for.
     * @return true if the x and y position given is withing the bounds of the widget
     */
    public boolean isWithinBounds(int x, int y) {
        return x >= this.x && y >= this.y && x <= this.x + width && y <= this.y + height;
    }

    /**
     * Check if a position is withing the bounds of the widget
     *
     * @param pos position to check for.
     * @return true if the x and y position given is withing the bounds of the widget
     */
    public boolean isWithinBounds(Vec2i pos) {
        return pos.x >= this.x && pos.y >= this.y && pos.x <= this.x + width && pos.y <= this.y + height;
    }

    protected final void removeFocus() {
        focused = false;
        onRemoveFocus();
        onFocusChange(false);
    }

    protected final void focus() {
        focused = true;
        onFocus();
        onFocusChange(true);
    }

    public final boolean isFocused() {
        return focused;
    }

    protected void onFocusChange(boolean setTo) {

    }

    protected void onFocus() {

    }

    @Override
    public void make() {
        visible = true;
    }

    protected void onRemoveFocus() {

    }

    protected void click() {

    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
