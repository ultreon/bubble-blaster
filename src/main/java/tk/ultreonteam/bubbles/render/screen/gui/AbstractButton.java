package tk.ultreonteam.bubbles.render.screen.gui;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.input.MouseInput;
import tk.ultreonteam.bubbles.vector.Vec2i;
import org.checkerframework.common.value.qual.IntRange;

public abstract class AbstractButton extends InputWidget {
    private Runnable command;
    //    private boolean hovered;
    private boolean pressed;
    private boolean valid;

    public AbstractButton(int x, int y, @IntRange(from = 0) int width, @IntRange(from = 0) int height) {
        super(x, y, width, height);
    }

    @Override
    public boolean onMousePress(int x, int y, int button) {
        if (isWithinBounds(x, y) && button == 1) {
            this.pressed = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean onMouseRelease(int x, int y, int button) {
        this.pressed = false;
        return false;
    }

    @Override
    public void make() {
        super.make();
        this.valid = true;

        Vec2i mousePos = MouseInput.getPos();
        if (isWithinBounds(mousePos)) {
            BubbleBlaster.getInstance().getGameWindow().setCursor(BubbleBlaster.getInstance().getPointerCursor());
        }
    }

    @Override
    public void destroy() {
        this.valid = false;

        if (this.isHovered()) {
            BubbleBlaster.getInstance().getGameWindow().setCursor(BubbleBlaster.getInstance().getDefaultCursor());
        }
    }

    @Override
    public boolean isValid() {
        return this.valid;
    }

    @Override
    public void onMouseEnter(int x, int y) {

    }

    @Override
    public void onMouseExit() {

    }

    public boolean isPressed() {
        return this.pressed;
    }

    public boolean isHovered() {
        return isWithinBounds(MouseInput.getPos());
    }

    /**
     * @return the command to run when the button is pressed.
     */
    public Runnable getCommand() {
        return this.command;
    }

    /**
     * Change the command to run when the button is pressed.
     *
     * @param command the command to change to.
     */
    public void setCommand(Runnable command) {
        this.command = command;
    }

    protected final void click() {
        getCommand().run();
    }
}
