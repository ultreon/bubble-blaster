package tk.ultreonteam.bubbles.render.screen.gui;

public interface GuiElement {
    /**
     * Used to create the gui element, used internally. And should only be called if you know that it's needed to be called.
     * The {@link Container#add(InputWidget)} method calls this method already.
     */
    void make();

    /**
     * Used to clean up the gui element after deletion, used internally. And should only be called if you know that it's needed to be called.
     * The {@link Container#clearWidgets()} method calls this method already.
     */
    void destroy();

    /**
     * Check if the gui element is valid.
     * The {@link #make()} method should change the return of this method to {@code true}, and {@link #destroy()} should set it to {@code false}.
     *
     * @return true if the gui element is valid, false if otherwise ofc.
     */
    boolean isValid();
}
