package tk.ultreonteam.bubbles.render.screen.gui.cursor;

import tk.ultreonteam.bubbles.render.Renderer;

public class BlankCursorRenderer extends CursorRenderer {
    public BlankCursorRenderer() {
        super("blank_cursor");
    }

    @Override
    public void draw(Renderer g) {

    }
}
