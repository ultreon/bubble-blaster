package tk.ultreonteam.bubbles.render.screen.gui.style;

public enum State {
    NORMAL, HOVERED, PRESSED, FOCUSED, DISABLED
}
