package tk.ultreonteam.bubbles.render.gui;

@FunctionalInterface
public interface IFormula {
    double calculate(double x);
}
