package tk.ultreonteam.bubbles.render;

public abstract class TextureSource {
    public abstract Texture create();
}
