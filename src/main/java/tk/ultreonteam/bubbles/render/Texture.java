package tk.ultreonteam.bubbles.render;

public abstract class Texture {
    public abstract void render(Renderer gfx, int xf, int yf, int xs, int ys);
}
