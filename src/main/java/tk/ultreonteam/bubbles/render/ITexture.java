package tk.ultreonteam.bubbles.render;

public interface ITexture {
    void render(Renderer gg);

    int width();

    int height();
}
