package tk.ultreonteam.bubbles.render;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * A texture that is loaded from an image file.
 *
 * @author Qboi123
 * @since 0.1.0
 */
public abstract class ImageTex extends Texture {
    BufferedImage image;

    /**
     * Implementation should load the data from a file or resource.
     *
     * @return the byte data loaded from the file or resource.
     */
    protected abstract byte[] loadBytes();

    /**
     * Loads the image from the given file.
     *
     * @since 0.1.0
     * @author Qboi123
     */
    public void load() {
        byte[] bytes = loadBytes();
        try {
            this.image = ImageIO.read(new ByteArrayInputStream(bytes));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Renders the image texture.
     *
     * @param renderer the renderer to use.
     * @param xf the start x position.
     * @param yf the start y position.
     * @param xs the end x position.
     * @param ys the end y position.
     */
    @Override
    public void render(Renderer renderer, int xf, int yf, int xs, int ys) {
        renderer.image(image, xf, yf, xs - xf, ys - yf);
    }
}
