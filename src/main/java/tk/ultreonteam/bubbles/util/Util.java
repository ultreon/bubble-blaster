package tk.ultreonteam.bubbles.util;

import com.google.common.annotations.Beta;
import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.References;
import tk.ultreonteam.bubbles.render.screen.ScreenManager;
import tk.ultreonteam.bubbles.save.GameSave;
import tk.ultreonteam.commons.annotation.FieldsAreNonnullByDefault;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;
import tk.ultreonteam.commons.io.filefilters.DirectoryFileFilter;

import javax.annotation.ParametersAreNonnullByDefault;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;

@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
@FieldsAreNonnullByDefault
public class Util {
    public static ScreenManager getSceneManager() {
        return BubbleBlaster.getInstance().getScreenManager();
    }

    public static BubbleBlaster getGame() {
        return BubbleBlaster.getInstance();
    }

    public static Font getGameFont() {
        return BubbleBlaster.getInstance().getGameFont();
    }

    public static void setCursor(Cursor cursor) {
        // Set the cursor to the Game Window.
        BubbleBlaster.getInstance().getGameWindow().setCursor(cursor);
    }

    @Beta
    public static ArrayList<GameSave> getSaves() {
        ArrayList<GameSave> saves = new ArrayList<>();
        File savesDir = References.SAVES_DIR;

        File[] files = savesDir.listFiles(new DirectoryFileFilter());
        if (files == null) files = new File[]{};
        for (File save : files) {
            if (save.isDirectory()) {
                saves.add(GameSave.fromFile(save));
            }
        }

        return saves;
    }
}
