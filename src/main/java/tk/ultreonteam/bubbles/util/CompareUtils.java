package tk.ultreonteam.bubbles.util;

public class CompareUtils {
    public CompareUtils() {
        throw ExceptionUtils.utilityClass();
    }

    public static <T extends Comparable<T>> boolean isGreater(T obj, T than) {
        return obj.compareTo(than) > 0;
    }

    public static <T extends Comparable<T>> boolean isGreaterOrEqual(T obj, T than) {
        return obj.compareTo(than) >= 0;
    }

    public static <T extends Comparable<T>> boolean isEqual(T obj, T than) {
        return obj.compareTo(than) == 0;
    }

    public static <T extends Comparable<T>> boolean isLessEqual(T obj, T than) {
        return obj.compareTo(than) <= 0;
    }

    public static <T extends Comparable<T>> boolean isLess(T obj, T than) {
        return obj.compareTo(than) < 0;
    }

    public static <T extends Comparable<T>> boolean isNotEqual(T obj, T than) {
        return obj.compareTo(than) != 0;
    }
}
