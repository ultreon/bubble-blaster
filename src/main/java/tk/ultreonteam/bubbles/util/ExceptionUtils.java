package tk.ultreonteam.bubbles.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
    private ExceptionUtils() {
        throw ExceptionUtils.utilityClass();
    }

    public static UnsupportedOperationException utilityClass() {
        return new UnsupportedOperationException("Tried to initialize utility class.");
    }

    public static String getStackTrace() {
        return getStackTrace(new RuntimeException());
    }

    public static String getStackTrace(String message) {
        return getStackTrace(new RuntimeException(message));
    }

    public static String getStackTrace(Throwable throwable) {
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        throwable.printStackTrace(printWriter);
        printWriter.flush();
        String stackTrace = writer.toString();
        printWriter.close();
        return stackTrace;
    }
}
