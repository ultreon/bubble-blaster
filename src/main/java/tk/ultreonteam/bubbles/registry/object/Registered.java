package tk.ultreonteam.bubbles.registry.object;

import tk.ultreonteam.bubbles.common.IRegistrable;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.registry.Registry;

import java.util.function.Supplier;

@SuppressWarnings({"rawtypes", "unchecked"})
public class Registered<B extends IRegistrable> {
    private final Registry registry;
    private final Supplier<B> supplier;
    private final Identifier identifier;

    public <T extends B> Registered(Registry<?> registry, Supplier<B> supplier, Identifier identifier) {
        this.registry = registry;
        this.supplier = supplier;
        this.identifier = identifier;
    }

    public void register() {
        registry.register(identifier, (Registrable) ((Supplier) supplier).get());
    }

    @SuppressWarnings("unchecked")
    public B get() {
        return (B) registry.get(identifier);
    }

    public Identifier id() {
        return identifier;
    }
}
