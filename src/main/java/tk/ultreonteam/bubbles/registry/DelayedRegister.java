package tk.ultreonteam.bubbles.registry;

import tk.ultreonteam.bubbles.common.IRegistrable;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.registry.RegistryEvent;
import tk.ultreonteam.bubbles.registry.object.Registered;
import org.apache.logging.log4j.LogManager;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Supplier;

public class DelayedRegister<@NonNull T extends IRegistrable> {
    @NonNull
    private final String modId;
    @NonNull
    private final Registry<T> registry;
    private final ArrayList<HashMap.Entry<Identifier, Supplier<T>>> objects = new ArrayList<>();

    protected DelayedRegister(@NonNull String modId, @NonNull Registry<T> registry) {
        this.modId = modId;
        this.registry = registry;
    }

    public static <T extends Registrable> DelayedRegister<T> create(String modId, Registry<T> registry) {
        return new DelayedRegister<>(modId, registry);
    }

    public <@NonNull C extends T> Registered<C> register(@NonNull String key, @NonNull Supplier<@NonNull C> supplier) {
        Identifier rl = new Identifier(key, modId);

        objects.add(new HashMap.SimpleEntry<>(rl, supplier::get));

        return new Registered<>(registry, supplier, rl);
    }

    public void register(@NonNull GameEvents events) {
        LogManager.getLogger("Registration").info("Mod " + modId + " subscribes for register events of type: " + registry.getType().getName());
        events.subscribe(this);
    }

    @SuppressWarnings("unused")
    @SubscribeEvent
    public void onRegister(RegistryEvent.@NonNull Register<@NonNull T> event) {
        if (!event.getRegistry().getType().equals(registry.getType())) {
            return;
        }

        LogManager.getLogger("Registration").info("Mod " + modId + " registration for: " + registry.getType().getName());

        for (HashMap.Entry<Identifier, Supplier<T>> entry : objects) {
            T object = entry.getValue().get();
            Identifier rl = entry.getKey();

            if (!event.getRegistry().getType().isAssignableFrom(object.getClass())) {
                throw new IllegalArgumentException("Got invalid type in deferred register: " + object.getClass() + " expected assignable to " + event.getRegistry().getType());
            }

            event.getRegistry().register(rl, object);
            object.setId(rl);
        }
    }
}
