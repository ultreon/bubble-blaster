package tk.ultreonteam.bubbles.data;

import tk.ultreonteam.bubbles.References;
import net.querz.nbt.tag.CompoundTag;

import java.io.File;
import java.io.IOException;

/**
 * This class is used to save and load data from the global save file.
 * <p>
 *     It's a {@link CompoundTag} that contains all the data that is saved in the game.
 *     Also, global save file is located at {@code global.dat} (at the root of the game save directory).
 * </p>
 * @author ultreon
 * @since 0.1.0
 */
@SuppressWarnings("unused")
public final class GlobalSaveData extends GameData {
    private static GlobalSaveData instance = new GlobalSaveData();
    private double highScore = 0.0;
    private long highScoreTime = 0L;

    /**
     * File to the global save data.
     * @since 0.1.0
     */
    public static final File FILE = new File(References.GAME_DIR, "global.dat");

    /**
     * Get the global save data instance.
     *
     * @return The global save data instance.
     * @since 0.1.0
     * @author Qboi123
     */
    public static GlobalSaveData instance() {
        return instance;
    }

    private GlobalSaveData() {
        instance = this;
    }

    @Override
    protected void load(CompoundTag tag) {
        highScore = tag.getDouble("HighScore");
        highScoreTime = tag.getLong("HighScoreTime");
    }

    public void load() throws IOException {
        if (!FILE.exists()) {
            create();
        }
        this.load(FILE);
    }

    public CompoundTag dump(CompoundTag tag) {
        tag.putDouble("HighScore", highScore);
        tag.putLong("HighScoreTime", highScoreTime);
        return tag;
    }

    public void dump() throws IOException {
        dump(FILE);
    }

    /**
     * Get the high score.
     *
     * @return the high score.
     * @since 0.1.0
     * @author Qboi123
     */
    public double getHighScore() {
        return highScore;
    }

    /**
     * Get the time the high score was set.
     *
     * @return the time the high score was set.
     * @since 0.1.0
     * @author Qboi123
     */
    public long getHighScoreTime() {
        return highScoreTime;
    }

    /**
     * Set the high score.
     *
     * @param highScore the high score.
     * @param time the time the high score was set.
     * @since 0.1.0
     * @author Qboi123
     */
    public void setHighScore(double highScore, long time) {
        if (this.highScore < highScore) {
            this.highScore = highScore;
            this.highScoreTime = time;

            try {
                dump();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Check if the data file exists.
     *
     * @return true if the data file exists.
     * @since 0.1.0
     * @author Qboi123
     */
    public boolean isCreated() {
        return FILE.exists();
    }

    /**
     * Check if the data file doesn't exist.
     *
     * @return true if the data file doesn't exist.
     * @since 0.1.0
     * @author Qboi123
     */
    public boolean isNotCreated() {
        return !FILE.exists();
    }

    /**
     * Create the data file.
     *
     * @throws IOException if an I/O error occurs while creating the data file.
     * @since 0.1.0
     * @author Qboi123
     */
    public void create() throws IOException {
        if (isNotCreated()) {
            dump();
        }
    }
}
