package tk.ultreonteam.bubbles.data;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.vector.Vec2d;
import net.querz.nbt.tag.CompoundTag;

public class DataManager {
    public CompoundTag storeEntity(Entity entity) {
        CompoundTag nbt = new CompoundTag();
        nbt.put("position", storePosition(entity.getPos()));
        nbt.put("data", entity.save());
        return nbt;
    }

    private CompoundTag storePosition(Vec2d pos) {
        CompoundTag nbt = new CompoundTag();
        nbt.putDouble("x", pos.getX());
        nbt.putDouble("y", pos.getY());
        return nbt;
    }
}
