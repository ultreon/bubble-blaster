package tk.ultreonteam.bubbles.command;

import tk.ultreonteam.bubbles.ingame.entity.player.Player;

public class BloodMoonCommand implements CommandExecutor {
    @Override
    public boolean execute(Player player, String[] args) {
        player.getEnvironment().triggerBloodMoon();
        return true;
    }
}
