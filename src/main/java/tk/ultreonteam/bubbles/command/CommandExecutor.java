package tk.ultreonteam.bubbles.command;

import tk.ultreonteam.bubbles.ingame.entity.player.Player;

public interface CommandExecutor {
    @SuppressWarnings("UnusedReturnValue")
    boolean execute(Player player, String[] args);
}
