package tk.ultreonteam.bubbles.command;

import tk.ultreonteam.bubbles.ingame.entity.player.Player;

public class HealCommand implements CommandExecutor {
    @Override
    public boolean execute(Player player, String[] args) {
        player.setDamageValue(player.getMaxDamageValue());
        return true;
    }
}
