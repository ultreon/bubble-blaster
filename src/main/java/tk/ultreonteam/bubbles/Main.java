package tk.ultreonteam.bubbles;

import tk.ultreonteam.preloader.PreClassLoader;
import tk.ultreonteam.preloader.PreGameLoader;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * The actual main class.
 * Btw, not the main startup-class that's from the Manifest: {@linkplain PreGameLoader}
 *
 * @see PreGameLoader
 * @author Qboi
 * @since 0.0.1
 */
public class Main {
    /**
     * Main class loader, class loader that loads the main class.
     * @since 0.0.1
     */
    public static PreClassLoader mainClassLoader;

    /**
     * @deprecated RPC is moved to {@link BubbleBlaster}
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public static boolean rpcReady;

    /**
     * The main method.
     * Main static method that is called by the JVM.
     * The game will start here.
     *
     * @param args the arguments
     * @since 0.0.1
     * @author Qboi
     */
    @SuppressWarnings("unused")
    public static void main(String[] args, PreClassLoader launchClassLoader) {
        System.setProperty("log4j2.formatMsgNoLookups", "true"); // Fix CVE-2021-44228 exploit.

        // Invoke using Swing Utilities to prevent issues occurring.
        try {
            SwingUtilities.invokeAndWait(() -> {
                mainClassLoader = launchClassLoader;

                try {
                    // Load the game class.
                    Class<?> gameClass = mainClassLoader.loadClass("tk.ultreonteam.bubbles.BubbleBlaster");
                    Method launchMethod = gameClass.getMethod("main", String[].class, PreClassLoader.class);

                    // Invoke the game's main method.
                    launchMethod.invoke(null, args, mainClassLoader);

                    // No fatal error occurred, so exit with code 0 (success).
    //                System.exit(0);
                    return;
                } catch (ClassNotFoundException e) {
                    // The game class was not found.
                    System.err.println("Cannot find the game class: " + BubbleBlaster.class.getName());
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    // The game class does not have a main method.
                    System.err.println("Cannot find main method in the game class: " + BubbleBlaster.class.getName());
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // Unable to access the game class or main method.
                    System.err.println("Cannot access main method in the game class: " + BubbleBlaster.class.getName());
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // The main method threw an exception.
                    System.err.println("Cannot invoke main method in the game class: " + BubbleBlaster.class.getName());
                    e.getCause().printStackTrace();
                } catch (Exception e) {
                    // An unknown exception occurred.
                    System.err.println("An unknown exception occurred while loading the game class: " + BubbleBlaster.class.getName());
                    e.printStackTrace();
                }

                // If we get here, something went wrong.
                System.exit(1);
            });
        } catch (InterruptedException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
