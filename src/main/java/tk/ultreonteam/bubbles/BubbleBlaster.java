package tk.ultreonteam.bubbles;

import com.google.common.annotations.Beta;
import de.jcm.discordgamesdk.Core;
import de.jcm.discordgamesdk.CreateParams;
import de.jcm.discordgamesdk.GameSDKException;
import de.jcm.discordgamesdk.activity.Activity;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
import org.checkerframework.common.value.qual.IntRange;
import org.checkerframework.common.value.qual.IntVal;
import org.fusesource.jansi.AnsiConsole;
import tk.ultreonteam.bubbles.command.*;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.common.Ticker;
import tk.ultreonteam.bubbles.common.exceptions.ResourceNotFoundException;
import tk.ultreonteam.bubbles.common.streams.CustomOutputStream;
import tk.ultreonteam.bubbles.data.GlobalSaveData;
import tk.ultreonteam.bubbles.debug.DebugRenderer;
import tk.ultreonteam.bubbles.event.*;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.bus.ModEvents;
import tk.ultreonteam.bubbles.event.input.KeyPressEvent;
import tk.ultreonteam.bubbles.event.load.LoadCompleteEvent;
import tk.ultreonteam.bubbles.event.registry.RegistryEvent;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.ingame.EnvironmentRenderer;
import tk.ultreonteam.bubbles.ingame.entity.bubble.BubbleSystem;
import tk.ultreonteam.bubbles.ingame.entity.player.InputController;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.player.PlayerController;
import tk.ultreonteam.bubbles.ingame.gamemode.Gamemode;
import tk.ultreonteam.bubbles.init.Gamemodes;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.media.MP3Player;
import tk.ultreonteam.bubbles.media.Sound;
import tk.ultreonteam.bubbles.media.SoundPlayer;
import tk.ultreonteam.bubbles.mod.ModContainer;
import tk.ultreonteam.bubbles.mod.loader.ModLoader;
import tk.ultreonteam.bubbles.mod.loader.ModManager;
import tk.ultreonteam.bubbles.mod.loader.Scanner;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.Registry;
import tk.ultreonteam.bubbles.registry.object.ObjectHolder;
import tk.ultreonteam.bubbles.render.*;
import tk.ultreonteam.bubbles.render.screen.MessengerScreen;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.render.screen.ScreenManager;
import tk.ultreonteam.bubbles.render.screen.TitleScreen;
import tk.ultreonteam.bubbles.resources.Resource;
import tk.ultreonteam.bubbles.resources.ResourceManager;
import tk.ultreonteam.bubbles.save.GameSave;
import tk.ultreonteam.bubbles.util.Util;
import tk.ultreonteam.commons.crash.ApplicationCrash;
import tk.ultreonteam.commons.crash.CrashLog;
import tk.ultreonteam.commons.lang.Messenger;
import tk.ultreonteam.commons.lang.ProgressMessenger;
import tk.ultreonteam.commons.time.TimeProcessor;
import tk.ultreonteam.commons.util.FileUtils;
import tk.ultreonteam.dev.DevClassPath;
import tk.ultreonteam.dev.GameDevMain;
import tk.ultreonteam.preloader.PreClassLoader;

import javax.annotation.ParametersAreNonnullByDefault;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.List;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;

/**
 * The Bubble Blaster game main class.
 *
 * @since 0.0.1-indev1
 */
@ParametersAreNonnullByDefault
@SuppressWarnings({"ResultOfMethodCallIgnored", "unused", "RedundantSuppression"})
public final class BubbleBlaster {
    public static final int TPS = 20;
    public static final String NAMESPACE = "tk.ultreonteam.bubbles";
    public static final String CODE_SOURCE;
    public static final File CODE_SOURCE_FILE;

    // Values
    @IntRange(from = 0)
    private int fps;
    @IntVal(20)
    private final int tps;

    // Modes
    private static boolean debugMode;
    private static boolean devMode;

    // Utility objects.
    public InputController input;
    private PlayerController playerController;
    @NonNull
    private final ResourceManager resourceManager;
    @NonNull
    private final GameWindow window;
    @NonNull
    private final ScreenManager screenManager;
    @NonNull
    private final RenderSettings renderSettings;

    // Game states.
    private boolean loaded;
    private boolean crashed;
    private volatile boolean running = false;

    // Tasks
    private final List<Runnable> tasks = new CopyOnWriteArrayList<>();

    // Threads
    private Thread mainThread;
    private float renderPartialTicks;
    private boolean stopping;

    // Initial game information / types.
    private static ClassLoader mainClassLoader;
    private static SoundPlayer soundPlayer;
    private static File gameDir = null;

    // Number values.
    private static long ticks = 0L;

    // Environment
    @Nullable
    public Environment environment;

    // Fonts.
    private final Font sansFont;
    private Font monospaceFont;
    private Font pixelFont;
    private Font gameFont;

    // Cursors
    private final Cursor blankCursor;
    private final Cursor defaultCursor;
    private final Cursor pointerCursor;
    private final Cursor textCursor;

    // Font names.
    private final String fontName;

    // Rendering
    private final DebugRenderer debugRenderer = new DebugRenderer(this);
    private final EnvironmentRenderer environmentRenderer;

    // Managers.
    private final ModManager modManager = ModManager.instance();
    private final TextureManager textureManager = TextureManager.instance();

    // logger.
    private static final Logger logger = LogManager.getLogger("Generic");

    // Running value.

    // Randomizers.
    private final Random random = new Random();

    // Loaded game.
    private LoadedGame loadedGame;

    // Misc
    private final BufferedImage background = null;
    private boolean debugGuiOpen = false;
    private GlitchRenderer glitchRenderer = null;
    private boolean isGlitched = false;

    // Threads
    private Thread renderThread;
    private Thread gcThread;

    // Player entity
    public Player player;

    // Instance
    private static BubbleBlaster instance;
    private Supplier<Activity> activity;
    private final Object rpcUpdateLock = new Object();
    private volatile boolean rpcUpdated;
    private LoadingGui loadingGui;
    private ModLoader modLoader;

    public static BubbleBlaster getInstance() {
        return instance;
    }

    private final Ticker ticker = new Ticker();

    static {
        URL url = BubbleBlaster.class.getProtectionDomain().getCodeSource().getLocation();
        CODE_SOURCE = URLDecoder.decode(url.toString(), Charset.defaultCharset());
        try {
            CODE_SOURCE_FILE = new File(url.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Class constructor for Bubble Blaster.
     * <b>Note:</b> <i>This constructor is not meant to be called directly.</i>
     *
     * @see LoadingGui
     * @since 0.0.1-indev1
     * @author Qboi
     */
    public BubbleBlaster() {
        GameWindow.Properties windowProperties = new GameWindow.Properties("Bubble Blaster", 1280, 720).fullscreen();
        BubbleBlaster.BootOptions bootOptions = new BootOptions().tps(20);
        // Set default uncaught exception handler.
        Thread.setDefaultUncaughtExceptionHandler(new GameExceptions(this));

        // Assign instance.
        instance = this;
        // Set game properties.
        this.tps = bootOptions.tps;

        // Prepare for game launch
        this.resourceManager = new ResourceManager();
        this.renderSettings = new RenderSettings();

        // Setup game window.
        this.window = new GameWindow(windowProperties);

        // Prepare for loading.
        this.prepare();

        // Load game with loading screen.
        this.load(new ProgressMessenger(new Messenger(this::log), 1000));
        this.screenManager = createScreenManager();
        BubbleBlaster.instance = this;

        setActivity(() -> {
            Activity activity = new Activity();
            activity.setState("Loading game.");
            return activity;
        });

        // Initialize Discord RPC.
        Thread rpcThread = new Thread(this::rpc);
        rpcThread.setDaemon(true);
        rpcThread.start();

        logger.info("Discord RPC is initializing!");

        // Set the renderers.
        environmentRenderer = new EnvironmentRenderer();

        // Subscribe to events.
        GameEvents.get().subscribe(this);

        // Do development class path stuff.
        DevClassPath classPath = GameDevMain.getClassPath();
        try {
            File file = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
            if (file.isDirectory()) {
                resourceManager.importResources(new File(file, "../../../resources/main"));
            } else {
                resourceManager.importResources(file);
            }
        } catch (Exception e) {
            if (classPath != null) {
                for (File file : classPath.getFiles()) {
                    logger.warn(file);
                    resourceManager.importResources(file);
                }
            } else {
                try {
                    throw new FileNotFoundException("Can't find bubble blaster game executable.");
                } catch (FileNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        }


        // Override the default thread names.
        for (Thread thread : Thread.getAllStackTraces().keySet()) {
            if (thread.getName().equals("JavaFX Application Thread")) {
                thread.setName("Bubble-Blaster-App");
            }
            if (thread.getName().equals("AWT-EventQueue-0")) {
                thread.setName("Native-Events");
            }
        }

        // Add ansi color compatibility in console.
        AnsiConsole.systemInstall();

        // We'll need to set the current working directory to the game directory.
        FileUtils.setCwd(References.GAME_DIR);

        // Transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        // Create a new blank cursor.
        blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg, new Point(0, 0), "blank cursor");

        // Transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg1 = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
        Polygon polygon = new Polygon(new int[]{0, 10, 5, 0}, new int[]{0, 12, 12, 16}, 4);

        Graphics2D gg = cursorImg1.createGraphics();
        gg.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gg.setColor(Color.black);
        gg.fillPolygon(polygon);
        gg.setColor(Color.white);
        gg.drawPolygon(polygon);
        gg.dispose();

        // Create a new blank cursor.
        defaultCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg1, new Point(1, 1), "default cursor");

        // Transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg2 = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
        Polygon polygon2 = new Polygon(new int[]{10, 20, 15, 10}, new int[]{10, 22, 22, 26}, 4);

        Graphics2D gg2 = cursorImg2.createGraphics();
        gg2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gg2.setColor(Color.white);
        gg2.drawOval(0, 0, 20, 20);
        gg2.setColor(Color.white);
        gg2.drawOval(2, 2, 16, 16);
        gg2.setColor(Color.black);
        gg2.fillPolygon(polygon2);
        gg2.setColor(Color.white);
        gg2.drawPolygon(polygon2);
        gg2.setColor(Color.black);
        gg2.drawOval(1, 1, 18, 18);
        gg2.dispose();

        // Create a new blank cursor.
        pointerCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg2, new Point(11, 11), "pointer cursor");

        // Transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg3 = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gg3 = cursorImg3.createGraphics();
        gg3.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gg3.setColor(Color.white);
        gg3.drawLine(0, 1, 0, 24);
        gg3.setColor(Color.white);
        gg3.drawLine(1, 0, 1, 25);
        gg3.setColor(Color.white);
        gg3.drawLine(2, 1, 2, 24);
        gg3.setColor(Color.black);
        gg3.drawLine(1, 1, 1, 24);
        gg3.dispose();

        // Create a new blank cursor.
        textCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg3, new Point(1, 12), "text cursor");

        // Hook output for logger.
        System.setErr(new PrintStream(new CustomOutputStream("STDERR", Level.ERROR), true));
        System.setOut(new PrintStream(new CustomOutputStream("STDOUT", Level.INFO), true));

        // Logs directory creation.
        References.LOGS_DIR.mkdirs();

        // Font Name
        fontName = "Chicle Regular";

        sansFont = loadFontInternally(res("ArialUnicode"));
        loadFontInternally(res("Pixel/PressStartK"));

        // Register Game Font.
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        // Create instance field in class.

        // Request focus
        getGameWindow().requestFocus();

        // Register event bus.
        getEventBus().subscribe(this);

        loadingGui = new LoadingGui(this);
        loadingGui.init();

        // Initialize the game.
        Thread initThread = new Thread(() -> {
            try {
                initialize();
            } catch (Exception e) {
                logger.error("Error initializing game.", e);
                crash(e);
            }
        });
        initThread.start();

        // Start scene-manager.
        try {
            Objects.requireNonNull(this.getScreenManager()).start();
        } catch (Throwable t) {
            CrashLog crashLog = new CrashLog("Oops, game crashed!", t);
            crash(t);
        }
    }

    /**
     * Loop method for Discord Rich Presence.
     */
    @SuppressWarnings("BusyWait")
    private void rpc() {
        // Download the discord-rpc library.
        File discordLibrary;
        try {
            discordLibrary = DownloadNativeLibrary.downloadDiscordLibrary();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        if (discordLibrary == null) {
            // Download failed.
            System.err.println("Error downloading Discord SDK.");
            System.exit(-1); // Exit the game with error code -1 (error).
        }

        // Initialize the Core
        Core.init(discordLibrary);

        // Set parameters for the Core
        try (CreateParams params = new CreateParams()) {
            params.setClientID(933147296311427144L);
            params.setFlags(CreateParams.getDefaultFlags());

            // Create the Core
            try (Core core = new Core(params)) {
                // Create the Activity
                try (Activity activity = new Activity()) {
                    activity.setDetails("Developer mode");
                    activity.setState("RPC Testing");

                    // Setting a start time causes an "elapsed" field to appear
                    activity.timestamps().setStart(Instant.now());

                    // Make a "cool" image show up
                    activity.assets().setLargeImage("icon");

                    // Finally, update the current activity to our activity
                    core.activityManager().updateActivity(activity);
                }

                // Run callbacks forever
                while (true) {
                    core.runCallbacks();
                    synchronized (rpcUpdateLock) {
                        if (rpcUpdated) {
                            rpcUpdated = false;
                            core.activityManager().updateActivity(activity.get());
                        }
                    }
                    try {
                        // Sleep for a bit to save CPU.
                        Thread.sleep(16);
                    } catch (InterruptedException e) {
                        // Got interrupted, so we're done.
                        try {
                            // Clean up the Discord RPC library.
                            core.close();
                        } catch (GameSDKException ignored) {
                            // Just putting a comment here to make the IDE happy.
                        }
                        return;
                    }
                }
            }
        }
    }

    /**
     * Update the Discord Rich Presence.
     *
     * @since 0.1.0
     * @author Qboi
     * @deprecated Use {@link #setActivity(Supplier)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void updateRPC() {

    }

    /**
     * Get the current RPC activity.
     *
     * @return The current RPC activity.
     * @since 0.1.0
     * @author Qboi
     */
    public Supplier<Activity> getActivity() {
        return activity;
    }

    /**
     * Set the current RPC activity.
     *
     * @param activity The new RPC activity.
     * @since 0.1.0
     * @author Qboi
     */
    public void setActivity(Supplier<Activity> activity) {
        synchronized (rpcUpdateLock) {
            this.activity = () -> {
                Activity ret = activity.get();
                ret.assets().setLargeImage("icon");
                return ret;
            };
            rpcUpdated = true;
        }
    }

    private void initialGameTick() {

    }

    private void initDiscord() {

    }

    @SuppressWarnings("DuplicatedCode")
    private void tickThread() {
        double tickCap = 1f / (double) getTps();

        double time = TimeProcessor.getTime();
        double unprocessed = 0;

        try {
            while (running) {
                boolean canTick = false;

                double time2 = TimeProcessor.getTime();
                double passed = time2 - time;
                unprocessed += passed;

                time = time2;

                while (unprocessed >= tickCap) {
                    unprocessed -= tickCap;

                    canTick = true;
                }

                if (canTick) {
                    try {
                        internalTick();
                    } catch (Throwable t) {
                        CrashLog crashLog = new CrashLog("Game being ticked.", t);
                        crash(crashLog.createCrash());
                    }
                }
            }
        } catch (Throwable t) {
            CrashLog crashLog = new CrashLog("Running game loop.", t);
            crash(crashLog.createCrash());
        }

        close();
    }

    private Font loadFontInternally(Identifier location) {
        try (InputStream inputStream = getClass().getResourceAsStream("/Assets/" + location.location() + "/Fonts/" + location.path() + ".ttf")) {
            if (inputStream == null) {
                throw new ResourceNotFoundException("Font resource " + location + " doesn't exists.");
            }
            Font font = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            Hydro.get().registerFont(font);
            return font;
        } catch (IOException | FontFormatException e) {
            throw new IOError(e);
        }
    }

    private Font loadFont(Identifier font) throws FontFormatException {
        Identifier identifier = new Identifier("Fonts/" + font.path() + ".ttf", font.location());
        Resource resource = resourceManager.getResource(new Identifier("Fonts/" + font.path() + ".ttf", font.location()));
        if (resource != null) {
            return resource.loadFont();
        } else {
            throw new NullPointerException("Resource is null: " + identifier);
        }
    }

    /**
     * <b>Note:</b> <i>This is an internal method and shouldn't be called.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void loadFonts() {
        try {
            gameFont = loadFont(res("Chicle"));
            pixelFont = loadFont(res("Pixel"));
            monospaceFont = loadFont(res("Dejavu/DejavuSansMono"));
        } catch (FontFormatException | NullPointerException e) {
            if (e instanceof NullPointerException) {
                System.err.println("Couldn't load fonts.");
            }
            e.printStackTrace();
        }
    }

    /**
     * Create a new {@link Identifier} from the path, and Bubble Blaster's ID as its location.
     *
     * @param path The identifier's path.
     * @return The identifier.
     */
    private static Identifier res(String path) {
        return new Identifier(path, NAMESPACE);
    }

    /**
     * <b>Note:</b> <i>This is an internal method and shouldn't be called.</i>
     *
     * @since 0.0.1
     * @author Qboi
     * @deprecated Removed usage.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void gcThread() {
        System.gc();
    }

    /**
     * Stops game-thread.
     *
     * @since 0.1.0
     * @author Qboi
     */
    @RequiresNonNull({"mainThread", "renderThread", "gcThread"})
    public synchronized void stop() {
        try {
            renderThread.join();
            mainThread.join();
            gcThread.join();
            running = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to handle closing of the game.<br>
     * <b>Note:</b> <i>This is an internal method and shouldn't be called.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void onClose() {
        // Shut-down game.
        logger.info("Shutting down Bubble Blaster");
        checkForExitEvents();

    }

    public int getScaledWidth() {
//        return (int) (getWidth() * renderSettings.getScale());
        return getWidth();
    }

    public int getScaledHeight() {
//        return (int) (getHeight() * renderSettings.getScale());
        return getHeight();
    }

    private void checkForExitEvents() {
        getEventBus().publish(new ExitEvent());
    }

    /**
     * Launch method.
     * Contains argument parsing.
     *
     * @param args The arguments.
     * @param mainClassLoader The main class loader.
     * @since 0.1.0
     * @author Qboi
     */
    public static void main(String[] args, PreClassLoader mainClassLoader) {
        System.setProperty("log4j2.formatMsgNoLookups", "true"); // Fix CVE-2021-44228 exploit.
        // Get game-directory.
        for (String arg : args) {
            if (arg.startsWith("gameDir=")) {
                BubbleBlaster.gameDir = new File(arg.substring(8));
            }
            if (arg.equals("--debug")) {
                BubbleBlaster.debugMode = true;
            }
            if (arg.equals("--dev")) {
                BubbleBlaster.devMode = true;
            }
        }

        // Check if game-dir is assigned, if not the game-dir is not specified in the arguments.
        if (getGameDir() == null) {
            System.err.println("Game Directory is not specified!");
            System.exit(1);
        }

        BubbleBlaster.mainClassLoader = mainClassLoader;

        // Boot the game.
        BubbleBlaster.initEngine(BubbleBlaster.debugMode, BubbleBlaster.devMode);
        new BubbleBlaster();
    }

    /**
     * Todo: implement save loading.
     *
     * @param saveName save to load
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("EmptyMethod")
    public void loadSave(String saveName) {
        createGame(new Random().nextLong());
    }


    /**
     * Todo: implement save loading.
     *
     * @param save save to load
     * @since 0.1.0
     * @author Qboi
     */
    public void loadSave(GameSave save) {
        createGame(new Random().nextLong());
    }

    /**
     * Todo: implement save creation and loading..
     *
     * @param saveName save to create and load
     * @since 0.1.0
     * @author Qboi
     */
    @Beta
    @SuppressWarnings("EmptyMethod")
    public void createAndLoadSave(String saveName) {

    }

    /**
     * Change to a new screen.
     * This will also initialize the screen with {@link Screen#init()}.
     *
     * @param screen the screen to switch to.
     * @since 0.1.0
     * @author Qboi
     * @see Screen#init()
     * @see ScreenManager
     */
    public void showScreen(@Nullable Screen screen) {
        this.screenManager.displayScreen(screen);
    }

    /**
     * Change tp a new screen, and optionally force to change to that screen.
     * This will also initialize the screen with {@link Screen#init()}.
     *
     * @param screen the screen to switch to.
     * @param force  whether to force switching or not.
     * @since 0.1.0
     * @author Qboi
     * @see Screen#init()
     * @see ScreenManager
     */
    public void showScreen(Screen screen, boolean force) {
        this.screenManager.displayScreen(screen, force);
    }

    /**
     * Get the currently shown screen.
     *
     * @return the currently shown screen.
     * @since 0.1.0
     * @author Qboi
     * @see ScreenManager
     */
    public @Nullable Screen getCurrentScreen() {
        return this.screenManager.getCurrentScreen();
    }

    /**
     * Run a task later.
     * Note: this will call the instance variant of this method (see {@link #scheduleTask(Runnable)})
     *
     * @param task the task to run.
     * @since 0.1.0
     * @author Qboi
     * @see #scheduleTask(Runnable)
     */
    public static void runLater(Runnable task) {
        instance.scheduleTask(task);
    }

    /**
     * Load the game environment.
     *
     * @since 0.1.0
     * @author Qboi
     * @deprecated use the one with the seed instead.
     */
    @Deprecated
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void createGame() {
        createGame(512L);
    }

    /**
     * Load the game environment.
     *
     * @param seed generator seed
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void createGame(long seed) {
        createGame(seed, Gamemodes.CLASSIC.get());
    }

    /**
     * Create a new saved game.
     *
     * @param seed     generator seed.
     * @param gamemode game mode to use.
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void createGame(long seed, Gamemode gamemode) {
        startGame(seed, gamemode, GameSave.fromFile(new File(References.SAVES_DIR, "save")), true);
    }

    /**
     * Loads the default saved game.
     *
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void loadGame() throws IOException {
        loadGame(GameSave.fromFile(new File(References.SAVES_DIR, "save")));
    }

    /**
     * Load a saved game.
     *
     * @param save the game save to load.
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void loadGame(GameSave save) throws IOException {
        long seed = save.getSeed();
        Gamemode gamemode = save.getGamemode();
        startGame(seed, gamemode, save, true);
    }

    /**
     * Start the game environment, with the given seed and gamemode.
     * This will show a loading screen, create save data, initialize the environment and other in-game components.
     *
     * @param seed generator seed
     * @param gamemode game mode to use.
     * @param save the game save to load / create.
     * @param create whether to create a new save or not.
     * @since 0.1.0
     * @author Qboi
     * @see GameSave
     * @see LoadedGame
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public void startGame(long seed, Gamemode gamemode, GameSave save, boolean create) {
        // Start loading.
        MessengerScreen screen = new MessengerScreen();

        // Show environment loader screen.
        showScreen(screen);
        try {
            // Todo: make saves persistent.
            // Delete existent save.
            File directory = save.getDirectory();
            if (create && directory.exists()) {
                org.apache.commons.io.FileUtils.deleteDirectory(directory);
            }

            // Create save directory.
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                    throw new IOException("Creating save folder failed.");
                }
            }

            // Initialize environment.
            Environment environment = this.environment = new Environment(save, gamemode, seed);

            if (create) {
                // Create save data.
                screen.setDescription("Preparing creation");
                environment.prepareCreation(save);
                screen.setDescription("Initializing environment");
                environment.initSave(screen.getMessenger());
                screen.setDescription("Saving initialized save");
                environment.save(save, screen.getMessenger());
            } else {
                // Load save data.
                screen.setDescription("Loading data...");
                gamemode = Gamemode.loadState(save, screen.getMessenger());
                environment.load(save, screen.getMessenger());
            }

            // Start the loaded game.
            LoadedGame loadedGame = new LoadedGame(save, this.environment);
            loadedGame.start();

            // Final things.
            this.loadedGame = loadedGame;
        } catch (Throwable t) {
            t.printStackTrace();
            CrashLog crashLog = new CrashLog("Game save being loaded", t);
            crashLog.add("Save Directory", save.getDirectory());
            crashLog.add("Current Description", screen.getDescription());
            crashLog.add("Create Flag", create);
            crashLog.add("Seed", seed);
            crashLog.add("Gamemode", gamemode.id());
            crash(crashLog.createCrash());
            return;
        }

        BubbleBlaster.getInstance().showScreen(null);
    }

    /**
     * Quit game environment and loaded game.
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void quitLoadedGame() {
        LoadedGame loadedGame = getLoadedGame();
        if (loadedGame != null) {
            loadedGame.quit();
        }

        this.loadedGame = null;

        showScreen(new TitleScreen());
    }

    //********************//
    //     Game flags     //
    //********************//
    /**
     * Checks if the current thread is the main thread.
     *
     * @return true if the method is called on the main thread.
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isOnMainThread() {
        return Thread.currentThread() == mainThread;
    }

    /**
     * Checks if the current thread is the rendering thread.
     *
     * @return whether the method is called on the rendering thread or not.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isOnRenderThread() {
        return Thread.currentThread() == renderThread;
    }

    /**
     * Checks if the game environment is active.
     * Note that this will return true even if the game is paused.
     *
     * @return whether the environment is active, even if game is paused.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isInGame() {
        return getLoadedGame() != null && environment != null;
    }

    /**
     * Checks if the game environment isn't running. Aka, is in menus like the title screen.
     *
     * @return whether the user is in menus.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isInMainMenus() {
        return getLoadedGame() == null && environment == null;
    }

    //******************//
    //     Managers     //
    //******************//
    /**
     * Get the screen manager.
     *
     * @return the screen manager.
     * @since 0.1.0
     * @author Qboi
     */
    @NonNull
    public ScreenManager getScreenManager() {
        return screenManager;
    }

    /**
     * Get the resource manager.
     *
     * @return the resource manager.
     * @since 0.1.0
     * @author Qboi
     */
    @NonNull
    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    /**
     * Get the texture manager.
     *
     * @return the texture manager.
     * @since 0.1.0
     * @author Qboi
     */
    public TextureManager getTextureManager() {
        return textureManager;
    }


    //*********************//
    //     Loaded Game     //
    //*********************//
    /**
     * Get the loaded game.
     *
     * @return the loaded game.
     * @since 0.1.0
     * @author Qboi
     */
    public @Nullable LoadedGame getLoadedGame() {
        return loadedGame;
    }

    /**
     * Get the environment.
     *
     * @return the in-game environment.
     * @since 0.1.0
     * @author Qboi
     */
    public @Nullable Environment getEnvironment() {
        return environment;
    }

    /**
     * Get the currently loaded save.
     *
     * @return the loaded save.
     * @since 0.1.0
     * @author Qboi
     */
    public @Nullable GameSave getCurrentSave() {
        LoadedGame loadedGame = getLoadedGame();
        if (loadedGame != null) {
            return loadedGame.getGameSave();
        }
        return null;
    }

    //***************//
    //     Fonts     //
    //***************//
    /**
     * Get the sans serif font.
     *
     * @return the sans serif font.
     * @since 0.1.0
     * @author Qboi
     */
    public Font getSansFont() {
        return sansFont;
    }

    /**
     * Get the monospace font.
     *
     * @return the monospace font.
     * @since 0.1.0
     * @author Qboi
     */
    public Font getMonospaceFont() {
        return monospaceFont;
    }

    /**
     * Get the pixel art font.
     *
     * @return the pixel art font.
     * @since 0.1.0
     * @author Qboi
     */
    public Font getPixelFont() {
        return pixelFont;
    }

    /**
     * Get the game's bubble font.
     *
     * @return the bubble font.
     * @since 0.1.0
     * @author Qboi
     */
    public Font getGameFont() {
        return gameFont;
    }

    //*****************//
    //     Cursors     //
    //*****************//
    /**
     * Get the transparent empty cursor.
     * This is used for when the cursor is hidden.
     *
     * @return the blank cursor.
     * @since 0.1.0
     * @author Qboi
     */
    public Cursor getBlankCursor() {
        return blankCursor;
    }

    /**
     * Get the text cursor.
     * This cursor is used for text input.
     *
     * @return the text cursor.
     * @since 0.1.0
     * @author Qboi
     */
    public Cursor getTextCursor() {
        return textCursor;
    }

    /**
     * Get the pointer cursor.
     * This cursor is used for hovering over buttons or other clickable elements.
     *
     * @return the pointer cursor.
     * @since 0.1.0
     * @author Qboi
     */
    public Cursor getPointerCursor() {
        return pointerCursor;
    }

    /**
     * Get the default cursor.
     * The ordinary cursor, used for most elements.
     *
     * @return the default cursor.
     * @since 0.1.0
     * @author Qboi
     */
    public Cursor getDefaultCursor() {
        return defaultCursor;
    }

    /**
     * Get the internal font name.
     *
     * @return the internal font name.
     * @since 0.1.0
     * @author Qboi
     */
    public String getFontName() {
        return fontName;
    }

    //*******************//
    //     Rendering     //
    //*******************//
    /**
     * Get the rendering settings.
     *
     * @return the rendering settings.
     * @since 0.1.0
     * @author Qboi
     */
    @NonNull
    public RenderSettings getRenderSettings() {
        return renderSettings;
    }

    /**
     * Get the environment renderer.
     *
     * @return the environment renderer.
     * @since 0.1.0
     * @author Qboi
     */
    public EnvironmentRenderer getEnvironmentRenderer() {
        return environmentRenderer;
    }

    //*********************//
    //     Game values     //
    //*********************//
    /**
     * Get the current game ticks.
     *
     * @return the current game ticks.
     * @since 0.1.0
     * @author Qboi
     */
    public static long getTicks() {
        return ticks;
    }

    //***********************//
    //     Value options     //
    //***********************//
    /**
     * Get the game's data folder.
     *
     * @return the game's data folder.
     * @since 0.1.0
     * @author Qboi
     */
    public static File getGameDir() {
        return gameDir;
    }

    //****************//
    //     Bounds     //
    //****************//
    /**
     * Get the game's bounds.
     *
     * @return the game's bounds.
     * @since 0.1.0
     * @author Qboi
     */
    public Rectangle getGameBounds() {
        return new Rectangle(0, 0, getWidth(), getHeight());
    }

    //***********************//
    //     Startup Modes     //
    //***********************//
    /**
     * Check if the game is in debug mode.
     *
     * @return whether the game is in debug mode.
     * @since 0.1.0
     * @author Qboi
     */
    public static boolean isDebugMode() {
        return debugMode;
    }

    /**
     * Check if the game is running in development mode.
     *
     * @return whether the game is running in development mode.
     * @since 0.1.0
     * @author Qboi
     */
    public static boolean isDevMode() {
        return devMode;
    }

    //***********************//
    //     Class Loaders     //
    //***********************//
    /**
     * The class loader for the game.
     *
     * @return the game class loader.
     * @since 0.1.0
     * @author Qboi
     */
    public static ClassLoader getMainClassLoader() {
        return mainClassLoader;
    }

    //********************//
    //     Game flags     //
    //********************//

    /**
     * Check if the game is currently running.
     *
     * @return whether the game is currently running.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Check if the game is currently stopping.
     *
     * @return whether the game is currently stopping.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isStopping() {
        return stopping;
    }

    //*************************//
    //     Middle position     //
    //*************************//

    /**
     * Get the middle x position of the game.
     *
     * @return the middle x position of the game.
     * @since 0.1.0
     * @author Qboi
     */
    public static double getMiddleX() {
        return (double) getInstance().getWidth() / 2;
    }

    /**
     * Get the middle y position of the game.
     *
     * @return the middle y position of the game.
     * @since 0.1.0
     * @author Qboi
     */
    public static double getMiddleY() {
        return (double) getInstance().getHeight() / 2;
    }

    /**
     * Get the middle position of the game.
     *
     * @return the middle position of the game.
     * @since 0.1.0
     * @author Qboi
     */
    public static Point2D getMiddlePoint() {
        return new Point2D.Double(getMiddleX(), getMiddleY());
    }

    //***************//
    //     Media     //
    //***************//

    /**
     * Get the game's media manager.
     *
     * @return the game's media manager.
     * @since 0.1.0
     * @author Qboi
     */
    public static SoundPlayer getAudioPlayer() {
        return soundPlayer;
    }

    //*****************//
    //     Loggers     //
    //*****************//

    /**
     * Get the game's logger.
     *
     * @return the game's logger.
     * @since 0.1.0
     * @author Qboi
     */
    public static Logger getLogger() {
        return logger;
    }

    /////////////////////////////////////
    //     Reduce ticks to seconds     //
    /////////////////////////////////////
    public static byte reduceTicks2Secs(byte value, byte seconds) {
        return (byte) ((double) value / ((double) TPS * seconds));
    }

    public static short reduceTicks2Secs(short value, short seconds) {
        return (short) ((double) value / ((double) TPS * seconds));
    }

    public static int reduceTicks2Secs(int value, int seconds) {
        return (int) ((double) value / ((double) TPS * seconds));
    }

    public static long reduceTicks2Secs(long value, long seconds) {
        return (long) ((double) value / ((double) TPS * seconds));
    }

    public static float reduceTicks2Secs(float value, float seconds) {
        return (float) ((double) value / ((double) TPS * seconds));
    }

    public static double reduceTicks2Secs(double value, double seconds) {
        return value / ((double) TPS * seconds);
    }

    /**
     * Check if the game is paused.
     *
     * @return whether the game is paused or not.
     * @see Screen#doesPauseGame()
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isPaused() {
        Screen currentScreen = getInstance().getCurrentScreen();
        return currentScreen != null && currentScreen.doesPauseGame();
    }

    /**
     * Loads the game.
     *
     * @param mainProgress main loading progress.
     * @since 0.1.0
     * @author Qboi
     */
    private void load(ProgressMessenger mainProgress) {

    }
    
    /**
     * Prepares the player.
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void preparePlayer() {

    }

    /**
     * Active game glitch.
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void glitch() {
        isGlitched = true;
    }

    /**
     * Creates a player.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @return the created player.
     * @since 0.1.0
     * @author Qboi
     */
    private Player createPlayer() {
        if (environment != null) {
            return new Player(environment);
        } else {
            throw new IllegalStateException("Creating a player while environment is not loaded.");
        }
    }

    /**
     * Creates an instance of the screen manager.
     *
     * @return the created screen manager.
     * @since 0.1.0
     * @author Qboi
     */
    private ScreenManager createScreenManager() {
        return ScreenManager.create(null, this);
    }

    /**
     * @return the amount of main loading steps.
     * @since 0.1.0
     * @author Qboi
     */
    private int getMainLoadingSteps() {
        return 0;
    }

    /**
     * Mainloop.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("DuplicatedCode")
    private void mainloop() {
        // Prepare variables for the main loop.
        double tickCap = 1f / (double) tps;
        double frameTime = 0d;
        double frames = 0;

        double time = TimeProcessor.getTime();
        this.renderPartialTicks = 0;

        // Do initial game tick.
        initialGameTick();

        try {
            //************************//
            //     Main game loop     //
            //************************//
            while (running) {
                boolean canTick = false;

                double time2 = TimeProcessor.getTime();
                double passed = time2 - time;
                this.renderPartialTicks += passed;
                frameTime += passed;

                time = time2;

                while (this.renderPartialTicks >= tickCap) {
                    this.renderPartialTicks -= tickCap;

                    canTick = true;
                }

                // Do game tick.
                if (canTick) {
                    try {
                        internalTick();
                    } catch (Throwable t) {
                        CrashLog crashLog = new CrashLog("Game being ticked.", t);
                        crash(crashLog.createCrash());
                    }
                }

                // Calculate the FPS.
                if (frameTime >= 1.0d) {
                    frameTime = 0;
                    fps = (int) Math.round(frames);
                    frames = 0;
                }

                frames++;

                // Do game render.
                try {
                    renderAll(fps);
                } catch (Throwable t) {
                    CrashLog crashLog = new CrashLog("Game being rendered.", t);
                    crash(crashLog.createCrash());
                }

                // Run tasks.
                for (Runnable task : new ArrayList<>(tasks)) {
                    task.run();
                    tasks.remove(task);
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
            CrashLog crashLog = new CrashLog("Running game loop.", t);
            crash(crashLog.createCrash());
        }

        close();
    }

    /**
     * Do internal game tick.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void internalTick() {
        @Nullable Screen currentScreen = screenManager.getCurrentScreen();

        // Tick the player controller.
        if (playerController != null) {
            playerController.tick();
        }

        // Do game tick.
        this.tick();

        // Call tick event.
        if (isLoaded() && (currentScreen == null || !currentScreen.doesPauseGame())) {
            GameEvents.get().publish(new TickEvent(this));
        }
    }

    /**
     * Render method, for rendering window.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @param fps current game framerate.
     * @since 0.1.0
     * @author Qboi
     */
    private void renderAll(int fps) {
        if (!window.isInitialized()) return;

        BufferStrategy bs;
        Renderer renderer;
        try {

            // Buffer strategy (triple buffering).
            bs = this.window.canvas.getBufferStrategy();

            // Create buffers if not created yet.
            if (bs == null) {
                this.window.canvas.createBufferStrategy(2);
                bs = this.window.canvas.getBufferStrategy();
            }

            // Get GraphicsProcessor and GraphicsProcessor objects.
            renderer = new Renderer(bs.getDrawGraphics(), getObserver());
        } catch (IllegalStateException e) {
            return;
        }

        FilterApplier filterApplier = new FilterApplier(getBounds().getSize(), this.getObserver());
        Renderer filterRenderer = filterApplier.getRenderer();

        if (this.renderSettings.isAntialiasingEnabled() && this.isTextAntialiasEnabled())
            filterRenderer.hint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        if (this.renderSettings.isAntialiasingEnabled() && this.isAntialiasEnabled())
            filterRenderer.hint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        List<BufferedImageOp> filters = BubbleBlaster.instance.getCurrentFilters();

        if (loadingGui != null) {
            loadingGui.render(renderer);
        } else {
            this.renderGame(filterRenderer);
        }

        // Set filter gotten from filter event-handlers.
        filterApplier.setFilters(filters);

        // Draw filtered image.
        renderer.image(filterApplier.done(), 0, 0);

        this.fps = fps;

        // Dispose and show.
        renderer.dispose();

        try {
            bs.show();
        } catch (IllegalStateException e) {
//            close();
        }
    }

    /**
     * Render game.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @param renderer the renderer to render the game with.
     * @since 0.1.0
     * @author Qboi
     */
    private void renderGame(Renderer renderer) {
        // Call to game environment rendering.
        // Get field and set local variable. For multithreaded null-safety.
        @Nullable Screen screen = screenManager.getCurrentScreen();
        @Nullable Environment environment = this.environment;
        @Nullable EnvironmentRenderer environmentRenderer = this.environmentRenderer;

        // Clear background.
        renderer.clearColor(Color.BLACK);
        renderer.clearRect(0, 0, getWidth(), getHeight());

        // Post event before rendering the game.
        GameEvents.get().publish(new RenderGameEvent.Before(renderer));

        // Render environment.
        if (environment != null && environmentRenderer != null) {
            GameEvents.get().publish(new RenderEnvironmentEvent.Before(renderer, environment));
            environmentRenderer.render(renderer);
            GameEvents.get().publish(new RenderEnvironmentEvent.After(renderer, environment));
        }

        // Render screen.
        if (screen != null) {
            GameEvents.get().publish(new RenderScreenEvent.Before(screen, renderer));
            screen.render(this, renderer, getRenderPartialTicks());
            GameEvents.get().publish(new RenderScreenEvent.After(screen, renderer));
        }

        // Post event after rendering the game.
        GameEvents.get().publish(new RenderGameEvent.After(renderer));

        // Post render.
        postRender(renderer);
    }

    /**
     * Post render method.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @param renderer the renderer to render the game with.
     * @since 0.1.0
     * @author Qboi
     */
    private void postRender(Renderer renderer) {
        if (isDebugMode() || debugGuiOpen) {
            debugRenderer.render(renderer);
        }

        if (isGlitched) {
            glitchRenderer.render(renderer);
        }
    }

    /**
     * Ticks the game.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void tick() {
        // Glitch tick.
        if (isGlitched) {
            glitchRenderer.tick();
            return;
        }

        // Environment tick.
        if (this.environment != null && !isPaused()) {
            Gamemode gamemode = this.environment.getGamemode();
            if (gamemode != null) {
                Player player = gamemode.getPlayer();
                if (player != null) {
                    if (player.getLevel() > 255) {
                        BubbleBlaster.getInstance().glitch();
                    }
                }

            }
            this.environment.tick();
        }

        if (ticker.advance() == 40) { // Every 40 ticks.
            ticker.reset();
            Player player = this.player;
            if (LoadingGui.isDone()) {
                // Update RPC activity.
                if (isInMainMenus()) {
                    setActivity(() -> {
                        Activity activity = new Activity();
                        activity.setState("In the menus");
                        return activity;
                    });
                } else if (isInGame()) {
                    setActivity(() -> {
                        Activity activity = new Activity();
                        activity.setState("In-Game");
                        double score = player.getScore();
                        activity.setDetails("Score: " + (int) score);
                        return activity;
                    });
                } else {
                    setActivity(() -> {
                        Activity activity = new Activity();
                        activity.setState("Is nowhere to be found");
                        return activity;
                    });
                }
            }
        }

        // Advance tick.
        BubbleBlaster.ticks++;
    }

    /**
     * Handle on-key-press event
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @param event`the event to handle.
     * @since 0.1.0
     * @author Qboi
     */
    @SubscribeEvent
    public void onKeyPress(KeyPressEvent event) {
        if (event.getKeyCode() == KeyInput.Map.KEY_F12) {
            debugGuiOpen = !debugGuiOpen;
        }
    }

    /**
     * Loads the game environment, and prepare the player.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void loadEnvironment() {
        preparePlayer();
    }

    /**
     * Starts loading the game.
     * <b>Note:</b> <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void startLoading() {
        glitchRenderer = new GlitchRenderer(this);
        getGameWindow().init();
    }

    /**
     * Get the game's default font.
     *
     * @return the game's default font.
     * @since 0.1.0
     * @author Qboi
     */
    public Font getFont() {
        return getSansFont();
    }

    /**
     * Get the game font.
     *
     * @return the game font.
     * @since 0.1.0
     * @author Qboi
     */
    public String getGameFontName() {
        return getGameFont().getFontName();
    }

    /**
     * Get the game's pixel art font name.
     *
     * @return the game's pixel art font name.
     * @since 0.1.0
     * @author Qboi
     */
    public String getPixelFontName() {
        return getPixelFont().getFontName();
    }

    /**
     * Get the game's monospace font name.
     *
     * @return the game's monospace font name.
     * @since 0.1.0
     * @author Qboi
     */
    public String getMonospaceFontName() {
        return getMonospaceFont().getFontName();
    }

    /**
     * Get the game's sans font name.
     *
     * @return the game's sans font name.
     * @since 0.1.0
     * @author Qboi
     */
    public String getSansFontName() {
        return getSansFont().getFontName();
    }

    /**
     * Get the game's event manager.
     *
     * @return the game's event manager.
     * @since 0.1.0
     * @author Qboi
     */
    public static GameEvents getEventBus() {
        return GameEvents.get();
    }

    /**
     * Get the font metrics for a font.
     *
     * @param font the font to get the metrics for.
     * @return the font metrics.
     * @since 0.1.0
     * @author Qboi
     */
    public FontMetrics getFontMetrics(Font font) {
        return window.canvas.getFontMetrics(font);
    }

    /**
     * Run a task on the game's main thread.
     *
     * @param task the task to run.
     * @since 0.1.0
     * @author Qboi
     */
    public static void runOnMainThread(Runnable task) {
        if (instance.isOnMainThread()) {
            // Already on main thread.
            task.run();
        } else {
            // Post to main thread.
            runLater(task);
        }
    }

    /**
     * Check if there's an open screen.
     *
     * @return Whether there's an open screen.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean hasScreenOpen() {
        return screenManager.getCurrentScreen() != null;
    }

    /**
     * Get sound from a resource identifier.
     *
     * @param identifier the resource identifier.
     * @return the sound.
     * @since 0.1.0
     * @author Qboi
     */
    public Sound getSound(Identifier identifier) {
        Resource resource = resourceManager.getResource(identifier);

        return null;
    }

    /**
     * Play a sound, and return the player.
     *
     * @param identifier the resource identifier.
     * @return the mp3 player.
     * @since 0.1.0
     * @author Qboi
     */
    public synchronized MP3Player playSound(Identifier identifier) {
        try {
            // Get the resource for the sound.
            Identifier identifier1 = identifier.mapPath(path -> "Audio/BGM/" + path + ".mp3");
            System.out.println("identifier1 = " + identifier1);
            Resource input = resourceManager.getResource(identifier1);
            System.out.println(input);

            // Create a new player.
            MP3Player player = new MP3Player(identifier.toString(), Objects.requireNonNull(input).loadOrOpenStream());

            // Play the sound.
            player.play();

            // Return the player.
            return player;
        } catch (Exception e) {
            // An error occurred.
            throw new RuntimeException(e);
        }
    }

    /**
     * Schedule a task to run later.
     *
     * @param task the task to run later.
     * @since 0.1.0
     * @author Qboi
     */
    public void scheduleTask(Runnable task) {
        this.tasks.add(task);
    }

    /**
     * Initialize the game's engine.=
     *
     * @param debugMode whether to enable debug mode.
     * @param devMode whether to enable dev mode.
     * @since 0.1.0
     * @author Qboi
     */
    public static void initEngine(boolean debugMode, boolean devMode) {
        BubbleBlaster.debugMode = debugMode;
        BubbleBlaster.devMode = devMode;
    }

    /**
     * @deprecated this is basically useless.
     * @since 0.1.0
     * @author Qboi
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    private void log(String text) {
        // Todo: implement
    }

    /**
     * This method should be overridden when filters are used in the game.<br>
     * Should be overridden for enabling / disabling render filters.
     *
     * @return a list of buffered image operations.
     * @since 0.1.0
     * @author Qboi
     */
    @NonNull
    public List<BufferedImageOp> getCurrentFilters() {
        return new ArrayList<>();
    }

    /**
     * Load the playing environment.
     * <i>This is an internally used method, and shouldn't be called unless you know what you are doing.</i>
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void loadPlayEnvironment() {
        Player player = createPlayer();
        this.input = player;
        this.playerController = new PlayerController(this.input);
        this.player = player;

        loadEnvironment();
    }

    //*****************//
    //     Getters     //
    //*****************//

    /**
     * Get the game's frame time.
     *
     * @return the game's frame time.
     * @since 0.1.0
     * @author Qboi
     */
    public float getRenderPartialTicks() {
        return renderPartialTicks;
    }

    /**
     * Check if anti-aliasing is enabled.
     *
     * @return whether anti-aliasing is enabled.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isAntialiasEnabled() {
        return true;
    }

    /**
     * Check if text anti-aliasing is enabled.
     *
     * @return whether text anti-aliasing is enabled.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isTextAntialiasEnabled() {
        return isAntialiasEnabled();
    }

    /**
     * Get the game's window.
     *
     * @return the game's window.
     * @since 0.1.0
     * @author Qboi
     */
    public GameWindow getGameWindow() {
        return this.window;
    }

    /**
     * Get the window's image observer.
     *
     * @return the window's image observer.
     * @since 0.1.0
     * @author Qboi
     */
    public ImageObserver getObserver() {
        return window.observer;
    }

    /**
     * Get the bounding box of the game's window.
     *
     * @return the bounding box of the game's window.
     * @since 0.1.0
     * @author Qboi
     */
    public Rectangle getBounds() {
        return new Rectangle(0, 0, window.getWidth(), window.getHeight());
    }

    /**
     * Get the game's window width.
     *
     * @return the game's window width.
     * @since 0.1.0
     * @author Qboi
     */
    public int getWidth() {
        return window.canvas.getWidth();
    }

    /**
     * Get the game's window height.
     *
     * @return the game's window height.
     * @since 0.1.0
     * @author Qboi
     */
    public int getHeight() {
        return window.canvas.getHeight();
    }

    /**
     * Get the game's frame rate.
     *
     * @return the game's frame rate.
     * @since 0.1.0
     * @author Qboi
     */
    public int getFps() {
        return fps;
    }

    /**
     * Check if the game is loaded.
     *
     * @return whether the game is loaded.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isLoaded() {
        return this.loaded;
    }

    /**
     * Mark the game as loaded.
     *
     * @since 0.1.0
     * @author Qboi
     */
    private void markLoaded() {
        this.loaded = true;
    }

    /**
     * Get the game's ticks per second.
     *
     * @return the game's ticks per second.
     * @since 0.1.0
     * @author Qboi
     */
    public int getTps() {
        return tps;
    }

    /**
     * Clean shutdown of the game. By setting the running flag to false, and stop flag to true.
     *
     * @since 0.1.0
     * @author Qboi
     * @see #isRunning()
     * @see #isStopping()
     */
    public void shutdown() {
        this.stopping = true;
        this.running = false;
        try {
            mainThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        close();
    }

    /**
     * Force closes the game.
     * <b>Note:</b> <i>This method should be used with caution.</i>
     */
    void close() {
        onClose();
        window.close();

        System.exit(crashed ? 1 : 0);
    }

    /**
     * Crash the game.
     * <b>Note:</b> <i>This method should be used with caution.</i>
     *
     * @param crash the game crash.
     * @since 0.1.0
     * @author Qboi
     */
    public void crash(@NonNull ApplicationCrash crash) {
        CrashLog crashLog = crash.getCrashLog();
        crashed = true; // Set the crashed flag to true.

        // Override the default crash handling if the onCrash method returns true.
        boolean overridden = false;
        try {
            overridden = onCrash(crash);
        } catch (@NonNull Throwable ignored) {

        }

        if (!overridden) {
            crashLog.defaultSave();
            crash.printCrash();
        }

        // Shutdown the game, and then also force close it.
        shutdown();
        close();
    }

    /**
     * Crash handler, for overriding default crash handling.
     *
     * @param crash the crash that happened.
     * @return whether the default should be overridden or not. The value {@code true}
     *         means that the default handling should be overridden.
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("unused")
    public boolean onCrash(ApplicationCrash crash) {
        GameEvents.get().publish(new CrashEvent(crash));
        return false;
    }

    @EnsuresNonNull({"mainThread"})
    @SuppressWarnings("Convert2Lambda")
    void windowLoaded() {
        this.mainThread = new Thread(new Runnable() {
            public void run() {
                running = true;
                BubbleBlaster.this.mainloop();
            }
        }, "Main-Thread");
        System.out.println("mainThread = " + mainThread);
        this.mainThread.setDaemon(true);
        this.mainThread.start();
    }

    /**
     * Crash the game with a throwable.
     *
     * @param t the throwable that caused the crash.
     */
    public void crash(Throwable t) {
        // Create a new crash.
        CrashLog crashLog = new CrashLog("Unknown source", t); // It's an unknown source because we don't know where it came from.
        crash(crashLog.createCrash());
    }

    //*****************************//
    //     Game Initialization     //
    //*****************************//
    public void initialize() {
        // Prepare
        loadingGui.make();
        prepare();

        // Initialize the mod loader, and load all mods.
        nextLoadMsg("Loading mods...");
        doLoadMods();

        // Load all resources.
        nextLoadMsg("Loading resources...");
        doLoadResources();

        // Construct all mods.
        nextLoadMsg("Constructing mods...");
        doConstructMods();

        // Loading object holders
        nextLoadMsg("Loading object-holders...");
        doLoadObjectHolders();

        // Loading object holders
        nextLoadMsg("Loading fonts");
        doLoadFonts();

        // Initializing bubble blaster itself.
        nextLoadMsg("Initializing Bubble Blaster");
        doInitGame();

        // Setup all the mods.
        nextLoadMsg("Setup Mods");
        doModSetup();

        // Do all the registration stuff.
        nextLoadMsg("Registering...");
        doRegistration();

        // Collect all textures, and load/generate them.
        nextLoadMsg("Collecting textures...");
        doCollectTextures();

        // BubbleSystem
        nextLoadMsg("Initialize bubble system...");
        doInitBubbleSystem();

        // Load complete.
        nextLoadMsg("Load Complete!");
        doSendLoadCompleteEvents();

        // Registry dump.
        nextLoadMsg("Registry Dump.");
        doRegistryDump();

        // Done
        loadingGui.done();
    }

    void prepare() {
        // Get game directory in Java's File format.
        File gameDir = BubbleBlaster.getGameDir();

        // Check game directory exists, if not, create it!
        if (!gameDir.exists()) {
            if (!gameDir.mkdirs()) {
                throw new IllegalStateException("Game Directory isn't created!");
            }
        }
    }

    void doLoadMods() {
        logger.info("Loading mods...");
        this.modLoader = new ModLoader(loadingGui);
        loadingGui.subProgress = null;
    }

    private void doLoadResources() {
        logger.info("Loading resources...");

        List<ModContainer> containers = ModManager.instance().getContainers();
        loadingGui.subProgress = loadingGui.createSubProgress(containers.size());
        for (ModContainer container : containers) {
            resourceManager.importResources(container.getSource());
            loadingGui.subProgress.sendNext("Mod: " + container.getModInfo().getName());
        }
        loadingGui.subProgress = null;
    }

    private void doConstructMods() {
        logger.info("Constructing mods...");
        this.modLoader.constructMods();
        loadingGui.subProgress = null;
    }

    private void doLoadFonts() {
        loadFonts();
        loadingGui.subProgress = null;
    }

    private void doModSetup() {
        logger.info("Setup mods...");
        this.modLoader.modSetup();
        loadingGui.subProgress = null;
    }

    private void doRegistryDump() {
        Registry.dump();
        BubbleBlaster.getEventBus().publish(new RegistryEvent.Dump());
    }

    private void doSendLoadCompleteEvents() {
        for (ModContainer container : ModManager.instance().getContainers()) {
            ModEvents publish = modLoader.getModEvents(container.getModId());
            publish.publish(new LoadCompleteEvent(this));
        }
    }

    private void doInitBubbleSystem() {
        BubbleSystem.init();
    }

    private void doCollectTextures() {
        Collection<TextureCollection> values = Registers.TEXTURE_COLLECTIONS.values();
        loadingGui.subProgress = loadingGui.createSubProgress(values.size());
        for (TextureCollection collection : values) {
            GameEvents.get().publish(new CollectTexturesEvent(collection));
            loadingGui.subProgress.increment();
        }
    }

    private void doRegistration() {
        Collection<Registry<?>> registries = Registry.getRegistries();
        loadingGui.subProgress = loadingGui.createSubProgress(registries.size());
        for (Registry<?> registry : registries) {
            loadingGui.subProgress.send(registry.id().toString());
            loadingGui.subProgress.increment();
            GameEvents.get().publish(new RegistryEvent.Register<>(registry));
        }
        loadingGui.subProgress = null;
    }

    @SuppressWarnings({"unchecked", "DuplicatedCode"})
    private void doLoadObjectHolders() {
        List<ModContainer> containers = ModManager.instance().getContainers();

        // Loop mods.
        for (ModContainer container : containers) {
            // Get scan result.
            Scanner.Result result = modLoader.getScanResult(container.getSource());
            List<Class<?>> classes = result.getClasses(ObjectHolder.class);

            // Loop classes to register constants in object-holder annotated classes.
            for (Class<?> clazz : classes) {

                // Check if class have object-holder annotation.
                if (!clazz.isAnnotationPresent(ObjectHolder.class)) continue;

                // Get annotation, to get the mod id from it.
                ObjectHolder holder = clazz.getDeclaredAnnotation(ObjectHolder.class);
                String modId;
                Class<?> type;

                // Check if modId is present, if true, the mod id variable will be assigned.
                if (holder != null) {
                    modId = holder.modId();
                    type = holder.type();

                    if (type == ObjectUtils.Null.class) type = null;
                } else {
                    throw new NullPointerException();
                }

                // Get fields.
                Field[] fields = clazz.getDeclaredFields();

                if (type != null) {
                    // Loop fields.
                    for (Field field : fields) {
                        // Try.
                        try {
                            if (type.isAssignableFrom(field.getType())) {
                                // Get type of the register-object.
                                Class<? extends Registrable> objType = (Class<? extends Registrable>) field.getType();

                                // Get register-object.
                                Registrable object = (Registrable) field.get(null);

                                // Set key.
                                object.setId(new Identifier(field.getName().toLowerCase(), modId));

                                // Register.
                                Registry.getRegistry(objType).registrable(object.id(), object);
                            }
                        } catch (IllegalAccessException e) {
                            // Oops, some problem occurred.
                            e.printStackTrace();
                        }
                    }
                    return;
                }
                // Loop fields.
                for (Field field : fields) {
                    // Try.
                    try {
                        // Is the field a RegistryObject?
                        if (Registrable.class.isAssignableFrom(field.getType())) {
                            // Get type of the register-object.
                            Class<? extends Registrable> objType = (Class<? extends Registrable>) field.getType();

                            // Get register-object.
                            Registrable object = (Registrable) field.get(null);

                            // Set key.
                            object.setId(new Identifier(field.getName().toLowerCase(), modId));

                            // Register.
                            Registry.getRegistry(objType).registrable(object.id(), object);
                        }
                    } catch (IllegalAccessException e) {
                        // Oops, some problem occurred.
                        e.printStackTrace();
                    }
                }
            }
        }
        loadingGui.subProgress = null;
    }

    public void doInitGame() {
        BubbleBlaster main = Util.getGame();

        // Request focus.
        window.frame.requestFocus();
        window.canvas.requestFocus();

        // Commands
        loadingGui.logInfo("Initializing Commands...");
        CommandConstructor.add("tp", new TeleportCommand());
        CommandConstructor.add("heal", new HealCommand());
        CommandConstructor.add("level", new LevelCommand());
        CommandConstructor.add("health", new HealthCommand());
        CommandConstructor.add("score", new ScoreCommand());
        CommandConstructor.add("effect", new EffectCommand());
        CommandConstructor.add("spawn", new SpawnCommand());
        CommandConstructor.add("shutdown", new ShutdownCommand());
        CommandConstructor.add("teleport", new TeleportCommand());
        CommandConstructor.add("game-over", new GameOverCommand());
        CommandConstructor.add("blood-moon", new BloodMoonCommand());

        try {
            GlobalSaveData.instance().load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        loadingGui.subProgress = null;
    }
    
    private void nextLoadMsg(String msg) {
        loadingGui.mainProgress.sendNext(msg);
    }

    public void doneLoading() {
        screenManager.displayScreen(new TitleScreen());
        loadingGui = null;
    }

    /**
     * The game's boot options.
     *
     * @author Qboi
     * @since 0.1.0
     */
    protected static class BootOptions {
        private int tps = 20;

        /**
         * Constructor for the boot options.
         *
         * @since 0.1.0
         * @author Qboi
         */
        public BootOptions() {
            // Do nothing.
        }

        /**
         * Set the TPS (ticks per second).
         *
         * @param tps the TPS.
         * @return the same object that it's called with.
         */
        public BootOptions tps(int tps) {
            this.tps = tps;
            return this;
        }
    }
}
