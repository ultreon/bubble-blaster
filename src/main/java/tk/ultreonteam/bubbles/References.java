package tk.ultreonteam.bubbles;

import java.io.File;
import java.io.IOError;
import java.io.IOException;

/**
 * References to various data files and directories.
 *
 * @author Qboi
 */
public class References {
    /**
     * The directory where the game data is stored.
     */
    public static final File GAME_DIR = BubbleBlaster.getGameDir();

    /**
     * The directory where the game logs are stored.
     */
    public static final File LOGS_DIR = new File(GAME_DIR.getAbsolutePath(), "Logs");

    /**
     * The directory where the mods are getting loaded from.
     */
    public static final File MODS_DIR = new File(GAME_DIR.getAbsolutePath(), "Mods");

    /**
     * The directory where the game saves are stored.
     */
    public static final File SAVES_DIR = new File(GAME_DIR, "Saves");

    /**
     * The file where the game settings are stored.
     */
    public static final File SETTINGS_FILE = new File(GAME_DIR, "Settings.json");

    /**
     * The directory where the game crashes are saved.
     */
    public static final File CRASH_REPORTS = new File(GAME_DIR, "Game-Crashes");

    static {
        if (!LOGS_DIR.exists() && !LOGS_DIR.mkdirs()) {
            throw new IOError(new IOException("Couldn't make directories. " + LOGS_DIR.getAbsolutePath()));
        }
    }
}
