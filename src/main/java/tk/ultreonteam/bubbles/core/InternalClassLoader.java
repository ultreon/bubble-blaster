package tk.ultreonteam.bubbles.core;

import com.google.gson.*;
import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.mod.loader.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class InternalClassLoader extends URLClassLoader {
    private static final String INTERNAL_ID = "tk.ultreonteam.bubbleblaster";
    private final Map<File, ModClassLoader> modClassLoaders = new HashMap<File, ModClassLoader>();
    private final Map<String, Scanner.Result> scans = new HashMap<>();
    private static final Gson gson = new Gson();

    private static final Logger MOD_LOADER_LOGGER = LogManager.getLogger("Mod-Loader");

    private final HashMap<Path, String> fileIdMap = new HashMap<java.nio.file.Path, String>();
    private final HashMap<String, File> fileMap = new HashMap<>();
    private static InternalClassLoader instance = new InternalClassLoader();
    private final File gameFile;
    private Scanner.Result scanResult;

    public InternalClassLoader() {
        super(new URL[]{BubbleBlaster.class.getProtectionDomain().getCodeSource().getLocation()}, BubbleBlaster.class.getClassLoader());
        try {
            this.gameFile = new File(BubbleBlaster.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        if (instance != null) {
            throw new IllegalStateException("Game class loader already initialized.");
        }
        instance = this;
    }

    public static InternalClassLoader get() {
        return instance;
    }

    @Nullable
    public ModClassLoader addMod(File file) {
        JsonObject modFileData;
        BasicFileAttributes basicFileAttributes = null;
        try {
            basicFileAttributes = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (basicFileAttributes.isRegularFile()) {
            JarFile jarFile = null;
            try {
                jarFile = new JarFile(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            JarEntry jarEntry = jarFile.getJarEntry("META-INF/modFile.json");
            if (jarEntry == null) {
                MOD_LOADER_LOGGER.warn("Failed to read modFile.json for file: " + file.getName());
                return null;
            }
            InputStream inputStream = null;
            try {
                inputStream = jarFile.getInputStream(jarEntry);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            modFileData = gson.fromJson(new InputStreamReader(inputStream), JsonObject.class);
        } else if (basicFileAttributes.isDirectory()) {
            File modFile = new File(file, "META-INF/modFile.json");
            if (!modFile.exists()) {
                modFile = new File(file, "../build/resources/main/META-INF/modFile.json");
            }
            if (!modFile.exists()) {
                modFile = new File(file, "../../../resources/main/META-INF/modFile.json");
            }
            if (!modFile.exists()) {
                MOD_LOADER_LOGGER.warn("Failed to read modFile.json for file: " + file.getName());
                return null;
            }

            InputStream inputStream = null;
            try {
                inputStream = new FileInputStream(modFile);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            modFileData = gson.fromJson(new InputStreamReader(inputStream), JsonObject.class);
        } else {
            throw new IllegalStateException("Expected path to be either a file or directory: " + file.getPath());
        }

        String fileId;
        if (modFileData.has("fileId")) {
            JsonElement jsonElement = modFileData.get("fileId");
            if (jsonElement.isJsonPrimitive()) {
                JsonPrimitive asJsonPrimitive = jsonElement.getAsJsonPrimitive();
                if (asJsonPrimitive.isString()) {
                    fileId = asJsonPrimitive.getAsString();
                } else throw new JsonParseException("Expected to find json string at member 'fileId'.");
            } else throw new JsonParseException("Expected to find json string (primitive) at member 'fileId'");
        } else {
            throw new JsonParseException("Expected to find member 'fileId' but got nothing.");
        }

        ModClassLoader modClassLoader = new ModClassLoader(this, file, fileId);

        this.fileIdMap.put(file.toPath().toAbsolutePath(), fileId);
        this.fileMap.put(fileId, file);
        this.modClassLoaders.put(file, modClassLoader);

        return modClassLoader;
    }

    public void scan(String modFileId) {
        if (Objects.equals(modFileId, INTERNAL_ID)) {
            if (scanResult == null) scan();
            return;
        }

        File file = fileMap.get(modFileId);
        ModClassLoader loader = this.modClassLoaders.get(file);
        Scanner.Result scanResult = loader.scan();
        this.scans.put(modFileId, scanResult);
    }

    public Scanner.Result scan() {
        Scanner scanner = new Scanner(gameFile, this);
        Scanner.Result scanResult = scanner.scan();
        this.scans.put(INTERNAL_ID, scanResult);
        return this.scanResult = scanResult;
    }

    public String getModFileId(File modFile) {
        return this.fileIdMap.get(modFile.toPath().toAbsolutePath());
    }

    public Class<?> getClass(String file, String name) throws ClassNotFoundException {
        ModClassLoader modClassLoader = this.modClassLoaders.get(new File(file));
        return modClassLoader.findClass(name);
    }

    public Class<?> getClassOrNull(String file, String name) {
        ModClassLoader modClassLoader = this.modClassLoaders.get(new File(file));
        try {
            return modClassLoader.findClass(name);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return super.findClass(name);
    }

    @Override
    protected Class<?> findClass(String moduleName, String name) {
        return super.findClass(moduleName, name);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        return super.loadClass(name, resolve);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return super.loadClass(name);
    }

    public void getModClassLoader(String modFileId) {
        this.modClassLoaders.get(fileMap.get(modFileId));
    }

    public Scanner.Result getResult(String modFileId) {
        return scans.get(modFileId);
    }

    public Scanner.Result getScanResult() {
        return scanResult;
    }

    public File getGameFile() {
        return gameFile;
    }
}
