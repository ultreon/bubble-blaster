package tk.ultreonteam.bubbles.core.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.input.MouseMotionEvent;
import tk.ultreonteam.bubbles.event.input.MousePressEvent;
import tk.ultreonteam.bubbles.event.input.MouseReleaseEvent;
import tk.ultreonteam.bubbles.event.input.MouseClickEvent;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.render.screen.ScreenManager;
import tk.ultreonteam.bubbles.vector.Vec2i;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Mouse controller for the Hydro Game Engine.
 * Should only for internal use.
 *
 * @author Qboi
 * @see KeyboardInput
 * @see java.awt.event.MouseAdapter
 */
@SuppressWarnings("ConstantConditions")
public abstract class MouseInput extends MouseAdapter {
    // Mouse input values.
    private Point posOnScreen;
    private Point pos;
    private int clicks;

    // Logger
    private static final Logger logger = LogManager.getLogger("Game-Input");

    // Other fields.
    private final Map<Integer, Boolean> buttonMap = new HashMap<>();
    private final BubbleBlaster game;
    private final HashMap<Integer, Vec2i> dragStarts = new HashMap<>();

    /**
     * @since 0.1.0
     * @author Qboi
     */
    public MouseInput() {
        this.game = BubbleBlaster.getInstance();
    }

    @Override
    public final void mouseClicked(MouseEvent e) {
        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        this.clicks = e.getClickCount();

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MouseClickEvent(BubbleBlaster.getInstance(), pos.x, pos.y, clicks, e.getButton()));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseClick(e.getX(), e.getY(), e.getButton(), e.getClickCount());
            }
        }
    }

    @Override
    public final void mousePressed(MouseEvent e) {
        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        buttonMap.put(e.getButton(), true);

        dragStarts.put(e.getButton(), new Vec2i(e.getPoint()));

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MousePressEvent(BubbleBlaster.getInstance(), pos.x, pos.y, e.getButton()));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMousePress(e.getX(), e.getY(), e.getButton());
            }
        }
    }

    @Override
    public final void mouseReleased(MouseEvent e) {
        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        buttonMap.put(e.getButton(), false);

        dragStarts.remove(e.getButton());

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MouseReleaseEvent(BubbleBlaster.getInstance(), pos.x, pos.y, e.getButton()));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseRelease(e.getX(), e.getY(), e.getButton());
            }
        }
    }

    @Override
    public final void mouseEntered(MouseEvent e) {
        Point oldPos = pos;

        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        if (oldPos == null) {
            oldPos = pos;
        }

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MouseMotionEvent(BubbleBlaster.getInstance(), oldPos.x, oldPos.y, pos.x, pos.y));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseEnter(e.getX(), e.getY());
            }
        }
    }

    @Override
    public final void mouseExited(MouseEvent e) {
        Point oldPos = pos;

        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MouseMotionEvent(BubbleBlaster.getInstance(), oldPos.x, oldPos.y, pos.x, pos.y));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseExit();
            }
        }
    }

    @Override
    public final void mouseDragged(MouseEvent e) {
        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

//        if (GameEvents.get() != null) {
//            GameEvents.get().publish(new MouseMotionEvent(Game.getInstance(), this, e, MouseEventType.DRAG));
//        }
    }

    @Override
    public final void mouseMoved(MouseEvent e) {
//        out.printf("Mouse Moved: x=%s, y=%s", e.getX(), e.getY());
        Point oldPos = pos;

        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        for (Map.Entry<Integer, Vec2i> entry : dragStarts.entrySet()) {
            ScreenManager screenManager = game.getScreenManager();
            if (screenManager != null) {
                Screen currentScreen = screenManager.getCurrentScreen();
                if (currentScreen != null) {
                    Vec2i vec = entry.getValue();
                    currentScreen.onMouseDrag(e.getX(), e.getY(), vec.x, vec.y, entry.getKey());
                }
            }
        }

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new MouseMotionEvent(BubbleBlaster.getInstance(), oldPos.x, oldPos.y, pos.x, pos.y));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseMove(e.getX(), e.getY());
            }
        }
    }

    @Override
    public final void mouseWheelMoved(MouseWheelEvent e) {
        posOnScreen = e.getLocationOnScreen() != null ? e.getLocationOnScreen() : posOnScreen;
        pos = e.getPoint() != null ? e.getPoint() : pos;

        if (GameEvents.get() != null) {
            GameEvents.get().publish(new tk.ultreonteam.bubbles.event.input.MouseWheelEvent(BubbleBlaster.getInstance(), e.getPreciseWheelRotation(), e.getWheelRotation(), e.getScrollAmount(), e.getScrollType(), e.getUnitsToScroll(), pos.x, pos.y));
        }

        ScreenManager screenManager = game.getScreenManager();
        if (screenManager != null) {
            Screen currentScreen = screenManager.getCurrentScreen();
            if (currentScreen != null) {
                currentScreen.onMouseWheel(e.getX(), e.getY(), e.getPreciseWheelRotation(), e.getScrollAmount(), e.getUnitsToScroll());
            }
        }

    }

    protected Vec2i getPosOnScreen() {
        return new Vec2i(posOnScreen);
    }

    protected Vec2i getPos() {
        return BubbleBlaster.getInstance().getGameWindow().getMousePosition();
    }

    protected int getClicks() {
        return clicks;
    }

    protected boolean isPressed(int button) {
        return buttonMap.getOrDefault(button, false);
    }
}
