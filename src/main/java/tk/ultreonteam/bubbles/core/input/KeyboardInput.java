package tk.ultreonteam.bubbles.core.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.LoadedGame;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.input.CharTypedEvent;
import tk.ultreonteam.bubbles.event.input.KeyPressEvent;
import tk.ultreonteam.bubbles.event.input.KeyReleaseEvent;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.render.screen.PauseScreen;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.render.screen.ScreenManager;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @see MouseInput
 * @see java.awt.event.KeyAdapter
 */
public abstract class KeyboardInput extends KeyAdapter {
    private static final Set<Integer> keysDown = new CopyOnWriteArraySet<>();
    private final BubbleBlaster game;

    public KeyboardInput() {
        this.game = BubbleBlaster.getInstance();
    }

    public static boolean isKeyDown(int keyCode) {
        return keysDown.contains(keyCode);
    }

    @Override
    public final void keyPressed(KeyEvent e) {
        ScreenManager screenManager = this.game.getScreenManager();
        Screen currentScreen = screenManager.getCurrentScreen();
        if (currentScreen != null) {
            currentScreen.onKeyPress(e.getKeyCode(), e.getKeyChar());
        } else if (e.getKeyCode() == KeyInput.Map.KEY_ESCAPE) {
            BubbleBlaster.getInstance().showScreen(new PauseScreen());
        }

        GameEvents events = GameEvents.get();
        if (events != null) {
            LoadedGame loadedGame = game.getLoadedGame();
            if (isKeyDown(e.getKeyCode())) {
                events.publish(new KeyPressEvent(BubbleBlaster.getInstance(), e.getKeyCode(), e.getExtendedKeyCode(), e.getKeyLocation(), e.getKeyChar(), e.getModifiersEx(), true));
            } else {
                if (loadedGame != null) {
                    loadedGame.onKeyPress(e.getKeyCode(), e.getKeyLocation(), e.getModifiersEx());
                }
                events.publish(new KeyPressEvent(BubbleBlaster.getInstance(), e.getKeyCode(), e.getExtendedKeyCode(), e.getKeyLocation(), e.getKeyChar(), e.getModifiersEx(), false));
                keysDown.add(e.getKeyCode());
            }
            if (loadedGame != null) {
                loadedGame.onKeyHold(e.getKeyCode(), e.getKeyLocation(), e.getModifiersEx());
            }
        }
    }

    @Override
    public final void keyReleased(KeyEvent e) {
        keysDown.remove(e.getKeyCode());
        if (GameEvents.get() != null) {
            GameEvents.get().publish(new KeyReleaseEvent(BubbleBlaster.getInstance(), e.getKeyCode(), e.getExtendedKeyCode(), e.getKeyLocation(), e.getKeyChar(), e.getModifiersEx()));
        }

//        logger.info("KeyInput: RELEASE (" + e.getKeyCode() + ", " + e.getKeyChar() + ")");

        ScreenManager screenManager = this.game.getScreenManager();
        Screen currentScreen = screenManager.getCurrentScreen();
        if (currentScreen != null) {
            currentScreen.onKeyRelease(e.getKeyCode(), e.getKeyChar());
        }
    }

    @Override
    public final void keyTyped(KeyEvent e) {
        if (GameEvents.get() != null) {
            GameEvents.get().publish(new CharTypedEvent(BubbleBlaster.getInstance(), e.getKeyCode(), e.getExtendedKeyCode(), e.getKeyLocation(), e.getKeyChar(), e.getModifiersEx()));
        }

//        logger.info("KeyInput: TYPE (" + e.getKeyCode() + ", " + e.getKeyChar() + ")");

        ScreenManager screenManager = this.game.getScreenManager();
        Screen currentScreen = screenManager.getCurrentScreen();
        if (currentScreen != null) {
            currentScreen.onKeyType(e.getKeyCode(), e.getKeyChar());
        }
    }
}
