package tk.ultreonteam.bubbles.core;

import tk.ultreonteam.bubbles.mod.loader.Scanner;
import tk.ultreonteam.preloader.PreClassLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * A simple delegating class loader used to load mods into the system
 *
 * @author cpw
 */
@AntiMod
public class ModClassLoader extends URLClassLoader {
    private PreClassLoader mainClassLoader;
    private InternalClassLoader internalClassLoader;
    @SuppressWarnings("FieldMayBeFinal")

    private final List<String> sources;
    private final File modSource;

    // Classes
    private final Map<String, Class<?>> classes = new HashMap<>();

    // File id.
    private final String fileId;

    private final JarFile jarFile;
    private final File file;

    public ModClassLoader(ClassLoader parent, File file, String fileId) {
        super(new URL[]{getURL(file)}, parent);
        JarFile jarFile1;

        if (parent instanceof PreClassLoader) {
            this.mainClassLoader = (PreClassLoader) parent;
        }
        this.sources = new ArrayList<>();
        this.fileId = fileId;
        this.modSource = file;

        if (file.isFile()) {
            try {
                jarFile1 = new JarFile(file);
            } catch (IOException e) {
                jarFile1 = null;
            }
            this.file = null;
        } else if (file.isDirectory()) {
            jarFile1 = null;
            this.file = file;
        } else {
            jarFile1 = null;
            this.file = null;
        }
        this.jarFile = jarFile1;
    }

    private static URL getURL(File file) {
        try {
            return file.toURI().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void addFile(File modFile) {
        String fileId = internalClassLoader.getModFileId(modFile);
    }

    public void addMod(String modFileId) throws MalformedURLException {
        this.sources.add(modFileId);
    }

    public boolean isInternalPackage(String pkg) {
        return mainClassLoader.isInternalPackage(pkg);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (Objects.equals(name, this.getClass().getName())) {
            throw new ClassNotFoundException("Class not found in current's classloader for mod: " + this.fileId);
        }

        try {
            Class<?> aClass = getParent().loadClass(name);
//            Class<?> aClass = Class.forName(name, false, getParent());
            if (aClass.isAnnotationPresent(AntiMod.class)) {
                throw new ClassNotFoundException(name);
            }

            return aClass;
        } catch (ClassNotFoundException e) {
            if (classes.containsKey(name)) {
                Class<?> aClass = classes.get(name);
                if (aClass.isAnnotationPresent(AntiMod.class)) {
                    throw new ClassNotFoundException(name);
                }
                return aClass;
            }

            byte[] data;
            try {
                data = getData(name);
            } catch (IOException exception) {
                throw new ClassNotFoundException("IO exception occurred when reading data for class.");
            }
            Class<?> definedClass = defineClass(name, data, 0, data.length);

            classes.put(name, definedClass);

            if (definedClass.isAnnotationPresent(AntiMod.class)) {
                throw new ClassNotFoundException(name);
            }

            return definedClass;
        }
    }

    private byte[] getData(String name) throws IOException {
        String classFilePath = name.replace('.', '/').concat(".class");

        if (file != null && file.isDirectory()) {
            File file = new File(this.file, classFilePath);
            try (FileInputStream input = new FileInputStream(file)) {
                byte[] bytes = input.readAllBytes();
                input.close();
                return bytes;
            } catch (IOException ignored) {

            }
        } else if (jarFile != null) {
            JarEntry jarEntry = jarFile.getJarEntry(classFilePath);
            try (InputStream input = jarFile.getInputStream(jarEntry)) {
                byte[] bytes = input.readAllBytes();
                input.close();
                return bytes;
            } catch (IOException ignored) {

            }
        }

        throw new IOException("Isn't either a directory or jar-file.");
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        if (Objects.equals(name, this.getClass().getName())) {
            throw new ClassNotFoundException("Class not found in current's classloader for mod: " + this.fileId);
        }

        if (name.startsWith("tk.ultreonteam.bubbles.core.")) {
            throw new ClassNotFoundException("Class not found in current's classloader for mod: " + this.fileId);
        }

        if (classes.containsKey(name)) {
            return classes.get(name);
        }

        Class<?> aClass = null;
        try {
            aClass = internalClassLoader.findClass(name);
        } catch (ClassNotFoundException e) {
            try {
                return super.findClass(name);
            } catch (ClassNotFoundException e1) {
                for (String file : sources) {
                    Class<?> classOrNull = internalClassLoader.getClassOrNull(file, name);
                    if (classOrNull != null) {
                        return classOrNull;
                    }
                }
            }
        }
        if (aClass == null || aClass.isAnnotationPresent(AntiMod.class)) {
            throw new ClassNotFoundException("Class not found in current's classloader for mod: " + this.fileId);
        }

        return aClass;
    }

    tk.ultreonteam.bubbles.mod.loader.Scanner.Result scan() {
        tk.ultreonteam.bubbles.mod.loader.Scanner scanner = new Scanner(modSource, this);
        return scanner.scan();
    }

    public File getModSource() {
        return modSource;
    }
}