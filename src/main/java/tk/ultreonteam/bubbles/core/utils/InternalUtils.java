package tk.ultreonteam.bubbles.core.utils;

import tk.ultreonteam.commons.annotation.FieldsAreNonnullByDefault;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
@MethodsReturnNonnullByDefault
@FieldsAreNonnullByDefault
public class InternalUtils {

}
