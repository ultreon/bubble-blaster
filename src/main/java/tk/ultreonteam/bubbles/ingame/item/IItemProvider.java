package tk.ultreonteam.bubbles.ingame.item;

public interface IItemProvider {
    ItemType getItem();
}
