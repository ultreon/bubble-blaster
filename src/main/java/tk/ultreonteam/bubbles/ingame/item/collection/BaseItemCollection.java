package tk.ultreonteam.bubbles.ingame.item.collection;

import tk.ultreonteam.bubbles.ingame.item.Item;

public interface BaseItemCollection {
    int size();

    Item[] getItems();

    void setItem(int index, Item item);

    void clear();

    Item getItem(int slot);

    void removeItem(int index);

    void tick();
}
