package tk.ultreonteam.bubbles.ingame.gamemode;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.ingame.bubble.AbstractBubble;
import tk.ultreonteam.bubbles.ingame.bubble.BubbleSpawnContext;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.random.BubbleRandomizer;
import tk.ultreonteam.bubbles.common.random.Rng;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.init.Bubbles;
import tk.ultreonteam.bubbles.init.Entities;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.save.GameSave;
import tk.ultreonteam.bubbles.settings.GameSettings;
import tk.ultreonteam.bubbles.util.ExceptionUtils;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;
import tk.ultreonteam.commons.crash.CrashLog;
import tk.ultreonteam.commons.lang.Messenger;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.NoSuchElementException;

/**
 * @author Qboi
 */
@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
@SuppressWarnings({"unused", "deprecation"})
public class ClassicMode extends Gamemode {
    // Hud and events-active flag.
    private ClassicModeHud hud;
    private boolean valid;

    // Threads
    private Thread spawner;

    public ClassicMode() {
        super();
    }

    /**
     * First-time loading.
     * It initializes the bubbles by placing them at random locations, including the x-axis.
     *
     * @param environment environment that's loading.
     * @param messenger   messaging system for showing loading information when a save is getting loaded.
     */
    @Override
    public void initEnv(Environment environment, Messenger messenger) {
        int maxBubbles = GameSettings.instance().getMaxBubbles();

        try {
            this.hud = new ClassicModeHud(this);

            // Spawn bubbles
            messenger.send("Spawning bubbles...");

            BubbleRandomizer randomizer = environment.getBubbleRandomizer();
            Rng xRng = randomizer.getXRng();
            Rng yRng = randomizer.getYRng();
            long spawnIndex = -1;
            for (int i = 0; i < maxBubbles; i++) {
                AbstractBubble bubble = environment.getRandomBubble(spawnIndex);
                int retry = 0;
                while (!bubble.canSpawn(environment)) {
                    bubble = environment.getRandomBubble(spawnIndex);
                    if (++retry == 5) {
                        spawnIndex--;
                    }
                }

                if (bubble != Bubbles.LEVEL_UP.get()) {
                    Point pos = new Point(xRng.getNumber(0, BubbleBlaster.getInstance().getWidth(), -i - 1), yRng.getNumber(0, BubbleBlaster.getInstance().getWidth(), -i - 1));
                    BubbleSpawnContext.inContext(spawnIndex, retry, () -> {
                        environment.spawn(Entities.BUBBLE.get().create(environment), pos);
                    });
                }

                spawnIndex--;

                messenger.send("Spawning bubble " + i + "/" + maxBubbles);
            }

            // Spawn player
            messenger.send("Spawning player...");
            game.loadPlayEnvironment();
            environment.spawn(game.player, new Point(game.getScaledWidth() / 4, BubbleBlaster.getInstance().getHeight() / 2));
        } catch (Exception e) {
            CrashLog crashLog = new CrashLog("Could not initialize classic game type.", e);

            BubbleBlaster.getInstance().crash(crashLog.createCrash());
        }

        this.make();
        this.initialized = true;
    }

    @Override
    public void onLoad(Environment environment, GameSave save, Messenger messenger) {
        this.hud = new ClassicModeHud(this);
        this.initialized = true;
    }

    /**
     * Load Game Type.
     * Used for initializing the game.
     */
    @Override
    public void start() {
//        spawner.start();
    }

    @SuppressWarnings("EmptyMethod")
    public void spawnerThread() {

    }

    @Override
    @Deprecated
    public void render(Renderer gg) {

    }

    @Override
    @Deprecated
    public @NonNull CompoundTag save() {
        BubbleBlaster.getLogger().warn(ExceptionUtils.getStackTrace("Deprecated call on Gamemode.save."));
        return new CompoundTag();
    }

    @Override
    @Deprecated
    public void load(CompoundTag tag) {
        BubbleBlaster.getLogger().warn(ExceptionUtils.getStackTrace("Deprecated call on Gamemode.load."));
    }

    @Override
    @Deprecated
    public boolean repair(GameSave gameSave) {
        BubbleBlaster.getLogger().warn(ExceptionUtils.getStackTrace("Deprecated call on Gamemode.repair."));
        return false;
    }

    @Override
    @Deprecated
    public boolean convert(GameSave gameSave) {
        BubbleBlaster.getLogger().warn(ExceptionUtils.getStackTrace("Deprecated call on Gamemode.convert."));
        return false;
    }

    @Override
    public int getGameTypeVersion() {
        return 0;
    }

    @Override
    public void renderHUD(Renderer gg) {
        hud.renderHUD(gg);
    }

    @Override
    public void renderGUI(Renderer gg) {

    }

    public ClassicModeHud getHud() {
        return this.hud;
    }

    @Override
    public Rectangle2D getGameBounds() {
        return new Rectangle2D.Double(70d, 0d, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight() - 70d);
    }

    @Override
    public Player getPlayer() {
        return this.game.player;
    }

    /**
     * Trigger Game Over
     * Deletes player and set game over flag in ClassicHUD. Showing Game Over message.
     *
     * @see ClassicModeHud#setGameOver()
     */
    @Override
    public void onGameOver() {
//        environment.gameOver(game.player);
        game.player.delete();
        hud.setGameOver();
//        gameOver = true;
    }

    @Override
    public Point getSpawnLocation(Entity entity, Identifier usageId, long spawnIndex, int retry) {
        return new Point(
                (int) getGameBounds().getMaxX() + entity.getBounds().width,
                (int) entity.getYRng().getNumber(getGameBounds().getMinY() - entity.getBounds().height, getGameBounds().getMaxY() + entity.getBounds().height, usageId.toString().toCharArray(), spawnIndex, retry)
        );
    }

    @Override
    public void onQuit() {
        this.hud = null;
    }

    public void make() {
        BubbleBlaster.getEventBus().subscribe(this);
        this.valid = true;
    }

    public void destroy() throws NoSuchElementException {
        BubbleBlaster.getEventBus().unsubscribe(this);
        this.valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public Thread getSpawner() {
        return spawner;
    }

    @Override
    public long getEntityId(Entity entity, Environment environment, long spawnIndex, int retry) {
        return spawnIndex;
    }
}
