package tk.ultreonteam.bubbles.ingame.entity;

import org.checkerframework.checker.nullness.qual.NonNull;
import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.command.Command;
import tk.ultreonteam.bubbles.ingame.Environment;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.awt.*;

public class SpawnInformation {
    private SpawnReason reason;
    @Nullable
    private Point pos;
    private @Nullable CompoundTag tag;
    @Nullable
    private Command command;
    private Environment environment;

    public enum SpawnReason {
        LOAD, NATURAL, COMMAND
    }

    private SpawnInformation(SpawnReason reason, @Nullable CompoundTag tag, @Nullable Point pos, @Nullable Command command, Environment environment) {
        this.pos = pos;
        this.tag = tag;
        this.reason = reason;
        this.command = command;
        this.environment = environment;
    }

    public static SpawnInformation fromLoadSpawn(@NonNull CompoundTag tag) {
        return new SpawnInformation(SpawnReason.LOAD, tag, null, null, BubbleBlaster.getInstance().environment);
    }

    public static SpawnInformation fromNaturalSpawn(Point pos) {
        return new SpawnInformation(SpawnReason.NATURAL, null, pos, null, BubbleBlaster.getInstance().environment);
    }

    public static SpawnInformation fromNaturalSpawn(Point pos, Environment environment) {
        return new SpawnInformation(SpawnReason.NATURAL, null, pos, null, environment);
    }

    public static SpawnInformation fromCommand(Command command) {
        return new SpawnInformation(SpawnReason.COMMAND, null, null, command, BubbleBlaster.getInstance().environment);
    }

    public static SpawnInformation fromCommand(Command command, Environment environment) {
        return new SpawnInformation(SpawnReason.COMMAND, null, null, command, environment);
    }

    public SpawnReason getReason() {
        return reason;
    }

    public void setReason(SpawnReason reason) {
        this.reason = reason;
    }

    public @Nullable CompoundTag getTag() {
        return tag;
    }

    public void setTag(@Nullable CompoundTag tag) {
        if (this.tag != null) {
            this.tag = tag;
        } else {
            throw new NullPointerException("Command property was initialized with null.");
        }
    }

    @Nullable
    public Command getCommand() {
        return command;
    }

    public void setCommand(@Nullable Command command) {
        if (this.tag != null) {
            this.command = command;
        } else {
            throw new NullPointerException("Command property was initialized with null.");
        }
    }

    @Nullable
    public Point getPos() {
        return pos;
    }

    public void setPos(@Nullable Point pos) {
        if (this.pos != null) {
            this.pos = pos;
        } else {
            throw new NullPointerException("Command property was initialized with null.");
        }
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
