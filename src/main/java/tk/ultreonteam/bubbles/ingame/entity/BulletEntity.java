package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.ingame.entity.ammo.AmmoType;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.init.AmmoTypes;
import tk.ultreonteam.bubbles.init.Entities;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.util.Wip;
import tk.ultreonteam.commons.annotation.FieldsAreNonnullByDefault;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.annotation.ParametersAreNonnullByDefault;
import java.awt.*;

@Wip
@MethodsReturnNonnullByDefault
@FieldsAreNonnullByDefault
@ParametersAreNonnullByDefault
public class BulletEntity extends Entity {
    @NonNull
    private AmmoType ammoType;

    public BulletEntity(Environment environment) {
        this(AmmoTypes.BASIC.get(), 0, 0, 0, environment);
    }

    public BulletEntity(@NonNull AmmoType type, float x, float y, int rotation, Environment environment) {
        super(Entities.AMMO.get(), environment);

        this.x = x;
        this.y = y;
        this.setRotation(rotation);

        this.ammoType = type;
        this.attributes.setAll(type.getDefaultAttributes());

        setSpeed(type.getSpeed());

        addCollidable(Entities.BUBBLE.get());
        addCollidable(Entities.GIANT_BUBBLE.get());
    }

    private void setSpeed(float speed) {
        attributes.setBase(Attribute.SPEED, speed);
    }

    @Override
    public void onCollision(Entity other, double frameTime) {
        ammoType.onCollision(this, other, frameTime);
    }

    @Override
    public Shape getShape() {
        return ammoType.getShape(this);
    }

    @Override
    public void render(Renderer gg) {
        this.ammoType.render(gg, this);
    }

    @Override
    protected void create() {

    }

    @Override
    protected void invalidate() {

    }

    @Override
    public void tick(Environment environment) {
        velX = (float) (getSpeed() * Math.sin(getRotation()));
        velY = (float) (getSpeed() * Math.cos(getRotation()));

        super.tick(environment);
    }

    private double getSpeed() {
        return attributes.getBase(Attribute.SPEED);
    }

    @NonNull
    public AmmoType getAmmoType() {
        return ammoType;
    }

    @SuppressWarnings("unused")
    public void setAmmoType(@NonNull AmmoType ammoType) {
        this.ammoType = ammoType;
        this.attributes.setAll(ammoType.getDefaultAttributes());
        this.attributes.clearModifiers();
    }
}
