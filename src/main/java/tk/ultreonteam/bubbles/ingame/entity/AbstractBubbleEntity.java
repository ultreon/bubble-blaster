package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.ingame.Environment;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.awt.*;

/**
 * ItemType Entity base class
 * For entities such as a {@link BubbleEntity bubble}
 *
 * @see Entity
 */
@SuppressWarnings("unused")
public abstract class AbstractBubbleEntity extends ActiveEntity {
    // Constructor
    public AbstractBubbleEntity(EntityType<?> type, Environment environment) {
        super(type, environment);
    }

    @Override
    public void onSpawn(Point pos, Environment environment) {
        this.damageValue = getMaxDamageValue();
    }

    public void restoreDamage(double value) {
        if (damageValue + value > getMaxDamageValue()) {
            this.damageValue = getMaxDamageValue();
            return;
        }
        this.damageValue += value;
    }

    @Override
    public @NonNull CompoundTag save() {
        return super.save();
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
    }

    public String toSimpleString() {
        return id() + "@(" + Math.round(getX()) + "," + Math.round(getY()) + ")";
    }
}
