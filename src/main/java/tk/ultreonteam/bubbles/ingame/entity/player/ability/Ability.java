package tk.ultreonteam.bubbles.ingame.entity.player.ability;

import tk.ultreonteam.bubbles.ingame.entity.player.ability.triggers.AbilityKeyTrigger;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.triggers.types.AbilityKeyTriggerType;
import tk.ultreonteam.bubbles.common.interfaces.StateHolder;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;

public abstract class Ability<T extends Ability<T>> implements StateHolder {
    private final AbilityType<T> type;
    private int cooldown;
    private int value;

    public Ability(AbilityType<T> type) {
        this.type = type;
    }

    @SuppressWarnings("SameReturnValue")
    public abstract int getTriggerKey();

    /**
     * Method for the trigger type of the ability.
     *
     * @return the trigger type.
     */
    @SuppressWarnings("SameReturnValue")
    public abstract AbilityTriggerType getTriggerType();

    /**
     * Method for have a key trigger for the Ability.
     *
     * @return the key trigger type. Null for no key trigger.
     */
    public AbilityKeyTriggerType getKeyTriggerType() {
        return null;
    }

    /**
     * Method for key trigger event.
     *
     * @param trigger the key trigger.
     */
    public void onKeyTrigger(AbilityKeyTrigger trigger) {

    }

    @Override
    public @NonNull CompoundTag save() {
        CompoundTag document = new CompoundTag();
        document.putInt("Cooldown", this.cooldown);
        document.putInt("Value", this.value);

        return document;
    }

    public void onEntityTick() {
        if (isRegeneratable()) {
            this.value += getRegenerationSpeed();
        }
    }

    @Override
    public void load(CompoundTag tag) {
        this.cooldown = tag.getInt("Cooldown");
        this.value = tag.getInt("Value");
    }

    public abstract void trigger(AbilityTrigger trigger);

    @SuppressWarnings("EmptyMethod")
    public abstract void triggerEntity();

    public abstract boolean isTriggerable(@SuppressWarnings("unused") Entity entity);

    @SuppressWarnings("SameReturnValue")
    public abstract boolean isRegeneratable();

    @SuppressWarnings("SameReturnValue")
    public int getRegenerationSpeed() {
        return 1;
    }

    public AbilityType<T> getType() {
        return type;
    }

    public int getCooldown() {
        return cooldown;
    }

    public int getValue() {
        return value;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void subtractValue(int amount) {
        this.value -= amount;
    }

    public void addValue(int amount) {
        this.value -= amount;
    }
}
