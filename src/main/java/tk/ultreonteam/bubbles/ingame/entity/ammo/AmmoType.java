package tk.ultreonteam.bubbles.ingame.entity.ammo;

import com.google.common.annotations.Beta;
import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.ingame.entity.BulletEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.item.ItemType;
import tk.ultreonteam.bubbles.render.Renderer;

import java.awt.*;

/**
 * Ammo type, handles things for {@link BulletEntity}.
 * This also adds a new {@link ItemType} to the registry.
 *
 * @author Qboi
 */
@Beta
public abstract class AmmoType extends Registrable {
    public abstract void render(Renderer g, BulletEntity rotation);

    /**
     * Handles collision for the given bullet if the bullet has {@link AmmoType this} as ammo type.
     *
     * @param bullet    bullet entity.
     * @param collided  the other entity that the bullet collided with.
     * @param deltaTime the collision delta time.
     */
    public void onCollision(BulletEntity bullet, Entity collided, double deltaTime) {

    }

    public abstract AttributeContainer getDefaultAttributes();

    public abstract Shape getShape(BulletEntity bulletEntity);

    @Override
    public String toString() {
        return "AmmoType[" + id() + "]";
    }

    public abstract float getSpeed();
}
