package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.random.BubbleRandomizer;
import tk.ultreonteam.bubbles.common.random.Rng;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.ingame.bubble.AbstractBubble;
import tk.ultreonteam.bubbles.ingame.bubble.BubbleProperties;
import tk.ultreonteam.bubbles.ingame.bubble.BubbleSpawnContext;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.ingame.entity.damage.EntityDamageSource;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.init.Bubbles;
import tk.ultreonteam.bubbles.init.Entities;
import tk.ultreonteam.bubbles.init.TextureCollections;
import tk.ultreonteam.bubbles.registry.Registry;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.TextureCollection;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.util.Util;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.Random;

/**
 * Bubble Entity.
 * One of the most important parts of the game.
 *
 * @see AbstractBubbleEntity
 */
@SuppressWarnings({"SameParameterValue", "unused"})
public class BubbleEntity extends AbstractBubbleEntity {
    public static final int VARIANT_RNG = 0;
    public static final int TYPE_RNG = 1;
    // Attributes
    protected int radius;
    protected int baseRadius;
    private boolean doesBounce;
    protected float bounceAmount;
    protected float baseBounceAmount;

    // Bubble type.
    protected AbstractBubble bubbleType;

    // Entity type.
    private static final EntityType<BubbleEntity> entityType = Entities.BUBBLE.get();

    private final Random random = new Random(Math.round((double) (System.currentTimeMillis() / 86400000))); // Random day. 86400000 milliseconds == 1 day.
    private boolean effectApplied = false;

    public static EntityType<? extends BubbleEntity> getRandomType(Environment environment, Rng rng) {
        if (rng.getNumber(0, 3_000, TYPE_RNG) == 0) {
            return Entities.GIANT_BUBBLE.get();
        }
        return Entities.BUBBLE.get();
    }

    public BubbleEntity(Environment environment) {
        super(entityType, environment);

        // Add player as collidable.
        this.addCollidable(Entities.PLAYER.get());

        // Get random properties
        BubbleRandomizer randomizer = this.environment.getBubbleRandomizer();
        BubbleSpawnContext ctx = BubbleSpawnContext.get();
        BubbleProperties properties = randomizer.getRandomProperties(environment.game().getGameBounds(), ctx.spawnIndex(), ctx.retry(), environment);

        // Bubble Type
        this.bubbleType = properties.getType().canSpawn(environment) ? properties.getType() : Bubbles.NORMAL.get();

        // Dynamic values
        this.radius = properties.getRadius();
        this.baseRadius = properties.getRadius();
        this.attributes.setBase(Attribute.MAX_DAMAGE, properties.getDamageValue());
        this.damageValue = properties.getDamageValue();

        // Set attributes.
        this.attributes.setBase(Attribute.ATTACK, properties.getAttack());
        this.attributes.setBase(Attribute.DEFENSE, properties.getDefense());
        this.attributes.setBase(Attribute.SCORE, properties.getScoreMultiplier());
        this.attributes.setBase(Attribute.SPEED, properties.getSpeed());
        this.bounceAmount = bubbleType.getBounceAmount();
        this.doesBounce = bubbleType.getDoesBounce();

        // Set velocity
        this.velX = -getBaseSpeed();

        // Set attributes.
        this.attributes.setBase(Attribute.DEFENSE, 0.5f);

        addAttackable(Entities.PLAYER.get());
    }

    public boolean doesBounce() {
        return doesBounce;
    }

    public void setDoesBounce(boolean doesBounce) {
        this.doesBounce = doesBounce;
    }

    @Override
    public void onCollision(Entity other, double frameTime) {
        this.bubbleType.onCollision(this, other);

        if (doesBounce && other instanceof Player player) {
            player.bounceOff(this, bounceAmount, -0.01f);
        }
    }

    /**
     * Spawn Event Handler
     * On-spawn.
     *
     * @param spawnData the entity's spawn data.
     */
    @Override
    public void prepareSpawn(SpawnInformation spawnData) {
        super.prepareSpawn(spawnData);

        create();
    }

    /**
     * @return the bubble-entity's bounds.
     */
    @Override
    public Rectangle getBounds() {
        Rectangle rectangle = super.getBounds();
        rectangle.width += 4;
        rectangle.height += 4;
        return rectangle;
    }

    /**
     * Tick bubble entity.
     *
     * @param environment the environment where the entity is from.
     */
    @Override
    public void tick(Environment environment) {
        // Check player and current scene.
        Player player = this.environment.getPlayer();
        @Nullable Screen currentScreen = Util.getSceneManager().getCurrentScreen();
        boolean gameLoaded = BubbleBlaster.getInstance().isInGame();

        if (player == null || !gameLoaded) {
            return;
        }

        // Set velocity speed.
        velX = -getSpeed() * (this.environment.getPlayer().getLevel() / 2d + 1);

        velX *= getEnvironment().getGlobalBubbleSpeedModifier();

        super.tick(environment);

        if (this.x < -this.radius) {
            delete();
        }
    }

    /**
     * Rendering method.<br>
     * <b>SHOULD NOT BE CALLED!</b>
     *
     * @param renderer renderer to render the bubble entity with.
     */
    @Override
    public void render(Renderer renderer) {
        if (willBeDeleted()) return;
        renderer.image(TextureCollections.BUBBLE_TEXTURES.get().get(new TextureCollection.Index(getBubbleType().id().location(), getBubbleType().id().path() + "/" + radius)), (int) x - radius / 2, (int) y - radius / 2);
    }

    /**
     * @return the bubble's shape.
     */
    @Override
    public Ellipse2D getShape() {
        int rad = radius + (bubbleType.colors.length * 2);
        return new Ellipse2D.Double(this.x - (float) rad / 2, this.y - (float) rad / 2, rad, rad);
    }

    ///////////////////////////////////////////////////////////////////////////
    //     Attributes     //
    ////////////////////////

    // Radius.
    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public int getBaseRadius() {
        return baseRadius;
    }

    // Bounce amount.
    public void setBounceAmount(float bounceAmount) {
        this.bounceAmount = bounceAmount;
    }

    public float getBounceAmount() {
        return bounceAmount;
    }

    public float getBaseBounceAmount() {
        return baseBounceAmount;
    }

    ///////////////////////////////////////////////////////////////////////////
    //     Bubble Type     //
    /////////////////////////
    public AbstractBubble getBubbleType() {
        return bubbleType;
    }

    public void setBubbleType(AbstractBubble type) {
        this.bubbleType = type;
    }

    @Override
    public void checkShouldRemove() {
        if (this.damageValue <= 0d || radius < 0) {
            delete();
        }
    }

    @Override
    public void setDamageValue(double hardness) {
        super.setDamageValue(hardness);
        radius = (int) hardness + 4;
        checkShouldRemove();
    }

    @Override
    public void damage(double value, EntityDamageSource source) {
        super.damage(value / attributes.getBase(Attribute.DEFENSE), source);
        radius = (int) damageValue + 4;
    }

    @Override
    public @NonNull CompoundTag save() {
        @NonNull CompoundTag document = super.save();
        document.putInt("Radius", radius);
        document.putInt("BaseRadius", baseRadius);

        document.putFloat("BounceAmount", bounceAmount);
        document.putFloat("BaseBounceAmount", baseBounceAmount);

        document.putBoolean("IsEffectApplied", effectApplied);
        document.putString("BubbleName", bubbleType.id().toString());

        return document;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);

        this.radius = tag.getInt("Radius");
        this.baseRadius = tag.getInt("BaseRadius");

        this.bounceAmount = tag.getFloat("BounceAmount");
        this.baseBounceAmount = tag.getFloat("BaseBounceAmount");

        Identifier bubbleTypeKey = Identifier.parse(tag.getString("bubbleType"));
        this.effectApplied = tag.getBoolean("IsEffectApplied");
        this.bubbleType = Registry.getRegistry(AbstractBubble.class).get(bubbleTypeKey);
    }

    public void setEffectApplied(boolean effectApplied) {
        this.effectApplied = effectApplied;
    }

    public boolean isEffectApplied() {
        return effectApplied;
    }
}
