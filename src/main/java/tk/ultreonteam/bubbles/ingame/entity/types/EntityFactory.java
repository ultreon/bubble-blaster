package tk.ultreonteam.bubbles.ingame.entity.types;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.Environment;

public interface EntityFactory<T extends Entity> {
    T create(Environment environment);
}
