package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.GameObject;
import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.interfaces.StateHolder;
import tk.ultreonteam.bubbles.common.random.Rng;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.ingame.effect.StatusEffectInstance;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.AbilityType;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.util.Wip;
import tk.ultreonteam.bubbles.vector.Vec2d;
import net.querz.nbt.tag.CompoundTag;
import net.querz.nbt.tag.ListTag;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Entity base class.
 * The base for all entities, such as the player or a bubble.
 *
 * @see ActiveEntity
 * @see AbstractBubbleEntity
 * @since 1.0.0
 * @author Qboi
 */
@SuppressWarnings({"unused", "UnusedReturnValue"})
public abstract class Entity extends GameObject implements StateHolder {
    // ID's
    private long entityId;
    private UUID uniqueId;

    // Misc
    protected Shape shape;

    private final Set<EntityType<?>> collidables = new HashSet<>();
    private final Set<EntityType<?>> attackables = new HashSet<>();
    private final Set<EntityType<?>> invulnerables = new HashSet<>();
    private final List<StatusEffectInstance> activeEffects = new CopyOnWriteArrayList<>();

    // Attributes
    private double scale = 1;
    private float rotation;

    protected AttributeContainer attributes = new AttributeContainer();

    // Position and velocity.
    protected double x, y;
    protected double prevX, prevY;
    protected double velX,  velY;

    // Types
    private final EntityType<?> type;

    // Flags

    private boolean markedForDeletion;
    private boolean motionEnabled = true;
    private boolean valid;
    private boolean spawned;

    protected final Environment environment;

    // Tags.
    private CompoundTag tag = new CompoundTag();

    // Abilities.
    private final HashMap<AbilityType<?>, CompoundTag> abilities = new HashMap<>();

    // Position RNG.
    private final Rng xRng, yRng;

    // Constructor

    /**
     * Entity constructor.
     *
     * @param type type of entity.
     * @param environment the environment the entity will be in.
     */
    public Entity(EntityType<?> type, Environment environment) {
        this.environment = environment;
        this.entityId = environment.getEntityId(this);
        this.type = type;
        this.uniqueId = UUID.randomUUID();

        yRng = environment.getBubbleRandomizer().getYRng();
        xRng = environment.getBubbleRandomizer().getXRng();
    }

    /**
     * Handles collision with another entity.
     * Note: Direct calls aren't recommended, unless done as super call.
     *
     * @param other the other entity that collided with this entity.
     * @param frameTime the time since the last frame.
     * @since 0.1.0
     * @author Qboi
     */
    public abstract void onCollision(Entity other, double frameTime);

    /**
     * Get the unique id (UUID) of this entity.
     *
     * @return the entity's uuid.
     * @since 0.1.0
     * @author Qboi
     */
    public UUID getUniqueId() {
        return uniqueId;
    }

    /**
     * Get the id of this entity.
     *
     * @return the entity's id.
     * @since 0.1.0
     * @author Qboi
     */
    public long getEntityId() {
        return entityId;
    }

    /**
     * Prepare the entity spawn.
     *
     * @param spawnData the date to spawn the entity with.
     * @since 0.1.0
     * @author Qboi
     */
    public void prepareSpawn(SpawnInformation spawnData) {
        Point pos = spawnData.getPos();
        if (pos != null) {
            this.x = pos.x;
            this.y = pos.y;
        }
    }

    /**
     * Handles post-spawn.
     *
     * @param pos         the position to spawn at.
     * @param environment te environment to spawn in.
     * @since 0.1.0
     * @author Qboi
     */
    public void onSpawn(Point pos, Environment environment) {
        spawned = true;
    }

    /**
     * Check the if the entity is valid.
     *
     * @return whether the entity is spawned and valid.
     * @since 0.1.0
     * @author Qboi
     */
    protected final boolean isValid() {
        return valid && !getClass().isAnnotationPresent(Wip.class);
    }

    /**
     * Entity ticking handler.
     * Note: Direct calls aren't recommended, unless done as super call.
     *
     * @param environment the environment where the entity is from.
     * @since 0.1.0
     * @author Qboi
     */
    public void tick(Environment environment) {
        for (StatusEffectInstance statusEffectInstance : this.activeEffects) {
            statusEffectInstance.tick(this);
        }

        this.activeEffects.removeIf((effectInstance -> effectInstance.getRemainingTime() < 0d));

        this.prevX = x;
        this.prevY = y;
        this.x += this.motionEnabled ? this.velX / BubbleBlaster.TPS : 0;
        this.y += this.motionEnabled ? this.velY / BubbleBlaster.TPS : 0;
    }

    /**
     * Marks the entity for deletion.
     * This will also invalidate the entity.
     *
     * @since 0.1.0
     * @author Qboi
     */
    public final void delete() {
        if (isValid()) {
            invalidate();
            valid = false;
        }

        onDelete();

        markedForDeletion = true;
    }

    /**
     * Handler for deletion.
     *
     * @since 0.1.0
     * @author Qboi
     */
    protected void onDelete() {

    }

    /**
     * Get the shape of the entity.
     *
     * @return the requested shape.
     * @since 0.1.0
     * @author Qboi
     */
    public abstract Shape getShape();

    //***************************//
    //     Equals & HashCode     //
    //***************************//
    /**
     * Checks if this entity is equal to another entity.
     *
     * @param other the other entity to check.
     * @return whether the entities are equal.
     * @since 0.1.0
     * @author Qboi
     */
    @Override
    @SubscribeEvent
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Entity entity = (Entity) other;
        return entityId == entity.entityId;
    }

    /**
     * Gets the hash code of this entity.
     *
     * @return the hash code of this entity.
     * @since 0.1.0
     * @author Qboi
     */
    @Override
    public int hashCode() {
        return (int) (entityId ^ (entityId >>> 32));
    }

    /**
     * Create the entity.
     *
     * @since 0.1.0
     * @author Qboi
     */
    protected void create() {
        valid = true;
    }

    /**
     * Invalidate the entity.
     *
     * @since 0.1.0
     * @author Qboi
     */
    protected void invalidate() {
        valid = false;
    }

    /**
     * Get the bounds of the entity.
     *
     * @return the bounds of the entity.
     */
    public Rectangle getBounds() {
        Shape shapeObj = getShape();
        return shapeObj.getBounds();
    }

    /**
     * Get the id of the entity type.
     *
     * @return the entity type id.
     */
    public final Identifier id() {
        return this.type.id();
    }

    /**
     * Get the type of the entity.
     *
     * @return the entity type.
     */
    public EntityType<?> getType() {
        return type;
    }

    //*******************************//
    //     Teleport and Position     //
    //*******************************//

    /**
     * Teleport the entity to a new position.
     *
     * @param x the x position to teleport to.
     * @param y the y position to teleport to.
     */
    public final void teleport(double x, double y) {
        this.teleport(new Vec2d(x, y));
    }

    /**
     * Teleport the entity to a new position.
     *
     * @param pos the position to teleport to.
     */
    public final void teleport(Vec2d pos) {
        if (onTeleporting(getPos().clone(), pos)) return;
        this.x = (float) pos.getX();
        this.y = (float) pos.getY();
        onTeleported(getPos().clone(), pos.clone());
    }

    /**
     * Get the position of the entity.
     *
     * @return the position of the entity.
     */
    public Vec2d getPos() {
        return new Vec2d(getX(), getY());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Teleport events     //
    /////////////////////////////
    /**
     * Called when the entity has been teleported.
     * The method {@link #onTeleporting(Vec2d, Vec2d)} is called before teleporting, this method is called after it's teleported.
     *
     * @param from the position the entity is teleporting from.
     * @param to the position the entity is teleporting to.
     * @see #onTeleporting(Vec2d, Vec2d)
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("EmptyMethod")
    public void onTeleported(Vec2d from, Vec2d to) {

    }

    /**
     * Called if the entity is teleporting.
     * The method {@link #onTeleported(Vec2d, Vec2d)} is called after it's teleported, this method is called before teleporting.
     *
     * @param from the position the entity is teleporting from.
     * @param to the position the entity is teleporting to.
     * @return whether the teleport should be cancelled.
     * @see #onTeleported(Vec2d, Vec2d)
     * @since 0.1.0
     * @author Qboi
     */
    @SuppressWarnings("EmptyMethod")
    public boolean onTeleporting(Vec2d from, Vec2d to) {
        return false;
    }

    //****************//
    //     Motion     //
    //****************//

    /**
     * Move the entity by a certain amount of pixels.
     *
     * @param deltaX the amount of pixels to move on the x-axis.
     * @param deltaY the amount of pixels to move on the y-axis.
     * @since 0.1.0
     * @author Qboi
     */
    public void move(double deltaX, double deltaY) {
        this.x += deltaX;
        this.y += deltaY;
    }

    /**
     * Move the entity by a certain amount of pixels.
     *
     * @param delta the amount of pixels to move.
     * @since 0.1.0
     * @author Qboi
     */
    public void move(Vec2d delta) {
        this.x += delta.getX();
        this.y += delta.getY();
    }

    /**
     * Get the velocity of the entity.
     *
     * @return the velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public Vec2d getVelocity() {
        return new Vec2d(velX, velY);
    }

    /**
     * Set the velocity of the entity.
     *
     * @param vel the velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public void setVelocity(Vec2d vel) {
        this.velX = vel.getX();
        this.velY = vel.getY();
    }

    /**
     * Set the velocity of the entity.
     *
     * @param velX the velocity on the x-axis.
     * @param velY the velocity on the y-axis.
     * @since 0.1.0
     * @author Qboi
     */
    public void setVelocity(float velX, float velY) {
        this.velX = velX;
        this.velY = velY;
    }

    /**
     * Get the x-velocity of the entity.
     *
     * @return the x-velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public double getVelX() {
        return velX;
    }

    /**
     * Set the x-velocity of the entity.
     *
     * @param velX the x-velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public void setVelX(float velX) {
        this.velX = velX;
    }

    /**
     * Get the y-velocity of the entity.
     *
     * @return the y-velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public double getVelY() {
        return velY;
    }

    /**
     * Set the y-velocity of the entity.
     *
     * @param velY the y-velocity of the entity.
     * @since 0.1.0
     * @author Qboi
     */
    public void setVelY(float velY) {
        this.velY = velY;
    }

    //**********************************************//
    //     Collidable / attackable / invulnerable     //
    //**********************************************//

    /**
     * Add a collidable entity to the list of collidables.
     *
     * @param entity the collidable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addCollidable(Entity entity) {
        collidables.add(entity.getType());
    }

    /**
     * Add a collidable entity to the list of collidables.
     *
     * @param entityType the type of the collidable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addCollidable(EntityType<?> entityType) {
        collidables.add(entityType);
    }

    /**
     * Add an attackable entity to the list of attackables.
     *
     * @param entity the attackable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addAttackable(Entity entity) {
        attackables.add(entity.getType());
    }

    /**
     * Add an attackable entity to the list of attackables.
     *
     * @param entityType the type of the attackable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addAttackable(EntityType<?> entityType) {
        attackables.add(entityType);
    }

    /**
     * Add an invulnerable entity to the list of invulnerable entities.
     *
     * @param entity the invulnerable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addInvulnerable(Entity entity) {
        invulnerables.add(entity.getType());
    }

    /**
     * Add an invulnerable entity to the list of invulnerable entities.
     *
     * @param entityType the type of the invulnerable entity to add.
     * @since 0.1.0
     * @author Qboi
     */
    public void addInvulnerable(EntityType<?> entityType) {
        invulnerables.add(entityType);
    }

    /**
     * Check if the entity is collidable.
     *
     * @param entity the entity to check.
     * @return whether the entity is collidable.
     * @since 0.1.0
     * @author Qboi
     * @deprecated use {@link #isCollidableWith(Entity)}
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public boolean isCollidingWith(Entity entity) {
        return isCollidableWith(entity);
    }

    //*****************//
    //     Effects     //
    //*****************//
    /**
     * Add an effect to the entity.
     *
     * @param effect the effect to add.
     * @since 0.1.0
     * @author Qboi
     */
    @Nullable
    public StatusEffectInstance addEffect(StatusEffectInstance effect) {
        for (StatusEffectInstance activeEffect : activeEffects) {
            if (activeEffect.getType() == effect.getType()) {
                if (activeEffect.getRemainingTime() < effect.getRemainingTime()) {
                    activeEffect.setRemainingTime(effect.getRemainingTime());
                }
                return activeEffect;
            }
        }
        activeEffects.add(effect);
        effect.start(this);
        return effect;
    }

    /**
     * Remove an effect from the entity.
     *
     * @param effect the effect to remove.
     * @since 0.1.0
     * @author Qboi
     */
    public void removeEffect(StatusEffectInstance effect) {
        activeEffects.remove(effect);
        if (effect.isActive()) {
            effect.stop(this);
        }
    }

    /**
     * Get the active effects of the entity.
     *
     * @return the active effects of the entity.
     * @author Qboi
     * @since 0.1.0
     */
    public List<StatusEffectInstance> getActiveEffects() {
        return activeEffects;
    }

    //*******************//
    //     To-String     //
    //*******************//

    /**
     * Get a simple string representation of the entity.
     *
     * @return a string representation of the entity.
     */
    public String toSimpleString() {
        return id() + ":(" + Math.round(getX()) + "," + Math.round(getY()) + ")";
    }

    /**
     * Get an advanced string representation of the entity.
     *
     * @return a string representation of the entity.
     */
    public final String toAdvancedString() {
        CompoundTag nbt = save();
        String data = nbt.toString();

        return id() + ":" + data;
    }

    //*********************//
    //     Save / Load     //
    //*********************//
    /**
     * Save the entity to a NBT tag.
     *
     * @return the NBT tag.
     * @since 0.1.0
     * @author Qboi
     */
    @Override
    public @NonNull CompoundTag save() {
        CompoundTag state = new CompoundTag();
        state.put("Tag", this.tag);
        state.put("Attributes", this.attributes.save());

        CompoundTag positionTag = new CompoundTag();
        positionTag.putDouble("x", x);
        positionTag.putDouble("y", y);
        state.put("Position", positionTag);

        CompoundTag previousTag = new CompoundTag();
        previousTag.putDouble("x", prevX);
        previousTag.putDouble("y", prevY);
        state.put("PrevPosition", previousTag);

        CompoundTag velocityTag = new CompoundTag();
        velocityTag.putDouble("x", velX);
        velocityTag.putDouble("y", velY);
        state.put("Velocity", velocityTag);

        state.putLong("id", this.entityId);
        state.putLongArray("uuid", new long[]{this.uniqueId.getMostSignificantBits(), this.uniqueId.getLeastSignificantBits()});
        state.putDouble("scale", this.scale);
        state.putString("type", this.type.id().toString());

        return state;
    }

    /**
     * Load the entity from a NBT tag.
     *
     * @param environment the environment to load the entity in.
     * @param tag the NBT tag.
     * @since 0.1.0
     * @author Qboi
     */
    @Nullable
    public static Entity load(Environment environment, CompoundTag tag) {
        Identifier type = Identifier.tryParse(tag.getString("type"));
        if (type == null) return null;
        EntityType<?> entityType = Registers.ENTITIES.get(type);
        return entityType == null ? null : entityType.create(environment, tag);
    }

    /**
     * Load the entity from a NBT tag.
     *
     * @param tag the NBT tag.
     * @since 0.1.0
     * @author Qboi
     */
    @Override
    public void load(CompoundTag tag) {
        this.tag = tag.getCompoundTag("Tag");
        this.attributes.load(tag.getListTag("Attributes").asCompoundTagList());

        CompoundTag positionTag = tag.getCompoundTag("Position");
        this.x = positionTag.getFloat("x");
        this.y = positionTag.getFloat("y");

        CompoundTag previousTag = tag.getCompoundTag("PrevPosition");
        this.prevX = previousTag.getFloat("x");
        this.prevY = previousTag.getFloat("y");

        CompoundTag velocityTag = tag.getCompoundTag("Velocity");
        this.velX = velocityTag.getFloat("x");
        this.velY = velocityTag.getFloat("y");

        ListTag<CompoundTag> activeEffectsTag = new ListTag<>(CompoundTag.class);
        for (StatusEffectInstance instance : activeEffects) {
            activeEffectsTag.add(instance.save());
        }

        this.entityId = tag.getLong("id");

        long[] uuidArray = tag.getLongArray("uuid");
        this.uniqueId = new UUID(uuidArray[0], uuidArray[1]);

        this.rotation = tag.getFloat("rotation");
    }

    /**
     * Set whether the entity has motion.
     * @param motionEnabled whether the entity has motion.
     */
    public void setMotionEnabled(boolean motionEnabled) {
        this.motionEnabled = motionEnabled;
    }

    /**
     * Get whether the entity has motion.
     * @return whether the entity has motion.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isMotionEnabled() {
        return motionEnabled;
    }

    /**
     * Get the ability tag of the entity.
     *
     * @param type the type of the ability.
     * @return the ability tag.
     * @since 0.1.0
     * @author Qboi
     */
    public CompoundTag getAbilityTag(AbilityType<?> type) {
        return this.abilities.get(type);
    }

    /**
     * Get the ability tag of the entity.
     * It creates a new tag if it doesn't exist.
     *
     * @param type the type of the ability.
     * @return the ability tag.
     * @since 0.1.0
     * @author Qboi
     */
    public CompoundTag getOrCreateAbilityTag(AbilityType<?> type) {
        return this.abilities.computeIfAbsent(type, $ -> new CompoundTag());
    }

    /**
     * Get the tag of the entity.
     *
     * @return the tag.
     * @since 0.1.0
     * @author Qboi
     */
    public CompoundTag getTag() {
        return tag;
    }

    /**
     * Get the attributes of the entity.
     *
     * @return the attributes.
     * @since 0.1.0
     * @author Qboi
     */
    public AttributeContainer getAttributes() {
        return attributes;
    }

    /**
     * Get the scale of the entity.
     * @return the scale.
     * @since 0.1.0
     * @author Qboi
     */
    public double getScale() {
        return scale;
    }

    /**
     * Set the scale of the entity.
     * @param scale the scale.
     * @since 0.1.0
     * @author Qboi
     */
    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * Get the x position of the entity.
     * @return the x position.
     * @since 0.1.0
     * @author Qboi
     */
    public double getX() {
        return x;
    }

    /**
     * Set the x position of the entity.
     * @param x the x position.
     * @since 0.1.0
     * @author Qboi
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Get the y position of the entity.
     * @return the y position.
     * @since 0.1.0
     * @author Qboi
     */
    public double getY() {
        return y;
    }

    /**
     * Set the y position of the entity.
     * @param y the y position.
     * @since 0.1.0
     * @author Qboi
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Get the rotation of the entity.
     * @return the rotation.
     * @since 0.1.0
     * @author Qboi
     */
    public double getRotation() {
        return rotation;
    }

    /**
     * Set the rotation of the entity.
     * @param rotation the rotation.
     * @since 0.1.0
     * @author Qboi
     */
    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    /**
     * Rotate the entity by a certain amount of degrees.
     * This will also wrap the rotation around 360 degrees.
     *
     * @param rotation the amount of degrees to rotate.
     * @since 0.1.0
     * @author Qboi
     */
    public void rotate(float rotation) {
        this.rotation = (this.rotation + rotation) % 360;
    }

    /**
     * Check if the entity is spawned.
     *
     * @return whether the entity is spawned.
     * @since 0.1.0
     * @author Qboi
     */
    public final boolean isSpawned() {
        return spawned;
    }

    /**
     * Check if the entity is NOT spawned.
     *
     * @return whether the entity is NOT spawned.
     * @since 0.1.0
     * @author Qboi
     */
    public final boolean hasNotSpawnedYet() {
        return !spawned;
    }

    /**
     * Get the environment the entity is in.
     *
     * @return the environment.
     * @since 0.1.0
     * @author Qboi
     */
    public Environment getEnvironment() {
        return environment;
    }

    /**
     * Check if the entity is marked for removal.
     *
     * @return whether the entity is marked for removal.
     * @since 0.1.0
     * @author Qboi
     */
    public final boolean willBeDeleted() {
        return markedForDeletion;
    }

    /**
     * Check if the entity should be visible.
     *
     * @return whether the entity should be visible.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isVisible() {
        return getBounds().intersects(BubbleBlaster.getInstance().getGameBounds());
    }

    /**
     * Check if the entity is collidable with another entity.
     *
     * @param other the other entity.
     * @return whether the entity is collidable with another entity.
     * @since 0.1.0
     * @author Qboi
     */
    public final boolean isCollidableWith(Entity other) {
        return collidables.contains(other.type);
    }

    /**
     * Check if the entity does attack another entity.
     *
     * @param other the other entity.
     * @return whether the entity does attack another entity.
     */
    public boolean doesAttack(Entity other) {
        return attackables.contains(other.type);
    }

    /**
     * Check if the entity is attackable by another entity.
     *
     * @param other the other entity.
     * @return whether the entity is attackable by another entity.
     */
    public boolean canBeAttackedBy(Entity other) {
        return !invulnerables.contains(other.type);
    }

    /**
     * Get the RNG for the x position.
     *
     * @return the RNG for the x position.
     */
    public Rng getXRng() {
        return xRng;
    }

    /**
     * Get the RNG for the y position.
     *
     * @return the RNG for the y position.
     */
    public Rng getYRng() {
        return yRng;
    }
}
