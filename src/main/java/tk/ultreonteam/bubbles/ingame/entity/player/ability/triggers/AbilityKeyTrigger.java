package tk.ultreonteam.bubbles.ingame.entity.player.ability.triggers;

import tk.ultreonteam.bubbles.core.input.KeyboardInput;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.AbilityTrigger;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.AbilityTriggerType;
import tk.ultreonteam.bubbles.event.input.CharTypedEvent;
import tk.ultreonteam.bubbles.event.type.KeyEventType;

public class AbilityKeyTrigger extends AbilityTrigger {
    private final KeyboardInput controller;
    private final int keyCode;
//    private final HashMap<Integer, Boolean> currentlyPressed;
    private final KeyEventType keyEventType;

    public AbilityKeyTrigger(CharTypedEvent evt, Entity entity) {
        super(AbilityTriggerType.KEY_TRIGGER, entity);
        this.controller = evt.getController();
        this.keyCode = evt.getKeyCode();
//        this.currentlyPressed = evt.getPressed();
        this.keyEventType = evt.getType();
    }

    public int getKeyCode() {
        return keyCode;
    }

//    public HashMap<Integer, Boolean> getCurrentlyPressed() {
//        return currentlyPressed;
//    }

    public KeyboardInput getController() {
        return controller;
    }

    public KeyEventType getKeyEventType() {
        return keyEventType;
    }
}
