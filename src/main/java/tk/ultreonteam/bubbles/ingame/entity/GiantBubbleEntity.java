package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.ingame.bubble.BubbleProperties;
import tk.ultreonteam.bubbles.ingame.bubble.BubbleSpawnContext;
import tk.ultreonteam.bubbles.common.random.BubbleRandomizer;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.init.Entities;

import java.awt.*;

/**
 * Bubble Entity.
 * One of the most important parts of the game.
 *
 * @see AbstractBubbleEntity
 */
@SuppressWarnings({"unused", "SameParameterValue"})
public class GiantBubbleEntity extends BubbleEntity {
    // Entity type.
    private static final EntityType<BubbleEntity> entityType = Entities.BUBBLE.get();

    public GiantBubbleEntity(Environment environment) {
        super(environment);
    }

    /**
     * Spawn Event Handler
     * On-spawn.
     *
     * @param pos         the position for spawn.
     * @param environment the environment to spawn in.
     */
    @Override
    public void onSpawn(Point pos, Environment environment) {

        // Get random properties
        BubbleRandomizer randomizer = this.environment.getBubbleRandomizer();
        BubbleSpawnContext ctx = BubbleSpawnContext.get();
        BubbleProperties properties = randomizer.getRandomProperties(environment.game().getGameBounds(), ctx.spawnIndex(), ctx.retry(), environment);

        // Bubble Type
        this.bubbleType = properties.getType();

        // Attributes
        this.attributes.setBase(Attribute.ATTACK, bubbleType.getAttack(this.environment, this.environment.getBubbleRandomizer().getAttackRng()));
        this.attributes.setBase(Attribute.DEFENSE, bubbleType.getDefense(this.environment, this.environment.getBubbleRandomizer().getDefenseRng()));
        this.attributes.setBase(Attribute.SCORE_MODIFIER, bubbleType.getScore(this.environment, this.environment.getBubbleRandomizer().getScoreMultiplierRng()));
        this.attributes.setBase(Attribute.SPEED, properties.getSpeed() / (Math.PI));
        this.attributes.setBase(Attribute.MAX_DAMAGE, properties.getDamageValue() * 4 + 80);

        // Dynamic values
        this.radius = properties.getRadius() * 4 + 80;
        this.baseRadius = properties.getRadius() * 4 + 80;
        this.damageValue = properties.getDamageValue() * 4 + 80;

        // Static values.
        this.bounceAmount = bubbleType.getBounceAmount();

        // Set velocity
        this.velX = -getBaseSpeed();

        create();
    }
}
