package tk.ultreonteam.bubbles.ingame.entity.player;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.Constants;
import tk.ultreonteam.bubbles.LoadedGame;
import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.common.PolygonBuilder;
import tk.ultreonteam.bubbles.ingame.entity.ActiveEntity;
import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.SpawnInformation;
import tk.ultreonteam.bubbles.ingame.entity.ammo.AmmoType;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.ingame.entity.damage.EntityDamageSource;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.AbilityContainer;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.input.KeyboardEvent;
import tk.ultreonteam.bubbles.event.type.KeyEventType;
import tk.ultreonteam.bubbles.init.AmmoTypes;
import tk.ultreonteam.bubbles.init.Entities;
import tk.ultreonteam.bubbles.ingame.item.collection.PlayerItemCollection;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.MessengerScreen;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.util.Util;
import tk.ultreonteam.bubbles.util.helpers.MathHelper;
import net.querz.nbt.tag.CompoundTag;
import org.apache.commons.lang3.SystemUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.*;
import java.util.Objects;

import static tk.ultreonteam.bubbles.BubbleBlaster.TPS;

public class Player extends ActiveEntity implements InputController {
    // These are the vertex coordinates:
    //
    //          |- Middle point
    //          |
    // 9876543210123456789
    // --------------------,
    // ....#*********#.....| -8
    // ....*.........*.....| -7
    // ....#***#.#***#.....| -6
    // ........*.*.........| -5
    // ..#*****#.#******#..| -4
    // .*..................| -3
    // #..................#| -2
    // *..................*| -1
    // *..................*| 0  // Middle point.
    // *..................*| 1
    // #..................#| 2
    // .*................*.| 3
    // ..#..............#..| 4

    private final Area shipShape;
    private final Area arrowShape;

    // Types
    private AmmoType currentAmmo = AmmoTypes.BASIC.get();

    // Motion (Arrow Keys).
    private boolean forward = false;
    private boolean backward = false;
    private boolean left = false;
    private boolean right = false;

    // Speed.
    private final float rotationSpeed = 16f;

    // Delta velocity.
    private double velDelta;

    // Motion (XInput).
    private float xInputX;
    private float xInputY;

    // Normal values/
    private double score = 0d;
    private int level = 1;

    // Modifiers.
    private long abilityEnergy;

    // Ability
    private final AbilityContainer abilityContainer = new AbilityContainer(this);

    private double accelerateX = 0.0d;
    private double accelerateY = 0.0d;
    private final PlayerItemCollection inventory = new PlayerItemCollection(this);

    /**
     * Player entity.
     * The player is controlled be the keyboard and is one of the important features of the game. (Almost any game).
     *
     * @see ActiveEntity
     */
    public Player(Environment environment) {
        super(Entities.PLAYER.get(), environment);

        this.addCollidable(Entities.BUBBLE.get());

        // Ship shape.
        Ellipse2D shipShape1 = new Ellipse2D.Double(-20, -20, 40, 40);
        this.shipShape = new Area(shipShape1);

        // Arrow shape.
//        Polygon arrowShape1 = new Polygon(new int[]{-5, -10, 15, -10}, new int[]{0, -10, 0, 10}, 4);
        Polygon arrowShape1 = new PolygonBuilder()
                .add(-5, 0)
                .add(-10, -10)
                .add(15, 0)
                .add(-10, 10)
                .build();
        this.arrowShape = new Area(arrowShape1);

        // Velocity.
        this.velX = 0;
        this.velY = 0;

        // Set attributes.
        this.attributes.setBase(Attribute.DEFENSE, 1f);
        this.attributes.setBase(Attribute.ATTACK, 0.75f);
        this.attributes.setBase(Attribute.MAX_DAMAGE, 100f);
        this.attributes.setBase(Attribute.SPEED, 16f);
        this.attributes.setBase(Attribute.SCORE_MODIFIER, 1f);

        // Health
        this.damageValue = 100f;

        for (EntityType<?> entityType : Registers.ENTITIES.values()) {
            addAttackable(entityType);
        }
    }

    /**
     * @param s the message.
     */
    public void sendMessage(String s) {
        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();
        if (loadedGame != null) {
            loadedGame.receiveMessage(s);
        }
    }

    /**
     * Prepare spawn
     * Called when the entity was spawned.
     *
     * @param spawnData the data to spawn with.
     */
    @Override
    public void prepareSpawn(SpawnInformation spawnData) {
        super.prepareSpawn(spawnData);
        @Nullable Screen currentScene = Objects.requireNonNull(Util.getSceneManager()).getCurrentScreen();
        if ((currentScene == null && BubbleBlaster.getInstance().isInGame()) ||
                currentScene instanceof MessengerScreen) {
            Rectangle2D gameBounds = environment.game().getGameBounds();
            this.x = (float) MathHelper.clamp(x, gameBounds.getMinX(), gameBounds.getMaxX());
            this.y = (float) MathHelper.clamp(y, gameBounds.getMinY(), gameBounds.getMaxY());
            create();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Shape     //
    ///////////////////
    @Override
    public Area getShape() {
        return transformArea(shipShape);
    }

    @NonNull
    private Area transformArea(Area shipShape) {
        Area area = new Area(shipShape);
        AffineTransform transform = new AffineTransform();
        transform.scale(getScale(), getScale());
        transform.translate(x, y);

        area.transform(transform);

        return area;
    }

    @NonNull
    private Area transformAreaRotated(Area shipShape) {
        Area area = new Area(shipShape);
        AffineTransform transform = new AffineTransform();
        transform.scale(getScale(), getScale());
        transform.translate(x, y);
        transform.rotate(Math.toRadians(getRotation()));

        area.transform(transform);

        return area;
    }

    /**
     * @return the x acceleration.
     */
    public double getAccelerateX() {
        return accelerateX;
    }

    /**
     * @return the y acceleration.
     */
    public double getAccelerateY() {
        return accelerateY;
    }

    /**
     * @param accelerateX the x acceleration to set.
     */
    public void setAccelerateX(double accelerateX) {
        this.accelerateX = accelerateX;
    }

    /**
     * @param accelerateY the y acceleration to set.
     */
    public void setAccelerateY(double accelerateY) {
        this.accelerateY = accelerateY;
    }

    /**
     * @return the shape of the ship.
     */
    public Area getShipArea() {
        return transformArea(shipShape);
    }

    /**
     * @return the arrow shape of the ship.
     */
    public Area getArrowArea() {
        return transformAreaRotated(arrowShape);
    }

    /**
     * @return the center position.
     */
    @SuppressWarnings("unused")
    public Point getCenter() {
        return new Point((int) getBounds().getCenterX(), (int) getBounds().getCenterY());
    }

    /**
     * Tick the player.
     *
     * @param environment the game-type where the entity is from.
     */
    @Override
    public void tick(Environment environment) {
        super.tick(environment);

//        logger.info("Player[df5d82fb] ON(TICK) ENTITY_ID={}", this.entityId);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Spawn and load checks //
        ///////////////////////////

        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();

//        logger.info("Player[ce153094] ON(TICK) LOADED_GAME={}", loadedGame);

        if (loadedGame == null) {
            return;
        }

//        logger.info("Player[9489d774] ON(TICK) IS_SPAWNED={}", isSpawned() ? 1 : 0);

//        logger.info("Player[9fe76104] FORWARD={} BACKWARD={} LEFT={} RIGHT={}", forward ? 1 : 0, backward ? 1 : 0, left ? 1 : 0, right ? 1 : 0);

        if (hasNotSpawnedYet()) return;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Player component ticking //
        //////////////////////////////

        this.abilityContainer.onEntityTick();
        this.inventory.tick();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Player motion. //
        ////////////////////

        this.accelerateX = accelerateX / ((double) TPS / 20d) / 1.1;
        this.accelerateY = accelerateY / ((double) TPS / 20d) / 1.1;

        double tempVelMotSpeed = 0.0f;
        float tempVelRotSpeed = 0.0f;

        // Check each direction, to create velocity
        if (this.forward) tempVelMotSpeed += this.attributes.getBase(Attribute.SPEED);
        if (this.backward) tempVelMotSpeed -= this.attributes.getBase(Attribute.SPEED);
        if (this.left) tempVelRotSpeed -= this.rotationSpeed;
        if (this.right) tempVelRotSpeed += this.rotationSpeed;
        if (this.xInputY != 0) tempVelMotSpeed = this.xInputY * this.attributes.getBase(Attribute.SPEED);
        if (this.xInputX != 0) tempVelRotSpeed = this.xInputX * this.rotationSpeed;

        // Update X, and Y.
        if (isMotionEnabled()) {
            this.rotate((tempVelRotSpeed) / TPS * 20);
        }

        // Calculate Velocity X and Y.
        double angelRadians = Math.toRadians(getRotation());
        double tempVelX = Math.cos(angelRadians) * tempVelMotSpeed;
        double tempVelY = Math.sin(angelRadians) * tempVelMotSpeed;

        if (isMotionEnabled()) {
            this.accelerateX += tempVelX / ((double) TPS);
            this.accelerateY += tempVelY / ((double) TPS);
        }

        // Update X, and Y.
        this.x += (this.accelerateX) + this.velX / ((double) TPS * 1);
        this.y += (this.accelerateY) + this.velY / ((double) TPS * 1);

        // Velocity.
        if (this.velX > 0) {
            if (this.velX + this.velDelta < 0) {
                this.velX = 0;
            } else {
                this.velX += this.velDelta;
            }
        } else if (this.velX < 0) {
            if (this.velX + this.velDelta > 0) {
                this.velX = 0;
            } else {
                this.velX -= this.velDelta;
            }
        }

        if (this.velY > 0) {
            if (this.velY + velDelta < 0) {
                this.velY = 0;
            } else {
                this.velY += this.velDelta;
            }
        } else if (this.velX < 0) {
            if (this.velY + this.velDelta > 0) {
                this.velY = 0;
            } else {
                this.velY -= this.velDelta;
            }
        }

        //////////////////////////
        // Collision detection. //
        //////////////////////////

        // Game border.
        Rectangle2D gameBounds = loadedGame.getGamemode().getGameBounds();
        this.x = (float) MathHelper.clamp(this.x, gameBounds.getMinX() + this.getBounds().getWidth(), gameBounds.getMaxX() - this.getBounds().getWidth());
        this.y = (float) MathHelper.clamp(this.y, gameBounds.getMinY() + this.getBounds().getHeight(), gameBounds.getMaxY() - this.getBounds().getHeight());

        if (score / Constants.LEVEL_THRESHOLD > level) {
            levelUp();
            environment.onLevelUp(this, level);
        }
    }

    @Override
    public void damage(double value, EntityDamageSource source) {
        double defense = attributes.getBase(Attribute.DEFENSE);
        if (defense <= 0.0d) {
            this.destroy();
            return;
        }

        // Deal damage to the player.
        this.damageValue -= value / defense;

        // Check health.
        this.checkShouldRemove();

        // Check if source has attack modifier.
        if (value > 0.0d) {

            // Check if window is not focused.
            if (!BubbleBlaster.getInstance().getGameWindow().isFocused()) {
                if (SystemUtils.IS_JAVA_9) {
                    // Let the taskbar icon flash. (Java 9+)
                    BubbleBlaster.getInstance().getGameWindow().requestUserAttention();
                }
            }
        }
    }

    @Override
    public void onCollision(Entity other, double frameTime) {
        if (other instanceof BubbleEntity bubble) {
            // Modifiers
            AttributeContainer attributeMap = bubble.getAttributes();
            double scoreMultiplier = attributeMap.getBase(Attribute.SCORE);
            double attack = attributeMap.getBase(Attribute.ATTACK);  // Maybe used.
            double defense = attributeMap.getBase(Attribute.DEFENSE);  // Maybe used.

            // Attributes
            double radius = bubble.getRadius();
            double speed = bubble.getSpeed();

            // Calculate score value.
            double visibleValue = radius * speed;
            double nonVisibleValue = attack * defense;
            double scoreValue = ((visibleValue * (nonVisibleValue + 1)) * scoreMultiplier * scoreMultiplier) * attributes.getBase(Attribute.SCORE_MODIFIER) * frameTime / Constants.BUBBLE_SCORE_REDUCTION;

            // Add score.
            addScore(scoreValue);
        }
    }

    @SubscribeEvent
    public void onKeyboardEvent(KeyboardEvent evt) {
        if (evt != null) {
            KeyEvent e = evt.getParentEvent();

            if (evt.getType() == KeyEventType.PRESS) {
                if (e.getKeyCode() == KeyInput.Map.KEY_UP) forward = true;
                if (e.getKeyCode() == KeyInput.Map.KEY_DOWN) backward = true;
                if (e.getKeyCode() == KeyInput.Map.KEY_LEFT) left = true;
                if (e.getKeyCode() == KeyInput.Map.KEY_RIGHT) right = true;
            }

            if (evt.getType() == KeyEventType.RELEASE) {
                if (e.getKeyCode() == KeyInput.Map.KEY_UP) forward = false;
                if (e.getKeyCode() == KeyInput.Map.KEY_DOWN) backward = false;
                if (e.getKeyCode() == KeyInput.Map.KEY_LEFT) left = false;
                if (e.getKeyCode() == KeyInput.Map.KEY_RIGHT) right = false;
            }
        }
    }

    @Override
    public void render(Renderer gg) {
        if (hasNotSpawnedYet()) return;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Fill the ship with the correct color. //
        ///////////////////////////////////////////
        gg.color(Color.red);
        gg.fill(getShipArea());

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Fill the arrow with the correct color. //
        ////////////////////////////////////////////
        gg.color(Color.white);
        gg.fill(getArrowArea());
    }

    /////////////////////////////////////////////////////////////////////////////////////
    //     Rotate Delta     //
    //////////////////////////
    @SuppressWarnings("unused")
    public void rotateDelta(int deltaRotation) {
        this.rotate(deltaRotation);
    }

    /**
     * Trigger a Reflection
     * Triggers a reflection, there are some problems with the velocity.
     * That's why it's currently in beta.
     *
     * @param velocityX the amount velocity for bounce.
     * @param velocityY the amount velocity for bounce.
     * @param delta     the delta change.
     */
    public void applyForce(double velocityX, double velocityY, double delta) {
//        this.velDelta = delta;
        accelerateX += velocityX;
        accelerateY += velocityY;
    }

    private void setAcceleration(double x, double y) {
        accelerateX = x;
        accelerateY = y;
    }

    /**
     * Trigger a Reflection
     * Triggers a reflection, there are some problems with the velocity.
     * That's why it's currently in beta.
     *
     * @param velocity the amount velocity for bounce.
     * @param delta    the delta change.
     */
    public void applyForce(Point2D velocity, double delta) {
        this.applyForce(velocity.getX(), velocity.getY(), delta);
    }

    /**
     * Trigger a Reflection
     * Triggers a reflection, there are some problems with the velocity.
     * That's why it's currently in beta.
     *
     * @param velocity the amount velocity for bounce.
     * @param delta    the delta change.
     */
    public void applyForce(float direction, float velocity, double delta) {
        double x = Math.cos(direction) * velocity;
        double y = Math.sin(direction) * velocity;
        this.applyForce(x, y, delta);
    }

    /**
     * Trigger a Reflection
     * Triggers a reflection, there are some problems with the velocity.
     * That's why it's currently in beta.
     *
     * @param source   the source entity that triggers the bounce.
     * @param velocity the amount velocity for bounce.
     * @param delta    the delta change.
     */
    public void bounceOff(Entity source, float velocity, double delta) {
        this.applyForce((float) Math.toDegrees(Math.atan2(source.getY() - y, source.getX() - x)), velocity, delta);
    }

    /**
     * Checks health
     * Checks health, if health is zero or less, it will trigger game-over.
     */
    @Override
    public void checkShouldRemove() {
        if (damageValue <= 0 && getEnvironment().isAlive()) {
            die();
        }
    }

    public void die() {
        if (getEnvironment().isAlive()) {
            getEnvironment().triggerGameOver();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Entity Data     //
    /////////////////////////
    @Override
    public @NonNull CompoundTag save() {
        @NonNull CompoundTag document = super.save();

        document.putDouble("score", score);
        document.putInt("level", level);
        return document;
    }

    @Override
    public void load(CompoundTag tag) {
        super.save();

        this.score = tag.getFloat("score");
        this.level = tag.getInt("level");
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Speed     //
    ///////////////////
    public float getRotationSpeed() {
        return rotationSpeed;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Values     //
    ////////////////////

    // Score
    public double getScore() {
        return score;
    }

    public void addScore(double value) {
        score += value;
    }

    public void subtractScore(double value) {
        score -= value;
    }

    public void setScore(double value) {
        score = value;
    }

    // Level
    public int getLevel() {
        return level;
    }

    public void levelUp() {
        level++;
    }

    public void levelDown() {
        setLevel(getLevel() - 1);
    }

    public void setLevel(int level) {
        this.level = Math.max(level, 1);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Modifiers     //
    ///////////////////////

    /**
     * Get the ability container of the entity.
     *
     * @return the requested {@link AbilityContainer}.
     * @see AbilityContainer
     */
    public AbilityContainer getAbilityContainer() {
        return abilityContainer;
    }

    /**
     * Get current ammo type.
     *
     * @return the current {@link AmmoType ammo type}.
     * @see AmmoType
     * @see #setCurrentAmmo(AmmoType)
     */
    public AmmoType getCurrentAmmo() {
        return currentAmmo;
    }

    /**
     * Set current ammo type.
     *
     * @param currentAmmo the ammo to set.
     * @see AmmoType
     * @see #getCurrentAmmo()
     */
    public void setCurrentAmmo(AmmoType currentAmmo) {
        this.currentAmmo = currentAmmo;
    }

    public void forward(boolean bool) {
        this.forward = bool;
    }

    public void backward(boolean bool) {
        this.backward = bool;
    }

    public void left(boolean bool) {
        this.left = bool;
    }

    public void right(boolean bool) {
        this.right = bool;
    }

    public void healToMax() {
        this.damageValue = attributes.getBase(Attribute.MAX_DAMAGE);
    }
}
