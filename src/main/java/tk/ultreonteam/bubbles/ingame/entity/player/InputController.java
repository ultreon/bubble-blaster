package tk.ultreonteam.bubbles.ingame.entity.player;


@SuppressWarnings({"RedundantSuppression", "unused"})
public interface InputController {
    void forward(boolean forward);

    void backward(boolean backward);

    void left(boolean left);

    void right(boolean right);
}
