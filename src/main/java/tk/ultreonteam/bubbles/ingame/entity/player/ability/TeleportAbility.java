package tk.ultreonteam.bubbles.ingame.entity.player.ability;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.triggers.AbilityKeyTrigger;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.triggers.types.AbilityKeyTriggerType;
import tk.ultreonteam.bubbles.init.Abilities;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.util.helpers.MathHelper;

import java.util.Objects;

public class TeleportAbility extends Ability<TeleportAbility> {
    public TeleportAbility() {
        super(Objects.requireNonNull(Abilities.TELEPORT_ABILITY).get());
    }

    @Override
    public int getTriggerKey() {
        return KeyInput.Map.KEY_SHIFT;
    }

    @Override
    public AbilityTriggerType getTriggerType() {
        return AbilityTriggerType.KEY_TRIGGER;
    }

    @Override
    public AbilityKeyTriggerType getKeyTriggerType() {
        return AbilityKeyTriggerType.HOLD;
    }

    @Override
    public void trigger(AbilityTrigger trigger) {
        Entity entity = trigger.getEntity();

        if (entity instanceof Player player) {
            long startTime = player.getTag().getNumber("TeleportAbilityStartTime").longValue();
            player.getTag().remove("TeleportAbilityStartTime");

            long deltaTime = System.currentTimeMillis() - startTime;
            deltaTime = MathHelper.clamp(deltaTime, 0, 2500);  // 0 to 2.5 seconds.

            double deltaMotion = Math.pow((double) deltaTime / 100, 2);

            // Calculate Velocity X and Y.
            double angelRadians = Math.toRadians(player.getRotation());
            double tempVelX = Math.cos(angelRadians) * deltaMotion;
            double tempVelY = Math.sin(angelRadians) * deltaMotion;

            player.teleport(player.getX() + tempVelX, player.getY() + tempVelY);
            subtractValue((int) deltaTime);
            setCooldown((int) (deltaTime / 3));
        }
    }

    @Override
    public void onKeyTrigger(AbilityKeyTrigger trigger) {
        Entity entity = trigger.getEntity();

        if (entity instanceof Player player) {
            player.getTag().putLong("TeleportAbilityStartTime", System.currentTimeMillis());
        }
    }

    @Override
    public void triggerEntity() {

    }

    @Override
    public boolean isTriggerable(Entity entity) {
        return entity instanceof Player;
    }

    @Override
    public boolean isRegeneratable() {
        return true;
    }
}
