package tk.ultreonteam.bubbles.ingame.entity;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.common.AttributeModifier;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.ingame.entity.damage.EntityDamageSource;
import tk.ultreonteam.bubbles.ingame.entity.modifier.Modifier;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.util.helpers.MathHelper;
import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Living Entity base class.
 * The class for all living entities, such as {@link Player the player}.
 *
 * @see Entity
 * @since 0.1.0
 * @author Qboi
 */
@SuppressWarnings("unused")
public abstract class ActiveEntity extends Entity {
    /**
     * The current damage value of the entity.
     */
    protected double damageValue;
    private final List<Modifier> modifiers = new ArrayList<>();

    /**
     * Constructor
     *
     * @param type the type of the entity
     * @param environment the environment the entity is in
     * @since 0.1.0
     * @author Qboi
     */
    public ActiveEntity(EntityType<?> type, Environment environment) {
        super(type, environment);
    }

    //********************//
    //     Properties     //
    //********************//
    /**
     * Get the current damage value of the entity.
     *
     * @return the current damage value.
     * @see #setDamageValue(double)
     * @see #getMaxDamageValue()
     * @since 0.1.0
     * @author Qboi
     */
    public double getDamageValue() {
        return damageValue;
    }

    /**
     * Set the damage value of the entity.
     *
     * @param damageValue the damage value to set.
     * @since 0.1.0
     * @author Qboi
     */
    public void setDamageValue(double damageValue) {
        this.damageValue = MathHelper.clamp(damageValue, 0.0, getMaxDamageValue());
    }

    /**
     * Get the maximum damage value of this entity.
     *
     * @return the maximum damage value of this entity.
     * @see #getDamageValue()
     * @since 0.1.0
     * @author Qboi
     */
    public double getMaxDamageValue() {
        return attributes.getValue(Attribute.MAX_DAMAGE);
    }

    /**
     * Set the maximum damage value of this entity.
     * Use {@link AttributeContainer#addModifier(AttributeModifier)} and {@link AttributeContainer#removeModifier(AttributeModifier)} instead.
     * To get the attribute container, use {@link #getAttributes()}.
     *
     * @param maxDamage the maximum damage value
     * @deprecated use {@link AttributeContainer#addModifier(AttributeModifier)} and {@link AttributeContainer#removeModifier(AttributeModifier)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void setMaxDamageValue(double maxDamage) {
        attributes.setBase(Attribute.MAX_DAMAGE, maxDamage);
    }

    /**
     * Get the current speed of the entity.
     *
     * @return the current speed.
     * @since 0.1.0
     * @author Qboi
     */
    public double getSpeed() {
        return attributes.getValue(Attribute.SPEED);
    }

    /**
     * Set the speed of this entity.
     * Use {@link AttributeContainer#addModifier(AttributeModifier)} and {@link AttributeContainer#removeModifier(AttributeModifier)} instead.
     * To get the attribute container, use {@link #getAttributes()}.
     *
     * @param speed the speed
     * @deprecated use {@link AttributeContainer#addModifier(AttributeModifier)} and {@link AttributeContainer#removeModifier(AttributeModifier)} instead.
     * @since 0.1.0
     * @author Qboi
     */
    public void setSpeed(double speed) {
        attributes.setBase(Attribute.SPEED, speed);
    }

    /**
     * Get the base speed of the entity.
     *
     * @return the base speed.
     * @since 0.1.0
     * @author Qboi
     */
    public double getBaseSpeed() {
        return attributes.getBase(Attribute.SPEED);
    }

    /**
     * Damage this entity.
     * WE ARE UNDER ATTACK!!!
     *
     * @param value  the attack value.
     * @param source the damage source.
     */
    @SuppressWarnings("unused")
    public void damage(double value, EntityDamageSource source) {
        if (attributes.getBase(Attribute.DEFENSE) == 0f) {
            // No defense, instant death.
            this.destroy();
        }

        // Calculate the damage.
        this.damageValue -= value / attributes.getBase(Attribute.DEFENSE);
        this.checkShouldRemove();
    }

    /**
     * Instant death of the entity.
     *
     * @since 0.1.0
     * @author Qboi
     * @see #damage(double, EntityDamageSource)
     * @see #isDead()
     */
    public void destroy() {
        this.damageValue = 0;
        checkShouldRemove();
    }

    /**
     * Check if the entity is dead.
     *
     * @return true if the entity is dead.
     * @since 0.1.0
     * @author Qboi
     * @see #destroy()
     */
    public boolean isDead() {
        return damageValue <= 0;
    }

    /**
     * Check if the entity is alive.
     *
     * @return true if the entity is alive.
     * @since 0.1.0
     * @author Qboi
     * @see #isDead()
     */
    public boolean isAlive() {
        return !isDead();
    }

    /**
     * Restore damage to the entity.
     *
     * @param value the value to restore.
     * @since 0.1.0
     * @author Qboi
     */
    public void restoreDamage(float value) {
        this.damageValue += value;
        this.damageValue = MathHelper.clamp(damageValue, 0f, attributes.getBase(Attribute.MAX_DAMAGE));
    }

    /**
     * Check if the entity should be removed.
     *
     * @since 0.1.0
     * @author Qboi
     */
    protected void checkShouldRemove() {
        if (this.damageValue <= 0) {
            this.delete();
        }
    }

    /**
     * Save the entity to an NBT tag.
     *
     * @return the tag.
     */
    @Override
    public @NonNull CompoundTag save() {
        @NonNull CompoundTag tag = super.save();

        tag.putDouble("damageValue", damageValue);

        return tag;
    }

    /**
     * Load the entity from an NBT tag.
     *
     * @param tag the tag.
     */
    @Override
    public void load(CompoundTag tag) {
        super.load(tag);

        damageValue = tag.getFloat("damageValue");
    }

    /**
     * Convert the entity to a simple string.
     *
     * @return the string.
     */
    public String toSimpleString() {
        return id() + "@(" + Math.round(getX()) + "," + Math.round(getY()) + ")";
    }
}
