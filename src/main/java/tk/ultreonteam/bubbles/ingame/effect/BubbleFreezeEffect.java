package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.commons.exceptions.InvalidValueException;

public class BubbleFreezeEffect extends StatusEffect {
    public BubbleFreezeEffect() throws InvalidValueException {
        super();
    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }

    @Override
    public void onStart(StatusEffectInstance statusEffectInstance, Entity entity) {
        if (entity instanceof Player) {
            if (!entity.getEnvironment().isGlobalBubbleFreeze()) {
                entity.getEnvironment().setGlobalBubbleFreeze(true);
            }
        }
    }

    @Override
    public void onStop(Entity entity) {
        if (entity instanceof Player) {
            if (entity.getEnvironment().isGlobalBubbleFreeze()) {
                entity.getEnvironment().setGlobalBubbleFreeze(false);
            }
        }
    }
}
