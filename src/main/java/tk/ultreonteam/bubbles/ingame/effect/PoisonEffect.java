package tk.ultreonteam.bubbles.ingame.effect;

import com.jhlabs.image.HSBAdjustFilter;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.damage.DamageSourceType;
import tk.ultreonteam.bubbles.ingame.entity.damage.EntityDamageSource;
import tk.ultreonteam.bubbles.event.FilterEvent;
import tk.ultreonteam.commons.exceptions.InvalidValueException;
import net.querz.nbt.tag.CompoundTag;

public class PoisonEffect extends StatusEffect {
    public PoisonEffect() throws InvalidValueException {
        super();
    }

    @Override
    public void onFilter(StatusEffectInstance statusEffectInstance, FilterEvent evt) {
        HSBAdjustFilter filter = new HSBAdjustFilter();
        filter.setHFactor((float) (System.currentTimeMillis() - statusEffectInstance.getStartTime()) / 3000 % 1);
        evt.addFilter(filter);
    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return System.currentTimeMillis() >= statusEffectInstance.getTag().getLong("nextDamage");
    }

    @Override
    public void execute(Entity entity, StatusEffectInstance statusEffectInstance) {
        entity.getEnvironment().attack(entity, (double) statusEffectInstance.getStrength() / 2, new EntityDamageSource(null, DamageSourceType.POISON));
        CompoundTag tag = statusEffectInstance.getTag();
        long nextDamage = tag.getLong("nextDamage");
        tag.putLong("nextDamage", nextDamage + 2000L);
    }

    @Override
    public void onStart(StatusEffectInstance statusEffectInstance, Entity entity) {
        CompoundTag tag = statusEffectInstance.getTag();
        tag.putLong("nextDamage", System.currentTimeMillis() + 2000);
        tag.putLong("startTime", System.currentTimeMillis());
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    public void onStop(Entity entity) {
        // Do nothing
    }

    @Override
    protected void updateStrength() {
        // Do nothing
    }
}
