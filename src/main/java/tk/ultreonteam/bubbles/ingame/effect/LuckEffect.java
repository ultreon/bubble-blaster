package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;

public class LuckEffect extends StatusEffect {
    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }

    @Override
    public AttributeContainer getAttributeModifiers() {
        AttributeContainer map = new AttributeContainer();
        map.setBase(Attribute.LUCK, 2.0f);
        return map;
    }
}
