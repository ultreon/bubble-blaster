package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.commons.exceptions.InvalidValueException;

public class MultiScoreEffect extends StatusEffect {
    public MultiScoreEffect() throws InvalidValueException {
        super();
    }

    @Override
    public AttributeContainer getAttributeModifiers() {
        AttributeContainer attributeMap = new AttributeContainer();
        attributeMap.setBase(Attribute.SCORE_MODIFIER, 1f);
        return attributeMap;
    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }
}
