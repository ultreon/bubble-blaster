package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.commons.exceptions.InvalidValueException;

@SuppressWarnings("GrazieInspection")
public class SpeedBoostEffect extends StatusEffect {
    public SpeedBoostEffect() throws InvalidValueException {
        super();
    }

    @Override
    public AttributeContainer getAttributeModifiers() {
        AttributeContainer attributeMap = new AttributeContainer();
        attributeMap.setBase(Attribute.SPEED, 1f);
        return attributeMap;
    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }

//    @Override
//    protected void updateStrength(EffectInstance effectInstance,  int old, int _new) {
//        Entity entity = effectInstance.getEntity();
//        if (entity instanceof PlayerEntity) {
//            PlayerEntity player = (PlayerEntity) entity;
//            player.setScoreModifier(player.getScoreModifier() - old + _new);
//        }
//    }
}
