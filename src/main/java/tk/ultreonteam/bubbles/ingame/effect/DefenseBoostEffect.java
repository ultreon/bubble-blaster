package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;

public class DefenseBoostEffect extends StatusEffect {
    public DefenseBoostEffect() {
        super();
    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }

    @Override
    public void execute(Entity entity, StatusEffectInstance statusEffectInstance) {

    }

    @Override
    public AttributeContainer getAttributeModifiers() {
        AttributeContainer map = new AttributeContainer();
        map.setBase(Attribute.DEFENSE, 1f);
        return map;
    }
}
