package tk.ultreonteam.bubbles.ingame.effect;

import tk.ultreonteam.bubbles.common.AttributeContainer;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.commons.exceptions.InvalidValueException;

public class AttackBoostEffect extends StatusEffect {
    public AttackBoostEffect() throws InvalidValueException {
        super();
    }

    @Override
    public void execute(Entity entity, StatusEffectInstance statusEffectInstance) {

    }

    @Override
    protected void updateStrength() {

    }

    @Override
    protected boolean canExecute(Entity entity, StatusEffectInstance statusEffectInstance) {
        return false;
    }

    @Override
    public AttributeContainer getAttributeModifiers() {
        AttributeContainer attributes = new AttributeContainer();
        attributes.setBase(Attribute.ATTACK, 1f);

        return attributes;
    }
}
