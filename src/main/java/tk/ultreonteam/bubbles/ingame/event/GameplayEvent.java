package tk.ultreonteam.bubbles.ingame.event;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.LoadedGame;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.Screen;
import tk.ultreonteam.bubbles.util.Util;
import tk.ultreonteam.commons.time.DateTime;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.awt.*;

@SuppressWarnings({"unused"})
public abstract class GameplayEvent extends Registrable {
    private Color backgroundColor;

    public GameplayEvent() {
        if (BubbleBlaster.getEventBus() == null) {
            throw new NullPointerException();
        }
        BubbleBlaster.getEventBus().subscribe(this);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isActive(DateTime dateTime) {
        @Nullable Screen currentScene = Util.getSceneManager().getCurrentScreen();

        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();
        if (loadedGame == null) {
            return false;
        }

        return loadedGame.getEnvironment().isGameStateActive(this);
    }

    public final Color getBackgroundColor() {
        return backgroundColor;
    }

    public void renderBackground(BubbleBlaster game, Renderer gg) {
        if (backgroundColor == null) return;
        if (!isActive(DateTime.current())) return;

        gg.color(getBackgroundColor());
        gg.fill(BubbleBlaster.getInstance().getGameBounds());
    }

    public final void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }


    @Override
    public String toString() {
        return "GameEvent[" + id() + "]";
    }
}
