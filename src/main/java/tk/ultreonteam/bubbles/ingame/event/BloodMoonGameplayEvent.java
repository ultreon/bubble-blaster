package tk.ultreonteam.bubbles.ingame.event;

import com.jhlabs.image.NoiseFilter;
import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.LoadedGame;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.event.FilterEvent;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.TickEvent;
import tk.ultreonteam.commons.time.Date;
import tk.ultreonteam.commons.time.DateTime;
import tk.ultreonteam.commons.time.Time;
import tk.ultreonteam.commons.util.ColorUtils;

import java.time.DayOfWeek;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public class BloodMoonGameplayEvent extends GameplayEvent {
    private final Date date = new Date(31, 10, 0);
    private final Time timeLo = new Time(3, 0, 0);
    private final Time timeHi = new Time(3, 59, 59);

    private boolean wasActive = false;
    private boolean wasPlayerActive = false;

    private final Map<Player, Double> playerDefenses = new ConcurrentHashMap<>();
    private boolean activating;
    private boolean deactivating;
    private long stopTime;

    public BloodMoonGameplayEvent() {
        super();

        setBackgroundColor(ColorUtils.unpackHex("#af0000"));
    }

    @SubscribeEvent
    public void onUpdate(TickEvent evt) {
        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();

        if (loadedGame == null) {
            return;
        }

        Environment environment = loadedGame.getEnvironment();

        if (stopTime < System.currentTimeMillis()) {
            environment.stopBloodMoon();
        }

        if (activating) {
            activating = false;
            deactivating = false;

            stopTime = System.currentTimeMillis() + 60000;

            // Game effects.
            if (!wasActive) BubbleBlaster.getLogger().info("Blood Moon activated!");

            environment.triggerBloodMoon();
            environment.setStateDifficultyModifier(this, 16f);
            wasActive = true;

            // Player effects.
            if (!wasPlayerActive && loadedGame.getGamemode().getPlayer() != null) {
                BubbleBlaster.getLogger().info("Blood Moon for player activated!");
                // Todo: implement this.
//                playerDefenses.put(GameScene.getGameType().getPlayer(), GameScene.getGameType().getPlayer().getDefenseModifier());
                wasPlayerActive = true;
            }
        } else if (deactivating) {
            deactivating = false;
            // Game effects.
            if (wasActive) {
                BubbleBlaster.getLogger().info("Blood Moon deactivated!");
                environment.removeStateDifficultyModifier(this);
                wasActive = false;
            }
        }
    }

    @SubscribeEvent
    public void onFilter(FilterEvent evt) {
        if (!isActive(DateTime.current())) return;

        NoiseFilter filter = new NoiseFilter();

        filter.setMonochrome(true);
        filter.setDensity(0.25f);
        filter.setAmount(60);
        filter.setDistribution(1);

        evt.addFilter(filter);
    }

    @Override
    public final boolean isActive(DateTime dateTime) {
        super.isActive(dateTime);

        LoadedGame loadedGame = BubbleBlaster.getInstance().getLoadedGame();
        if (loadedGame == null) {
            return false;
        }

        return loadedGame.getEnvironment().isBloodMoonActive();
    }

    public final boolean wouldActive(DateTime dateTime) {
        boolean flag1 = dateTime.getTime().isBetween(timeLo, timeHi);  // Devil's hour.
        boolean flag2 = dateTime.getDate().equalsIgnoreYear(date);  // Halloween.

        boolean flag3 = dateTime.getDate().getDayOfWeek() == DayOfWeek.FRIDAY;  // Friday
        boolean flag4 = dateTime.getDate().getDay() == 13;  // 13th

        return (flag1 && flag2) || (flag3 && flag4);  // Every October 31st in devil's hour. Or Friday 13th.
    }

    public void deactivate() {
        this.deactivating = true;
    }

    public void activate() {
        this.activating = true;
    }

    public Map<Player, Double> getPlayerDefenses() {
        return playerDefenses;
    }
}
