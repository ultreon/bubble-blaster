package tk.ultreonteam.bubbles.ingame.bubble;

import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.commons.util.ColorUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;

public class AccelerateBubble extends AbstractBubble {
    public AccelerateBubble() {
        setColors(ColorUtils.parseColorString("#00003f,#00007f,#0000af,#0000ff"));

        setPriority(2440000);
        setRadius(new IntRange(25, 54));
        setSpeed(new DoubleRange(6.0, 28.0));
        setDefense(1.0f);
        setAttack(0.001f);
        setScore(1);
        setHardness(0.7d);
    }

    @Override
    public void onCollision(BubbleEntity source, Entity target) {
        if (target instanceof Player player) {
            // Calculate Velocity X and Y.
            double accelerateX = 0;
            double accelerateY = 0;
            if (player.isMotionEnabled()) {
                accelerateX += Math.cos(Math.toRadians(player.getRotation())) * 1.5/* * 0.625d*/;
                accelerateY += Math.sin(Math.toRadians(player.getRotation())) * 1.5/* * 0.625d*/;
            }

            // Set velocity X and Y.
            player.applyForce(accelerateX, accelerateY, 1.0f);
        }
    }
}
