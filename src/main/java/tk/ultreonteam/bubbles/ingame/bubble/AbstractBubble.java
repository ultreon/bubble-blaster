package tk.ultreonteam.bubbles.ingame.bubble;

import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.common.Registrable;
import tk.ultreonteam.bubbles.common.random.Rng;
import tk.ultreonteam.bubbles.common.text.translation.Translatable;
import tk.ultreonteam.bubbles.ingame.effect.StatusEffectInstance;
import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.commons.exceptions.InvalidValueException;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * @see EntityType
 */
@SuppressWarnings({"unused", "SameParameterValue", "SameReturnValue"})
public abstract class AbstractBubble extends Registrable implements Serializable, Translatable {
    public Color[] colors;
    private double priority;

    private IntRange radius;
    private DoubleRange speed;
    private boolean doesBounce = false;
    private float bounceAmount;
    private final BubbleEffectCallback effect = (source, target) -> null;

    private float score;
    private float defense;
    private float attack;
    private double hardness;
    private int rarity;

    public AbstractBubble() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractBubble that = (AbstractBubble) o;
        return that.id().equals(id());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Effect     //
    ////////////////////
    public StatusEffectInstance getEffect(BubbleEntity source, Entity target) {
        return effect.get(source, target);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Attributes     //
    ////////////////////////
    public int getMinRadius() {
        return radius.getMinimumInteger();
    }

    public int getMaxRadius() {
        return radius.getMaximumInteger();
    }

    public double getMinSpeed() {
        return speed.getMinimumDouble();
    }

    public double getMaxSpeed() {
        return speed.getMaximumDouble();
    }

    protected final void setMinRadius(int radius) {
        this.radius = new IntRange(radius, this.radius.getMaximumInteger());
    }

    protected final void setMaxRadius(int radius) {
        this.radius = new IntRange(this.radius.getMinimumInteger(), radius);
    }

    protected final void setMinSpeed(double speed) {
        this.speed = new DoubleRange(speed, this.radius.getMaximumDouble());
    }

    protected final void setMaxSpeed(double speed) {
        this.speed = new DoubleRange(this.speed.getMinimumInteger(), speed);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Modifiers     //
    ///////////////////////
    public boolean isDefenseRandom() {
        return false;
    }

    public boolean isAttackRandom() {
        return false;
    }

    public boolean isScoreRandom() {
        return false;
    }

    public float getDefense(Environment environment, Rng rng) {
        return getDefense();
    }

    public float getAttack(Environment environment, Rng rng) {
        return getAttack();
    }

    public float getScore(Environment environment, Rng rng) {
        return getScore();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Other     //
    ///////////////////
    public double getModifiedPriority(double localDifficulty) {
        return getPriority();
    }

    public boolean canSpawn(@NonNull Environment environment) {
        return true;
    }

    public ArrayList<Object> getFilters(BubbleEntity bubbleEntity) {
        return new ArrayList<>();
    }

    @FunctionalInterface
    public interface BubbleEffectCallback {
        StatusEffectInstance get(BubbleEntity source, Entity target);
    }

    public double getPriority() {
        return priority;
    }

    public IntRange getRadius() {
        return radius;
    }

    public DoubleRange getSpeed() {
        return speed;
    }

    public boolean getDoesBounce() {
        return doesBounce;
    }

    public float getBounceAmount() {
        return bounceAmount;
    }

    public float getScore() {
        return score;
    }

    public float getDefense() {
        return defense;
    }

    public float getAttack() {
        return attack;
    }

    public double getHardness() {
        return hardness;
    }

    protected void setPriority(double priority) {
        this.priority = priority;
    }

    protected void setRadius(IntRange radius) {
        this.radius = radius;
    }

    protected void setSpeed(DoubleRange speed) {
        this.speed = speed;
    }

    protected void setBounceAmount(float bounceAmount) {
        this.bounceAmount = bounceAmount;
        this.doesBounce = true;
    }

    protected void setScore(float score) {
        this.score = score;
    }

    protected void setDefense(float defense) {
        this.defense = defense;
    }

    protected void setAttack(float attack) {
        this.attack = attack;
    }

    protected void setHardness(double hardness) {
        this.hardness = hardness;
    }

    protected void setRarity(int rarity) {
        this.rarity = rarity;
    }

    protected int getRarity() {
        return rarity;
    }

    @Override
    public String toString() {
        return "Bubble{" +
                "colors=" + Arrays.toString(colors) +
                ", priority=" + priority +
                ", radius=" + radius +
                ", speed=" + speed +
                ", bounceAmount=" + bounceAmount +
                ", effect=" + effect +
                ", score=" + score +
                ", defense=" + defense +
                ", attack=" + attack +
                ", hardness=" + hardness +
                ", rarity=" + rarity +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long _priority = null;
        private float score = 1f;
        private float defense = 0.1f;
        private float attack = 0f;
        private IntRange radius = new IntRange(21, 80);
        private DoubleRange speed = new DoubleRange(1d, 2.5d);
        private int _rarity;
        private float bounceAmount = 0f;
        private double hardness = 1d;
        private Color[] colors;
        private BubbleEffectCallback _bubbleEffect = (source, target) -> null;
        private DoubleRange _speed;
        private boolean doesBounce = false;

        private Builder() {
        }

        public AbstractBubble build() {
            AbstractBubble bubbleType = new AbstractBubble() {
                @Override
                public StatusEffectInstance getEffect(BubbleEntity source, Entity target) {
                    return _bubbleEffect.get(source, target);
                }
            };
            if (_priority == null) {
                throw new IllegalArgumentException("Priority must be specified");
            }
            if (colors == null) {
                throw new IllegalArgumentException("Colors must be specified");
            }

            bubbleType.setPriority(_priority);
            bubbleType.setRarity(_rarity);
            bubbleType.setScore(score);
            bubbleType.setAttack(attack);
            bubbleType.setDefense(defense);
            bubbleType.setRadius(radius);
            bubbleType.setSpeed(speed);
            bubbleType.setHardness(hardness);
            if (doesBounce) {
                bubbleType.setBounceAmount(bounceAmount);
            }
            bubbleType.colors = colors;
            return bubbleType;
        }

        @Deprecated(since = "0.0.3470-indev5", forRemoval = true)
        public Builder name(String name) {
            return this;
        }

        // Longs
        public Builder priority(long priority) {
            this._priority = priority;
            return this;
        }

        // Ints
        public Builder score(float score) {
            this.score = score;
            return this;
        }

        public Builder rarity(int rarity) {
            this._rarity = rarity;
            return this;
        }

        // Doubles
        public Builder attack(float attack) {
            this.attack = attack;
            return this;
        }

        public Builder defense(float defense) {
            this.defense = defense;
            return this;
        }

        public Builder hardness(double hardness) {
            this.hardness = hardness;
            return this;
        }

        // Floats
        public Builder bounceAmount(float bounceAmount) {
            this.bounceAmount = bounceAmount;
            this.doesBounce = true;
            return this;
        }

        // Ranges
        public Builder radius(int _min, int _max) {
            this.radius = new IntRange(_min, _max);
            return this;
        }

        public Builder radius(IntRange range) {
            this.radius = range;
            return this;
        }

        public Builder speed(double _min, double _max) {
            this.speed = new DoubleRange(_min, _max);
            return this;
        }

        public Builder speed(DoubleRange range) {
            this.speed = range;
            return this;
        }

        // Arrays (Dynamic)
        public Builder colors(Color... _colors) {
            this.colors = _colors;
            return this;
        }

        // Callbacks
        public Builder effect(BubbleEffectCallback _bubbleEffect) {
            this._bubbleEffect = _bubbleEffect;
            return this;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Collision     //
    ///////////////////////
    public void onCollision(BubbleEntity source, Entity target) {
        StatusEffectInstance statusEffectInstance = getEffect(source, target);
        if (statusEffectInstance == null) {
            return;
        }

        if (source.isEffectApplied()) return;

        if (target instanceof Player player) {
            try {
                source.setEffectApplied(true);
                player.addEffect(statusEffectInstance);
            } catch (InvalidValueException valueError) {
                valueError.printStackTrace();
            }
        }
    }

    @Override
    public String getTranslationPath() {
        Identifier registryName = id();
        return registryName.location() + "/Bubble/Name/" + registryName.path();
    }

    public String getDescriptionTranslationPath() {
        Identifier registryName = id();
        return registryName.location() + "/Bubble/Description/" + registryName.path();
    }

    public Color[] getColors() {
        return colors;
    }

    protected void setColors(Color[] colors) {
        this.colors = colors;
    }
}
