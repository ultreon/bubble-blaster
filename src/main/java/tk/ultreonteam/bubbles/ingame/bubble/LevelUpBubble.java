package tk.ultreonteam.bubbles.ingame.bubble;

import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.commons.util.ColorUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import org.checkerframework.checker.nullness.qual.NonNull;

/**
 * Levelup Bubble
 *
 * @author Qboi
 * @since 1.0.0
 */
public class LevelUpBubble extends AbstractBubble {
    public LevelUpBubble() {
        // Color & key.
        colors = ColorUtils.parseColorString("#ffff00,#ffffff,#ff9f00");

        // Set initial data values.
        setPriority(131_072L);
        setRadius(new IntRange(21, 60));
        setSpeed(new DoubleRange(6.4, 19.2));
        setDefense(Float.NaN);
        setAttack(0.0f);
        setScore(1);
        setHardness(1.0d);
    }

    @Override
    public void onCollision(BubbleEntity source, Entity target) {
        super.onCollision(source, target);

        // Check target is a player/
        if (target instanceof Player player) {
            // Remove Bubble.
            source.delete();

            // Player level-up.
            player.levelUp();
        }
    }

    @Override
    public boolean canSpawn(@NonNull Environment environment) {
        // If player is not spawned yet, the player cannot have any change. So return false.
        if (environment.getPlayer() == null) return false;

        // Calculate the maximum level for the player's score.
        int maxLevelUp = (int) Math.round(environment.getPlayer().getScore()) / 50_000 + 1;

        // Check for existing level-up bubble entities.
        if (environment.getEntities().stream().
                filter((entity) -> entity instanceof BubbleEntity) // Filter for bubble entities.
                .map((entity) -> (BubbleEntity) entity) // Cast to bubble entities.
                .anyMatch(bubbleEntity -> bubbleEntity.getBubbleType() == this)) { // Check for level-up bubbles of the type of this class.
            return false; // Then it can't spawn.
        }

        // Return flag for ‘if the maximum level for score is greater than the player's current level’
        return maxLevelUp > environment.getPlayer().getLevel();
    }
}
