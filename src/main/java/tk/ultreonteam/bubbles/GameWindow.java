package tk.ultreonteam.bubbles;

import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.core.CursorManager;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.window.WindowClosingEvent;
import tk.ultreonteam.bubbles.event.window.WindowResizedEvent;
import tk.ultreonteam.bubbles.event.window.WindowShownEvent;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.input.MouseInput;
import tk.ultreonteam.bubbles.vector.Vec2i;
import tk.ultreonteam.bubbles.vector.size.IntSize;
import tk.ultreonteam.commons.exceptions.OneTimeUseException;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.common.value.qual.IntRange;
import org.jdesktop.swingx.JXFrame;
import org.jetbrains.annotations.Contract;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Window class for the game's window.
 *
 * @author Qboi
 */
@SuppressWarnings({"unused", "InnerClassMayBeStatic"})
public class GameWindow {
    // GUI Elements.
    @NonNull
    final JXFrame frame;
    @NonNull
    final Canvas canvas;

    // AWT Toolkit.
    @NonNull
    private final Toolkit toolkit = Toolkit.getDefaultToolkit();

    // Graphics thingies.
    @NonNull
    final ImageObserver observer;
    @NonNull
    private final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    @NonNull
    private final GraphicsDevice gd;
    @NonNull
    final Properties properties;
    private boolean initialized = false;

    @IntRange(from = 0)
    private int fps;
    private CursorManager cursorManager;
    private Thread mainThread;
    private boolean borderlessFullscreen;

    /**
     * Window constructor.
     *
     * @param properties window properties, not fully implemented yet. LOL
     */
    @SuppressWarnings("FunctionalExpressionCanBeFolded")
    GameWindow(@NonNull Properties properties) {
        this.properties = properties;
        this.frame = new JXFrame(properties.title);
        this.frame.setPreferredSize(new Dimension(properties.width, properties.height));
        this.frame.setSize(properties.width, properties.height);
        this.frame.setResizable(false);
        this.frame.enableInputMethods(true);
        this.frame.setFocusTraversalKeysEnabled(false);
        this.gd = this.frame.getGraphicsConfiguration().getDevice();

        this.canvas = new Canvas(this.frame.getGraphicsConfiguration());
        this.canvas.enableInputMethods(true);
        this.canvas.setFocusTraversalKeysEnabled(false);

        this.observer = canvas::imageUpdate; // Didn't use canvas directly because of security reasons.

        GameWindow.this.canvas.setSize(properties.width, properties.height);

        this.frame.add(this.canvas);

        this.frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                GameWindow.this.canvas.setSize(e.getComponent().getSize());
            }
        });

        this.frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        this.frame.addWindowListener(new WindowHandler());
        this.frame.addComponentListener(new WindowComponentHandler());
        this.frame.addFocusListener(new WindowFocusHandler());
        this.canvas.addFocusListener(new CanvasFocusHandler());

        canvas.requestFocus();
    }

    public synchronized void init() {
        if (initialized) {
            throw new OneTimeUseException("The game window is already initialized.");
        }

        this.canvas.setVisible(true);

        setBorderlessFullscreen(true);

        this.frame.setVisible(true);

        KeyInput.init();
        MouseInput.init();

        KeyInput.listen(this.frame);
        MouseInput.listen(this.frame);

        KeyInput.listen(this.canvas);
        MouseInput.listen(this.canvas);

        this.initialized = true;

        BubbleBlaster.getLogger().info("Initialized game window");

        game().windowLoaded();
    }

    void close() {
        this.frame.dispose(); // Window#dispose() closes the awt-based window.
    }

    public Cursor registerCursor(int hotSpotX, int hotSpotY, Identifier identifier) {
        Identifier textureEntry = new Identifier("Textures/Cursors/" + identifier.path(), identifier.location());
        Image image;
        try (InputStream assetAsStream = game().getResourceManager().getAssetAsStream(textureEntry)) {
            image = ImageIO.read(assetAsStream);
        } catch (IOException e) {
            throw new IOError(e);
        }

        return toolkit.createCustomCursor(image, new Point(hotSpotX, hotSpotY), identifier.toString());
    }

    private BubbleBlaster game() {
        return BubbleBlaster.getInstance();
    }

    /**
     * Set the borderless fullscreen mode.
     *
     * @param borderlessFullscreen whether to use borderless fullscreen.
     */
    public void setBorderlessFullscreen(boolean borderlessFullscreen) {
        if (borderlessFullscreen == this.borderlessFullscreen) return;

        if (borderlessFullscreen) {
            this.frame.setVisible(false);
            this.frame.setUndecorated(true);
            this.frame.setResizable(false);
            this.frame.setVisible(true);
            this.frame.setLocation(0, 0);
            this.frame.setSize(this.properties.width, this.properties.height);
            this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else {
            this.frame.setVisible(false);
            this.frame.setUndecorated(false);
            this.frame.setResizable(true);
            this.frame.setVisible(true);
            this.frame.setLocation(0, 0);
            this.frame.setSize(this.properties.width, this.properties.height);
            this.frame.setExtendedState(JFrame.NORMAL);
        }
        this.borderlessFullscreen = borderlessFullscreen;
    }

    /**
     * Check if the window is in borderless fullscreen mode.
     *
     * @return whether the window is in borderless fullscreen mode.
     */
    public boolean isBorderlessFullscreen() {
        return borderlessFullscreen;
    }

    /**
     * Toggles fullscreen mode on/off.
     *
     * @return the new fullscreen state
     * @since 0.1.0
     * @author Qboi
     * @deprecated Use {@link #setBorderlessFullscreen(boolean)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public boolean toggleFullscreen() {
        setFullscreen(!isFullscreen());
        return isFullscreen();
    }

    /**
     * Set the fullscreen state of the window.
     *
     * @param fullscreen true to set the window to fullscreen, false to set it to windowed.
     * @throws IllegalStateException if the window is not initialized.
     * @since 0.1.0
     * @author Qboi
     * @see #isFullscreen()
     * @deprecated Use {@link #setBorderlessFullscreen(boolean)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void setFullscreen(boolean fullscreen) {
        if (isFullscreen() && !fullscreen) { // If currently not fullscreen and disabling fullscreen.
            gd.setFullScreenWindow(null); // Set fullscreen to false.
        } else if (!isFullscreen() && fullscreen) { // If currently in fullscreen and enabling fullscreen.
            gd.setFullScreenWindow(this.frame); // Set fullscreen to true.
        }
    }

    /**
     * Check if the window is in fullscreen mode.
     *
     * @return whether the window is in fullscreen mode.
     * @since 0.1.0
     * @author Qboi
     * @deprecated Use {@link #isBorderlessFullscreen()} instead.
     */
    @Deprecated
    public boolean isFullscreen() {
        return gd.getFullScreenWindow() == this.frame;
    }

    /**
     * Check if the window is initialized.
     *
     * @return whether the window is initialized.
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * Get the window's bounds.
     *
     * @return the window's bounds.
     */
    public Rectangle getBounds() {
        return this.frame.getBounds();
    }

    /**
     * Get the window's size.
     *
     * @return the window's size.
     */
    public IntSize getSize() {
        return new IntSize(this.frame.getWidth(), this.frame.getHeight());
    }

    /**
     * Get the window's position.
     *
     * @return the window's position.
     */
    public Vec2i getPos() {
        return new Vec2i(this.frame.getX(), this.frame.getY());
    }

    /**
     * Get the window's width.
     *
     * @return the window's width.
     */
    public int getWidth() {
        return this.frame.getWidth();
    }

    /**
     * Get the window's height.
     *
     * @return the window's height.
     */
    public int getHeight() {
        return this.frame.getHeight();
    }

    /**
     * Get the window's x position.
     *
     * @return the window's x position.
     */
    public int getX() {
        return this.frame.getX();
    }

    /**
     * Get the window's y position.
     *
     * @return the window's y position.
     */
    public int getY() {
        return this.frame.getY();
    }

    /**
     * Get the current mouse position.
     *
     * @return the mouse position.
     */
    @Nullable
    public Vec2i getMousePosition() {
        @Nullable Point mousePosition = this.canvas.getMousePosition();

        if (mousePosition == null) return null;
        return new Vec2i(mousePosition);
    }

    public void setCursor(Cursor defaultCursor) {
        if (cursorManager == null) return;
        cursorManager.setCursor(this.frame, defaultCursor);
    }

    public void requestFocus() {

    }

    /**
     * Check if the window is focused.
     *
     * @return whether the window is focused.
     */
    public boolean isFocused() {
        return this.frame.isFocused();
    }

    /**
     * Taskbar feature, flashes the taskbar icon on Windows.
     * Other operating systems are unknown for this behavior.
     */
    public void requestUserAttention() {
        if (Taskbar.isTaskbarSupported()) {
            Taskbar taskbar = Taskbar.getTaskbar();
            if (taskbar.isSupported(Taskbar.Feature.USER_ATTENTION_WINDOW)) {
                taskbar.requestWindowUserAttention(this.frame);
            }
        }
    }

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    public static class Properties {
        private final int width;
        private final int height;
        private final String title;
        private boolean fullscreen;

        public Properties(@NonNull String title, @IntRange(from = 0) int width, @IntRange(from = 0) int height) {
            if (width < 0) throw new IllegalArgumentException("Width is negative");
            if (height < 0) throw new IllegalArgumentException("Height is negative");
            Objects.requireNonNull(title, "Title is set to null");

            this.width = width;
            this.height = height;
            this.title = title;
        }

        @Contract("->this")
        public Properties fullscreen() {
            this.fullscreen = true;
            return this;
        }
    }

    class WindowHandler extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            boolean cancelClose = GameEvents.get().publish(new WindowClosingEvent(GameWindow.this));
            if (!cancelClose) {
                game().close();
            }
        }
    }

    class WindowFocusHandler extends FocusAdapter {
        @Override
        public void focusGained(FocusEvent e) {
            GameWindow.this.canvas.requestFocus();
        }
    }

    class CanvasFocusHandler extends FocusAdapter {
        @Override
        public void focusLost(FocusEvent e) {
            GameWindow.this.canvas.requestFocus();
        }
    }

    private class WindowComponentHandler extends ComponentAdapter {
        @Override
        public void componentResized(ComponentEvent e) {
            GameEvents.get().publish(new WindowResizedEvent(GameWindow.this, e.getComponent().getWidth(), e.getComponent().getHeight()));
        }

        @Override
        public void componentShown(ComponentEvent e) {
            GameEvents.get().publish(new WindowShownEvent(GameWindow.this));
        }
    }
}
