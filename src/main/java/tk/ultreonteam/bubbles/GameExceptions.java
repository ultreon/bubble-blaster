package tk.ultreonteam.bubbles;

import tk.ultreonteam.commons.crash.CrashLog;

/**
 * @author Qboi
 * @since 0.1.0
 */
class GameExceptions implements Thread.UncaughtExceptionHandler {
    /**
     * @since 0.1.0
     */
    private final BubbleBlaster game;

    /**
     * @param game the game instance.
     * @since 0.1.0
     * @author Qboi
     */
    GameExceptions(BubbleBlaster game) {
        this.game = game;
    }

    /**
     * @param thread the thread
     * @param throwable the exception
     * @since 0.1.0
     * @author Qboi
     */
    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        CrashLog crashLog = new CrashLog("Uncaught exception", throwable);
        game.crash(crashLog.createCrash());
    }
}
