package tk.ultreonteam.bubbles.resources;

import com.google.gson.JsonElement;
import tk.ultreonteam.bubbles.Constants;
import tk.ultreonteam.commons.function.ThrowingSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonResource extends Resource {
    private JsonElement json;

    public JsonResource(ThrowingSupplier<InputStream, IOException> opener) {
        super(opener);
    }

    @Override
    public void load() {
        try (InputStream inputStream = opener.get()) {
            this.json = Constants.GSON.fromJson(new InputStreamReader(inputStream), JsonElement.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonElement getJson() {
        return json;
    }
}
