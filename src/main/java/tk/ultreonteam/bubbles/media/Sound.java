package tk.ultreonteam.bubbles.media;

import javafx.scene.media.AudioClip;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class Sound {
    private final AudioClip clip;
    private final String name;
    private boolean stopped = false;

    public Sound(File file) {
        this(file, "");
    }

    public Sound(File file, String name) {
        clip = new AudioClip(file.toURI().toString());
        this.name = name;
    }

    public Sound(URI uri) {
        this(uri, "");
    }

    public Sound(URI uri, String name) {
        clip = new AudioClip(uri.toString());
        this.name = name;
    }

    public Sound(URL url) throws URISyntaxException {
        this(url, "");
    }

    public Sound(URL url, String name) throws URISyntaxException {
        clip = new AudioClip(url.toURI().toString());
        this.name = name;
    }

    public AudioClip getClip() {
        return clip;
    }

    public void play() {
        clip.play();
        stopped = false;
    }

    public void stop() {
        clip.stop();
        stopped = true;
    }

    public double getBalance() {
        return clip.getBalance();
    }

    public double getRate() {
        return clip.getRate();
    }

    public void setVolume(double v) {
        clip.setVolume(v);
    }

    public void setMute(double v) {
        clip.setBalance(v);
    }

    public double getVolume() {
        return clip.getVolume();
    }

    public String getSource() {
        return clip.getSource();
    }

    public boolean isStopped() {
        return stopped;
    }

    public boolean isPlaying() {
        return !stopped;
    }

    public String getName() {
        return name;
    }
}
