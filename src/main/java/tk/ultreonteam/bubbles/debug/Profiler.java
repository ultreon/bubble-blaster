package tk.ultreonteam.bubbles.debug;

import it.unimi.dsi.fastutil.objects.Reference2LongArrayMap;
import it.unimi.dsi.fastutil.objects.Reference2LongMap;
import it.unimi.dsi.fastutil.objects.Reference2LongMaps;

public class Profiler {
    private final Reference2LongMap<String> values = new Reference2LongArrayMap<>();

    private final Reference2LongMap<String> start = new Reference2LongArrayMap<>();

    public Profiler() {

    }

    public void start() {
        this.values.clear();
        this.start.clear();
    }

    public void startSection(String value) {
        start.put(value, System.currentTimeMillis());
    }

    public void endSection(String value) {
        long startTime = start.removeLong(value);
        long endTime = System.currentTimeMillis();
        long time = endTime - startTime;
        values.put(value, time);
    }

    public Reference2LongMap<String> end() {
        return Reference2LongMaps.unmodifiable(values);
    }
}
