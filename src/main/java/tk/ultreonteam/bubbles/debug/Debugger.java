package tk.ultreonteam.bubbles.debug;

import tk.ultreonteam.bubbles.BubbleBlaster;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Debugger {
    private static final Logger logger = LogManager.getLogger("Debugger");
    private static final Logger verboseLogger = LogManager.getLogger("Verbose");

    public static void log(String message) {
        if (BubbleBlaster.isDebugMode()) {
            logger.info(message);
        }
    }

    public static void verbose(String message) {
        if (BubbleBlaster.isDebugMode()) {
            verboseLogger.info(message);
        }
    }
}
