package tk.ultreonteam.bubbles.debug;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.ingame.event.GameplayEvent;
import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.input.MouseInput;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.Screen;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DebugRenderer {
    private final Font font;
    private final BubbleBlaster game;

    public DebugRenderer(BubbleBlaster game) {
        this.game = game;
        font = new Font("consolas", Font.PLAIN, 11);
    }

    @SuppressWarnings("UnusedAssignment")
    public void render(Renderer renderer) {
        renderer.font(font);
        renderer.color(255, 255, 255);

        int i = 0;

        Rectangle gameBounds = game.getGameBounds();
        drawLine(renderer, "FPS: " + game.getFps(), i++);
        drawLine(renderer, "RPT: " + game.getRenderPartialTicks(), i++);
        drawLine(renderer, "Game Bounds: (" + gameBounds.x + ", " + gameBounds.y + ") " + gameBounds.width + " x " + gameBounds.height, i++);
        drawLine(renderer, "Scaled Size: " + game.getScaledWidth() + " \u00D7 " + game.getScaledHeight(), i++);
        drawLine(renderer, "Window Size: " + game.getGameWindow().getWidth() + " \u00D7 " + game.getGameWindow().getHeight(), i++);
        drawLine(renderer, "Canvas Size: " + game.getWidth() + " \u00D7 " + game.getHeight(), i++);
        Environment environment = game.environment;
        if (environment != null) {
            GameplayEvent curGe = environment.getCurrentGameEvent();
            drawLine(renderer, "Entity Count: " + environment.getEntities().size(), i++);
            drawLine(renderer, "Visible Entity Count: " + environment.getEntities().stream().filter(Entity::isVisible).count(), i++);
            drawLine(renderer, "Entity Removal Count: " + environment.getEntities().stream().filter(Entity::willBeDeleted).count(), i++);
            drawLine(renderer, "Cur. Game Event: " + (curGe != null ? curGe.id() : "null"), i++);
            drawLine(renderer, "Is Initialized: " + environment.isInitialized(), i++);
            drawLine(renderer, "Difficulty: " + environment.getDifficulty().name(), i++);
            drawLine(renderer, "Local Difficulty: " + environment.getLocalDifficulty(), i++);
            drawLine(renderer, "Seed: " + environment.getSeed(), i++);

            if (KeyInput.isDown(KeyInput.Map.KEY_SHIFT)) {
                Entity entityAt = environment.getEntityAt(MouseInput.getPos());
                if (entityAt != null) {
                    drawLine(renderer, "Entity Type: " + entityAt.getType().id(), i++);
                    if (entityAt instanceof BubbleEntity bubble) {
                        drawLine(renderer, "Bubble Type: " + bubble.getBubbleType().id(), i++);
                        drawLine(renderer, "Base Speed: " + bubble.getBaseSpeed(), i++);
                        drawLine(renderer, "Speed: " + bubble.getSpeed(), i++);
                    }
                }
            }

            Player player = environment.getPlayer();
            if (player != null) {
                NumberFormat formatter = new DecimalFormat("#0.00000");
                drawLine(renderer, "Pos: (" + formatter.format(player.getPos().getX()) + ", " + formatter.format(player.getPos().getY()) + ")", i++);
                drawLine(renderer, "Score: " + player.getScore(), i++);
                drawLine(renderer, "Level: " + player.getLevel(), i++);
                drawLine(renderer, "Speed: " + player.getSpeed(), i++);
                drawLine(renderer, "Rotation: " + player.getRotation(), i++);
                drawLine(renderer, "Rot Speed: " + player.getRotationSpeed(), i++);
            }
        }
        Screen screen = game.getCurrentScreen();
        drawLine(renderer, "Screen: " + (screen == null ? "null" : screen.getClass().getName()), i++);
    }

    private void drawLine(Renderer renderer, String text, int line) {
        int height = renderer.fontMetrics(font).getHeight() + 1 + 2;
        int width = renderer.fontMetrics(font).stringWidth(text);
        line++;
        int y = line * height - 3;
        if (game.getCurrentScreen() == null) {
            y += 70;
        }

        renderer.color(0, 0, 0, 0x99);
        renderer.rect(10, y, width + 4, height - 1);
        renderer.color("#fff");
        renderer.text(text, 10 + 2, y + (height - 1 + 1) / 1.5f);
    }
}
