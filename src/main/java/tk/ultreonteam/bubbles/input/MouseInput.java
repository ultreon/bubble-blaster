package tk.ultreonteam.bubbles.input;

import tk.ultreonteam.bubbles.vector.Vec2i;

import java.awt.*;

public final class MouseInput {
    private final Controller controller = new Controller();
    private static MouseInput instance;

    private MouseInput() {

    }

    public static void init() {
        if (instance == null) {
            instance = new MouseInput();
        }
    }

    public static boolean isPressed(Button button) {
        return instance.controller.isPressed(button.id);
    }

    public static int getX() {
        return getPos().x;
    }

    public static int getY() {
        return getPos().y;
    }

    public static Vec2i getPos() {
        Vec2i mouseVec = instance.controller.getPos();
        return mouseVec == null ? new Vec2i(-1, -1) : mouseVec;
    }

    public static void listen(Component canvas) {
        canvas.addMouseListener(instance.controller);
        canvas.addMouseMotionListener(instance.controller);
        canvas.addMouseWheelListener(instance.controller);
    }

    private static class Controller extends tk.ultreonteam.bubbles.core.input.MouseInput {
        @Override
        protected Vec2i getPosOnScreen() {
            return super.getPosOnScreen();
        }

        @Override
        protected Vec2i getPos() {
            return super.getPos();
        }

        @Override
        protected int getClicks() {
            return super.getClicks();
        }

        @Override
        protected boolean isPressed(int button) {
            return super.isPressed(button);
        }
    }

    enum Button {
        LEFT(1),
        RIGHT(2),
        MIDDLE(3),
        ;

        private final int id;

        Button(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}
