package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.BubbleBlaster;

/**
 * Update Event
 * This event is for updating values, or doing things such as collision.
 *
 * @see GameEvent
 * @see RenderEvent
 */
public class TickEvent extends GameEvent {
    private final BubbleBlaster main;

    public TickEvent(BubbleBlaster main) {
        this.main = main;
    }

    public BubbleBlaster getGame() {
        return main;
    }
}
