package tk.ultreonteam.bubbles.event.load;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.GameEvent;

public class GameSetupEvent extends GameEvent {
    private final BubbleBlaster game;

    public GameSetupEvent(BubbleBlaster game) {
        this.game = game;
    }

    public BubbleBlaster getGame() {
        return game;
    }
}