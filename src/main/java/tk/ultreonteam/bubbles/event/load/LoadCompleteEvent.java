package tk.ultreonteam.bubbles.event.load;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.ModEvent;

public class LoadCompleteEvent extends ModEvent {
    private final BubbleBlaster game;

    public LoadCompleteEvent(BubbleBlaster game) {
        this.game = game;
    }

    public BubbleBlaster getGame() {
        return game;
    }
}