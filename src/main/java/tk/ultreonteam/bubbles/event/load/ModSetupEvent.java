package tk.ultreonteam.bubbles.event.load;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.ModEvent;
import tk.ultreonteam.bubbles.mod.ModContainer;

/**
 * Mod Setup Event
 * This event is for post-initialize mods.
 *
 * @since 1.0.0
 */
public class ModSetupEvent extends ModEvent {
    private final ModContainer mod;
    private final BubbleBlaster game;

    public ModSetupEvent(ModContainer mod, BubbleBlaster game) {
        this.mod = mod;
        this.game = game;
    }

    public ModContainer getMod() {
        return mod;
    }

    public BubbleBlaster getGame() {
        return game;
    }
}
