package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.type.KeyEventType;

public class KeyPressEvent extends KeyboardEvent {
    private final boolean isHold;

    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game      The game's instance.
     * @param keyCode   The key-code of the key in the event.
     * @param keyCodeEx the extended key-code of the key in the event.
     * @param scanCode  the keyboard scancode of the key in the event.
     * @param symbol   the character of the key in the event.
     * @param modifiers the modifiers used in the event.
     */
    public KeyPressEvent(BubbleBlaster game, int keyCode, int keyCodeEx, int scanCode, char symbol, int modifiers, boolean isHold) {
        super(game, keyCode, keyCodeEx, scanCode, symbol, modifiers, isHold ? KeyEventType.HOLD : KeyEventType.PRESS);
        this.isHold = isHold;
    }

    /**
     * Check if the key is held.
     *
     * @return whether the key is held down.
     * @since 0.1.0
     * @author Qboi
     */
    public boolean isHold() {
        return isHold;
    }
}
