package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.type.KeyEventType;

public class KeyReleaseEvent extends KeyboardEvent {
    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game      The game's instance.
     * @param keyCode   The key-code of the key in the event.
     * @param keyCodeEx the extended key-code of the key in the event.
     * @param scanCode  the keyboard scancode of the key in the event.
     * @param symbol    the character of the key in the event.
     * @param modifiers the modifiers used in the event.
     */
    public KeyReleaseEvent(BubbleBlaster game, int keyCode, int keyCodeEx, int scanCode, char symbol, int modifiers) {
        super(game, keyCode, keyCodeEx, scanCode, symbol, modifiers, KeyEventType.RELEASE);
    }
}
