package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.core.input.MouseInput;
import tk.ultreonteam.bubbles.event.GameEvent;
import tk.ultreonteam.bubbles.event.type.MouseEventType;
import tk.ultreonteam.bubbles.render.shapes.Point;
import tk.ultreonteam.bubbles.vector.Vec2i;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Mouse Event
 * This event is used for handling mouse input.
 *
 * @author Qboi
 * @see MouseMotionEvent
 * @see MouseWheelEvent
 * @see MouseClickEvent
 * @see MousePressEvent
 * @see MouseReleaseEvent
 */
public class MouseEvent extends GameEvent {
    private final BubbleBlaster game;
    private final int posX;
    private final int posY;

    private final MouseEventType type;

    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game the game instance.
     * @param posX the x position of the mouse cursor when the event happened.
     * @param posY the y position of the mouse cursor when the event happened.
     * @param type the type of mouse event that happened.
     */
    public MouseEvent(BubbleBlaster game, int posX, int posY, MouseEventType type) {
        this.game = game;
        this.posX = posX;
        this.posY = posY;
        this.type = type;
    }

    public BubbleBlaster getGame() {
        return game;
    }

    @Deprecated(since = "0.1.0")
    public @Nullable MouseInput getController() {
        return null;
    }

    @Deprecated(since = "0.1.0")
    public java.awt.event.MouseEvent getParentEvent() {
        return null;
    }

    public MouseEventType getType() {
        return type;
    }

    @Deprecated(since = "0.1.0")
    public Point getLocationOnScreen() {
        return new Point(0,0);
    }

    @Deprecated(since = "0.1.0")
    public int getClickCount() {
        return 0;
    }

    @Deprecated(since = "0.1.0")
    public Point getPoint() {
        return new Point(0,0);
    }

    public Vec2i getPos() {
        return new Vec2i(posX, posY);
    }

    public int getPosX() {
        return posX;
    }

    @Deprecated(since = "0.1.0")
    public int getXOnScreen() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    @Deprecated(since = "0.1.0")
    public int getYOnScreen() {
        return posY;
    }
}
