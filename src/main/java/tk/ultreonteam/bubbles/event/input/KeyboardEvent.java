package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.core.input.KeyboardInput;
import tk.ultreonteam.bubbles.event.GameEvent;
import tk.ultreonteam.bubbles.event.type.KeyEventType;

import java.awt.event.KeyEvent;

/**
 * Keyboard Event
 * This event is used for handling keyboard input.
 *
 * @see KeyPressEvent
 * @see KeyEventType
 */
public class KeyboardEvent extends GameEvent {
    private final BubbleBlaster game;
    private final int keyCode;
    private final char symbol;
    private final int modifiers;

    private final int keyCodeEx;
    private final int scanCode;
    private final KeyEventType type;

    private static final int SHIFT_MASK = 1;
    private static final int CTRL_MASK = 1 << 1;
    private static final int META_MASK = 1 << 2;
    private static final int ALT_MASK = 1 << 3;
    private static final int ALT_GRAPH_MASK = 1 << 5;

    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game The game's instance.
     * @param keyCode The key-code of the key in the event.
     * @param keyCodeEx the extended key-code of the key in the event.
     * @param scanCode the keyboard scancode of the key in the event.
     * @param symbol the character of the key in the event.
     * @param modifiers the modifiers used in the event.
     * @param type the type of keyboard event happened.
     */
    public KeyboardEvent(BubbleBlaster game, int keyCode, int keyCodeEx, int scanCode, char symbol, int modifiers, KeyEventType type) {
        this.game = game;
        this.keyCode = keyCode;
        this.keyCodeEx = keyCodeEx;
        this.scanCode = scanCode;
        this.symbol = symbol;
        this.modifiers = modifiers;
        this.type = type;
    }

    public BubbleBlaster getGame() {
        return game;
    }

    /**
     * Returns the KeyboardController instance used in the event.
     *
     * @return The KeyboardController instance.
     */
    @Deprecated(since = "0.1.0")
    public KeyboardInput getController() {
        return null;
    }

    @Deprecated(since = "0.1.0")
    public KeyEvent getParentEvent() {
        return null;
    }

    @Deprecated(since = "0.1.0")
    public int getExtendedKeyCode() {
        return keyCodeEx;
    }

    /**
     * @return the key-code of the event/
     */
    public int getKeyCode() {
        return keyCode;
    }

    /**
     * @return the character of the key happening in the event.
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * @return the modifiers used in the event.
     */
    public int getModifiers() {
        return modifiers;
    }

    /**
     * @return the extended key-code of the key happening in the event.
     */
    public int getKeyCodeEx() {
        return keyCodeEx;
    }

    /**
     * @return the keyboard scancode of the key.
     */
    public int getScanCode() {
        return scanCode;
    }

    /**
     * @return the type of keyboard event happening.
     */
    public KeyEventType getType() {
        return type;
    }

    /**
     * Returns whether the Shift modifier is down on this event.
     * @return whether the Shift modifier is down on this event
     */
    public boolean isShiftDown() {
        return (modifiers & SHIFT_MASK) != 0;
    }

    /**
     * Returns whether the Control modifier is down on this event.
     * @return whether the Control modifier is down on this event
     */
    public boolean isControlDown() {
        return (modifiers & CTRL_MASK) != 0;
    }

    /**
     * Returns whether the Meta modifier is down on this event.
     * @return whether the Meta modifier is down on this event
     */
    public boolean isMetaDown() {
        return (modifiers & META_MASK) != 0;
    }

    /**
     * Returns whether the Alt modifier is down on this event.
     * @return whether the Alt modifier is down on this event
     */
    public boolean isAltDown() {
        return (modifiers & ALT_MASK) != 0;
    }

    /**
     * Returns whether the AltGraph modifier is down on this event.
     * @return whether the AltGraph modifier is down on this event
     */
    public boolean isAltGraphDown() {
        return (modifiers & ALT_GRAPH_MASK) != 0;
    }
}
