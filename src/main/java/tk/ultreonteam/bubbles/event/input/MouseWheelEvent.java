package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.core.input.MouseInput;
import tk.ultreonteam.bubbles.event.type.MouseEventType;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Mouse Wheel Event
 * This event is used for handling keyboard input.
 *
 * @author Qboi
 */
public class MouseWheelEvent extends MouseEvent {
    private final double preciseWheelRotation;
    private final int wheelRotation;
    private final int scrollAmount;
    private final int scrollType;
    private final int unitsToScroll;
    private final int atX;
    private final int atY;

    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game                 The {@link BubbleBlaster} instance.
     * @param preciseWheelRotation the exact wheel rotation (this is a floating point variant of {@code wheelRotation}).
     * @param wheelRotation the wheel rotation in normal integers.
     * @param scrollAmount scroll amount.
     * @param scrollType scroll type.
     * @param unitsToScroll units to scroll.
     * @param atX the x position of the mouse when the event happened.
     * @param atY the y position of the mouse when the event happened.
     */
    public MouseWheelEvent(BubbleBlaster game, double preciseWheelRotation, int wheelRotation, int scrollAmount, int scrollType, int unitsToScroll, int atX, int atY) {
        super(game, atX, atY, MouseEventType.SCROLL);
        this.preciseWheelRotation = preciseWheelRotation;
        this.wheelRotation = wheelRotation;
        this.scrollAmount = scrollAmount;
        this.scrollType = scrollType;
        this.unitsToScroll = unitsToScroll;
        this.atX = atX;
        this.atY = atY;
    }

    @Deprecated(since = "0.1.0")
    public @Nullable MouseInput getController() {
        return null;
    }

    @Deprecated(since = "0.1.0-indev", forRemoval = true)
    public java.awt.event.MouseEvent getParentEvent() {
        return null;
    }

    public double getPreciseWheelRotation() {
        return preciseWheelRotation;
    }

    public int getWheelRotation() {
        return wheelRotation;
    }

    public int getScrollAmount() {
        return scrollAmount;
    }

    public int getScrollType() {
        return scrollType;
    }

    public int getUnitsToScroll() {
        return unitsToScroll;
    }

    public int getPosX() {
        return atX;
    }

    public int getPosY() {
        return atY;
    }
}
