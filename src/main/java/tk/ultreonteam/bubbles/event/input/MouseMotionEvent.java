package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.type.MouseEventType;

/**
 * Mouse Motion Event
 * This event is used for handling mouse motion input.
 *
 * @author Qboi
 */
public class MouseMotionEvent extends MouseEvent {
    private final int fromX;
    private final int fromY;
    private final int toX;
    private final int toY;

    /**
     * Keyboard event, called from a specific scene.
     *
     * @param game       The {@link BubbleBlaster} instance.
     * @param fromX the original x position.
     * @param fromY the original y position.
     * @param toX the new y position.
     * @param toY the new y position.
     */
    public MouseMotionEvent(BubbleBlaster game, int fromX, int fromY, int toX, int toY) {
        super(game, toX, toY, MouseEventType.MOTION);
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
    }

    public int getFromX() {
        return fromX;
    }

    public int getFromY() {
        return fromY;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }
}
