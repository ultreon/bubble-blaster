package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.type.MouseEventType;

/**
 * @author Qboi
 */
public class MousePressEvent extends MouseEvent {
    private final BubbleBlaster game;
    private final int atX;
    private final int atY;
    private final int button;

    public MousePressEvent(BubbleBlaster game, int atX, int atY, int button) {
        super(game, atX, atY, MouseEventType.CLICK);
        this.game = game;
        this.atX = atX;
        this.atY = atY;
        this.button = button;
    }

    public BubbleBlaster getGame() {
        return game;
    }

    public int getPosX() {
        return atX;
    }

    public int getPosY() {
        return atY;
    }

    public int getButton() {
        return button;
    }
}
