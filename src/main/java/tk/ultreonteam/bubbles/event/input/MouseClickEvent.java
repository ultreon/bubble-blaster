package tk.ultreonteam.bubbles.event.input;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.event.type.MouseEventType;

public class MouseClickEvent extends MouseEvent {
    private final int clickCount;
    private final int button;

    public MouseClickEvent(BubbleBlaster game, int atX, int atY, int clickCount, int button) {
        super(game, atX, atY, MouseEventType.CLICK);
        this.clickCount = clickCount;
        this.button = button;
    }

    public int getClickCount() {
        return clickCount;
    }

    public int getButton() {
        return button;
    }
}
