package tk.ultreonteam.bubbles.event.type;

/**
 * Key Event Types.
 */
public enum KeyEventType {
    PRESS(), RELEASE(), TYPE(), HOLD
}
