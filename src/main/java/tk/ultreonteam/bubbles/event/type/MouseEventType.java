package tk.ultreonteam.bubbles.event.type;

/**
 * Mouse Event Types.
 */
public enum MouseEventType {
    CLICK, RELEASE, MOTION, PRESS, ENTER, LEAVE, SCROLL, DRAG
}
