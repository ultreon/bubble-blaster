package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.render.Renderer;

public abstract class RenderEnvironmentEvent extends RenderEvent {
    private final Environment environment;

    public RenderEnvironmentEvent(Renderer graphics, Environment environment) {
        super(graphics);
        this.environment = environment;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public static class Before extends RenderEnvironmentEvent {
        public Before(Renderer graphics, Environment environment) {
            super(graphics, environment);
        }
    }

    public static class After extends RenderEnvironmentEvent {
        public After(Renderer graphics, Environment environment) {
            super(graphics, environment);
        }
    }
}
