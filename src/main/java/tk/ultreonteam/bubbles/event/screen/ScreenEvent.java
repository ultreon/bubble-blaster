package tk.ultreonteam.bubbles.event.screen;

import tk.ultreonteam.bubbles.event.GameEvent;
import tk.ultreonteam.bubbles.render.screen.Screen;

public abstract class ScreenEvent extends GameEvent {
    private final Screen screen;

    public ScreenEvent(Screen screen) {
        this.screen = screen;
    }

    public Screen getScreen() {
        return screen;
    }
}
