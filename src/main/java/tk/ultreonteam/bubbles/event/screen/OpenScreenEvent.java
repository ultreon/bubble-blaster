package tk.ultreonteam.bubbles.event.screen;

import tk.ultreonteam.bubbles.render.screen.Screen;

public class OpenScreenEvent extends ScreenEvent {
    public OpenScreenEvent(Screen screen) {
        super(screen);
    }
}
