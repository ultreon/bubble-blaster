package tk.ultreonteam.bubbles.event.screen;

import tk.ultreonteam.bubbles.render.screen.Screen;

public class InitScreenEvent extends ScreenEvent {
    public InitScreenEvent(Screen screen) {
        super(screen);
    }
}
