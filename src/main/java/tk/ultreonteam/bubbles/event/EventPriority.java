package tk.ultreonteam.bubbles.event;

/**
 * Event Priorities
 */
public enum EventPriority {
    LOWEST, LOWER, LOW, NORMAL, HIGH, HIGHER, HIGHEST
}
