package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.GameObject;
import tk.ultreonteam.commons.lang.ICancellable;

public class CollisionEvent extends GameEvent implements ICancellable {
    private final BubbleBlaster game;
    private final GameObject source;
    private final GameObject target;

    public CollisionEvent(BubbleBlaster game, GameObject source, GameObject target) {
        this.game = game;
        this.source = source;
        this.target = target;
    }

    public BubbleBlaster getGame() {
        return game;
    }

    public GameObject getSource() {
        return source;
    }

    public GameObject getTarget() {
        return target;
    }
}
