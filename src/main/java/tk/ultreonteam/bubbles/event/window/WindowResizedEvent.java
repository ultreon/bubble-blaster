package tk.ultreonteam.bubbles.event.window;

import tk.ultreonteam.bubbles.GameWindow;

public class WindowResizedEvent extends WindowEvent {
    private final int width;
    private final int height;

    public WindowResizedEvent(GameWindow window, int width, int height) {
        super(window);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
