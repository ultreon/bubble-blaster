package tk.ultreonteam.bubbles.event.window;

import tk.ultreonteam.bubbles.GameWindow;

public class WindowShownEvent extends WindowEvent {
    public WindowShownEvent(GameWindow window) {
        super(window);
    }
}
