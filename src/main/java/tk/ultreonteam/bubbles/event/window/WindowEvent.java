package tk.ultreonteam.bubbles.event.window;

import tk.ultreonteam.bubbles.GameWindow;
import tk.ultreonteam.bubbles.event.GameEvent;

public abstract class WindowEvent extends GameEvent {
    private final GameWindow window;

    public WindowEvent(GameWindow window) {
        this.window = window;
    }

    public GameWindow getWindow() {
        return window;
    }
}
