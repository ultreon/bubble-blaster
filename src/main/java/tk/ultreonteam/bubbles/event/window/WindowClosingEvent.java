package tk.ultreonteam.bubbles.event.window;

import tk.ultreonteam.bubbles.GameWindow;
import tk.ultreonteam.commons.lang.ICancellable;

public class WindowClosingEvent extends WindowEvent implements ICancellable {
    public WindowClosingEvent(GameWindow window) {
        super(window);
    }
}
