package tk.ultreonteam.bubbles.event.window;

import tk.ultreonteam.bubbles.GameWindow;
import tk.ultreonteam.bubbles.vector.Vec2f;
import tk.ultreonteam.bubbles.vector.Vec2i;

public class WindowResizeEvent extends WindowEvent {
    private final Vec2i oldSize;
    private final Vec2f newSize;

    public WindowResizeEvent(GameWindow window, Vec2i oldSize, Vec2f newSize) {
        super(window);
        this.oldSize = oldSize;
        this.newSize = newSize;
    }

    public Vec2i getOldSize() {
        return oldSize;
    }

    public Vec2f getNewSize() {
        return newSize;
    }
}
