package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.BubbleBlaster;

public class GameExitEvent extends GameEvent {
    private final BubbleBlaster game;

    public GameExitEvent(BubbleBlaster game) {
        this.game = game;
    }

    public BubbleBlaster getGame() {
        return game;
    }
}
