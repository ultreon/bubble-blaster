package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.render.Renderer;

public abstract class RenderEvent extends GameEvent {
    private final Renderer graphics;

    public RenderEvent(Renderer graphics) {
        this.graphics = graphics;
    }

    public Renderer graphics() {
        return graphics;
    }

}
