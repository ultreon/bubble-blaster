package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.render.TextureCollection;

public class CollectTexturesEvent extends GameEvent {
    private final TextureCollection textureCollection;

    public CollectTexturesEvent(TextureCollection textureCollection) {
        this.textureCollection = textureCollection;
    }

    public TextureCollection getTextureCollection() {
        return textureCollection;
    }
}
