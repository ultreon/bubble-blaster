package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.annotation.Cancelable;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.SpawnInformation;

@Cancelable
public class EntitySpawnEvent extends EntityEvent {
    private final SpawnInformation spawnInformation;

    public EntitySpawnEvent(Entity entity, SpawnInformation spawnInformation) {
        super(entity);
        this.spawnInformation = spawnInformation;
    }

    public SpawnInformation getSpawnInformation() {
        return spawnInformation;
    }
}
