package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.util.ExceptionUtils;

public class PlayerTickEvent {
    private PlayerTickEvent() {
        throw ExceptionUtils.utilityClass();
    }

    public static class Before extends EntityTickEvent.Before {
        private final Player player;

        public Before(Player player) {
            super(player);
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }
    }

    public static class After extends EntityTickEvent.Before {
        private final Player player;

        public After(Player player) {
            super(player);
            this.player = player;
        }

        public Player getPlayer() {
            return player;
        }
    }
}
