package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.event.GameEvent;

public abstract class EntityEvent extends GameEvent {
    private final Entity entity;

    public EntityEvent(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
