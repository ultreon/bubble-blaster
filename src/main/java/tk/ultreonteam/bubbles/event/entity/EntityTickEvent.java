package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.entity.Entity;

public class EntityTickEvent extends EntityEvent {
    public EntityTickEvent(Entity entity) {
        super(entity);
    }

    public static class Before extends EntityTickEvent {
        public Before(Entity entity) {
            super(entity);
        }
    }

    public static class After extends EntityTickEvent {
        public After(Entity entity) {
            super(entity);
        }
    }
}
