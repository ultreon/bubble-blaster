package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.damage.DamageSource;
import tk.ultreonteam.commons.lang.ICancellable;

public class EntityDamageEvent extends EntityEvent implements ICancellable {
    private DamageSource damageSource;

    public EntityDamageEvent(Entity entity, DamageSource damageSource) {
        super(entity);
        this.damageSource = damageSource;
    }

    public DamageSource getDamageSource() {
        return damageSource;
    }

    public void setDamageSource(DamageSource damageSource) {
        this.damageSource = damageSource;
    }
}
