package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.entity.damage.DamageSource;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;

public class PlayerDamageEvent extends EntityDamageEvent {
    private final Player player;

    public PlayerDamageEvent(Player player, DamageSource damageSource) {
        super(player, damageSource);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
