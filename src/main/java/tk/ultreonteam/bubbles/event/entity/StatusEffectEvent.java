package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.ingame.effect.StatusEffectInstance;
import tk.ultreonteam.bubbles.event.GameEvent;

public abstract class StatusEffectEvent extends GameEvent {
    protected StatusEffectInstance statusEffect;

    public StatusEffectEvent(StatusEffectInstance statusEffect) {
        this.statusEffect = statusEffect;
    }

    public static class Gain extends StatusEffectEvent {
        public Gain(StatusEffectInstance statusEffect) {
            super(statusEffect);
        }

        public void setStatusEffect(StatusEffectInstance statusEffect) {
            this.statusEffect = statusEffect;
        }
    }

    public static class Update extends StatusEffectEvent {
        private final StatusEffectInstance from;

        public Update(StatusEffectInstance from, StatusEffectInstance statusEffect) {
            super(statusEffect);
            this.from = from;
        }

        public StatusEffectInstance getTo() {
            return statusEffect;
        }

        public void setTo(StatusEffectInstance statusEffect) {
            this.statusEffect = statusEffect;
        }

        public StatusEffectInstance getFrom() {
            return from;
        }
    }

    public static class Loose extends StatusEffectEvent {
        public Loose(StatusEffectInstance statusEffect) {
            super(statusEffect);
        }
    }
}
