package tk.ultreonteam.bubbles.event.entity;

import tk.ultreonteam.bubbles.BubbleBlaster;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.event.CollisionEvent;
import tk.ultreonteam.bubbles.render.Renderer;

/**
 * Render Event
 * This event is for rendering the game objects on the canvas.
 *
 * @see Renderer
 * @see Renderer
 */
public class EntityCollisionEvent extends CollisionEvent {
    private final BubbleBlaster main;
    private final double deltaTime;
    private final Entity source;
    private final Entity target;

    public EntityCollisionEvent(BubbleBlaster main, double deltaTime, Entity source, Entity target) {
        super(main, source, target);
        this.deltaTime = deltaTime;
        this.source = source;
        this.target = target;
        this.main = main;
    }

    public BubbleBlaster getMain() {
        return main;
    }

    public Entity getSource() {
        return source;
    }

    public Entity getTarget() {
        return target;
    }

    public double getDeltaTime() {
        return deltaTime;
    }
}
