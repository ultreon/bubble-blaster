package tk.ultreonteam.bubbles.event.bus;

import tk.ultreonteam.bubbles.common.EventSubscriber;
import tk.ultreonteam.bubbles.common.MethodCall;
import tk.ultreonteam.bubbles.event.AbstractEvent;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.Subscriber;
import tk.ultreonteam.commons.lang.ICancellable;
import org.apache.logging.log4j.Logger;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Consumer;
import java.util.function.Predicate;

@SuppressWarnings("unchecked")
public abstract class AbstractEvents<T extends AbstractEvent> {
    protected static final Predicate<Method> classPredicate;
    protected static final Predicate<Method> instancePredicate;

    static {
        Predicate<Method> isHandler = AbstractEvents::isSubscriber;
        Predicate<Method> isSubscriber = AbstractEvents::isSubscribing;
        classPredicate = isHandler.and(isSubscriber).and((method) -> Modifier.isStatic(method.getModifiers()));
        instancePredicate = isHandler.and(isSubscriber).and((method) -> !Modifier.isStatic(method.getModifiers()));
    }

    //    public final Map<Class<? extends AbstractEvent>, CopyOnWriteArraySet<Pair<Object, Method>>> eventToMethods = new HashMap<>();
    public final Map<Class<? extends T>, CopyOnWriteArraySet<EventSubscriber<?>>> eventToHandlers = new HashMap<>();
    public final Map<EventSubscriber<?>, Class<? extends T>> handlerToEvent = new HashMap<>();
    public final Map<MethodCall, EventSubscriber<?>> methodToHandler = new HashMap<>();

    protected final Logger logger;

    public AbstractEvents(Logger logger) {
        this.logger = logger;
    }

    private static boolean isSubscribing(Method method) {
//        LogManager.getLogger("Subscribe-Check").info(method.getDeclaringClass().getName() + "." + method.getName());

        return method.isAnnotationPresent(SubscribeEvent.class);
    }

    private static boolean isSubscriber(Method method) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 1) {
            Class<?> clazz1 = parameterTypes[0];
            return AbstractEvent.class.isAssignableFrom(clazz1);
        }
        return false;
    }

    public <E extends T> boolean publish(E event) {
        if (!eventToHandlers.containsKey(event.getClass())) {
            return false;
        }

        CopyOnWriteArraySet<EventSubscriber<?>> methods = eventToHandlers.get(event.getClass());
        for (EventSubscriber<?> method : methods) {
            try {
                method.handle0(event);
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }

        return event instanceof ICancellable && ((ICancellable) event).isCancelled();
    }

    public void subscribe(Class<?> clazz) {
        loopDeclaredMethods(clazz, (method) -> {
            // Get types and values.
            Class<? extends T> event = (Class<? extends T>) method.getParameterTypes()[0];
            subscribe(event, null, method);
        });
    }

    public void subscribe(Object o) {
        loopMethods(o, (method) -> {
            // Get types and values.
            Class<? extends T> event = (Class<? extends T>) method.getParameterTypes()[0];
            subscribe(event, o, method);
        });
    }

    public <E extends T> void subscribe(Class<E> eventClass, EventSubscriber<E> subscriber) {
        handlerToEvent.put(subscriber, eventClass);
        eventToHandlers.computeIfAbsent(eventClass, o -> new CopyOnWriteArraySet<>()).add(subscriber);
    }

    public void unsubscribe(Class<? extends T> event, Class<?> clazz) {
        loopDeclaredMethods(clazz, (method) -> {
            // Get and check event.
            Class<? extends AbstractEvent> evt = (Class<? extends AbstractEvent>) method.getParameterTypes()[0];
            if (event == evt) {
                // Remove handler.
                try {
                    unsubscribe(event, null, method);
                } catch (IllegalStateException ignored) {

                }
            }
        });
    }

    public void unsubscribe(Class<? extends T> event, Object o) {
        loopMethods(o, (method) -> {
            // Get types and values.
            Class<? extends AbstractEvent> evt = (Class<? extends AbstractEvent>) method.getParameterTypes()[0];
            if (event == evt) {
                // Remove handler.
                try {
                    unsubscribe(event, o, method);
                } catch (IllegalStateException ignored) {

                }
            }
        });
    }

    public void unsubscribe(Class<?> clazz) {
        loopDeclaredMethods(clazz, (method) -> {
            // Get and check event.
            Class<? extends AbstractEvent> evt = (Class<? extends AbstractEvent>) method.getParameterTypes()[0];

            // Remove handler.
            try {
                unsubscribe(evt, null, method);
            } catch (IllegalStateException ignored) {

            }
        });
    }

    public void unsubscribe(Object o) {
        loopMethods(o, (method) -> {
            // Get types and values.
            Class<? extends AbstractEvent> evt = (Class<? extends AbstractEvent>) method.getParameterTypes()[0];

            // Remove handler.
            try {
                unsubscribe(evt, o, method);
            } catch (IllegalStateException ignored) {

            }
        });
    }

    private void loopDeclaredMethods(Class<?> clazz, Consumer<Method> consumer) {
        // Loop declared methods.
        loopMethods0(clazz.getDeclaredMethods(), classPredicate, consumer);
    }

    private void loopMethods(Object o, Consumer<Method> consumer) {
        // Loop methods.
        loopMethods0(o.getClass().getMethods(), instancePredicate, consumer);
    }

    private void loopMethods0(Method[] methods, Predicate<Method> predicate, Consumer<Method> consumer) {
        // Check all methods for event subscribers.
        for (Method method : methods) {
            // Check is instance method.
            if (predicate.test(method)) {
                // Set accessible.
                method.setAccessible(true);
                consumer.accept(method);
            }
        }
    }

    private void unsubscribe(Class<? extends AbstractEvent> event, @Nullable Object obj, Method method) {
        MethodCall call = new MethodCall(obj, method);
        EventSubscriber<?> handler = methodToHandler.get(call);
        if (handler == null) throw new IllegalStateException("Method not registered.");

        if (!eventToHandlers.containsKey(event))
            throw new IllegalStateException("Cannot unregister method for a non-registered event.");
        else if (!eventToHandlers.get(event).contains(handler))
            throw new IllegalStateException("Cannot unregister an unregistered handler.");

        if (!handlerToEvent.containsKey(handler))
            throw new IllegalStateException("Cannot unregister an unregistered handler.");

        handlerToEvent.remove(handler);
        eventToHandlers.get(event).remove(handler);
    }

    private void unsubscribeAll(@Nullable Object obj, Method method) {
        MethodCall call = new MethodCall(obj, method);
        EventSubscriber<?> handler = methodToHandler.get(call);

        if (!handlerToEvent.containsKey(handler))
            throw new IllegalStateException("Cannot unregister an unregistered method.");

        Class<? extends T> event = handlerToEvent.get(handler);
        handlerToEvent.remove(handler);
        eventToHandlers.get(event).remove(handler);
    }

    private void subscribe(Class<? extends T> event, @Nullable Object obj, Method method) {
        MethodCall call = new MethodCall(obj, method);
        EventSubscriber<?> handler = EventSubscriber.ofCall(call);

        if (methodToHandler.containsKey(call)) {
            return;
        }

        if (!eventToHandlers.containsKey(event))
            eventToHandlers.put(event, new CopyOnWriteArraySet<>());
        methodToHandler.put(call, handler);
        eventToHandlers.get(event).add(handler);
        handlerToEvent.put(handler, event);
    }

    @Deprecated
    public static abstract class AbstractSubscription {
        protected abstract void onRemove();

        public abstract <T extends AbstractEvent> Collection<Subscriber<T>> getSubscribers(Class<T> clazz);

        public abstract long id();

        public void unsubscribe() {
            onRemove();
        }

        @SuppressWarnings("unchecked")
        <T extends AbstractEvent> void onPublish(T event) {
            Collection<Subscriber<T>> handlers = getSubscribers((Class<T>) event.getClass());
            if (handlers == null) {
                return;
            }

            for (Subscriber<T> handler : handlers) {
                handler.handle(event);
            }
        }
    }
}
