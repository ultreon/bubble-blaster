package tk.ultreonteam.bubbles.event.bus;

import tk.ultreonteam.bubbles.mod.ModInstance;
import tk.ultreonteam.bubbles.mod.ModObject;
import tk.ultreonteam.bubbles.mod.ModContainer;
import tk.ultreonteam.bubbles.mod.ModList;
import org.checkerframework.checker.nullness.qual.NonNull;
import tk.ultreonteam.bubbles.util.ExceptionUtils;

/**
 * This class is used to manage all events.
 *
 * @author ultreon
 * @since 0.1.0
 */
public final class EventManagers {
    /**
     * <b>Should not be called, since this class is not meant to be instantiated.</b>
     */
    private EventManagers() {
        throw ExceptionUtils.utilityClass();
    }

    /**
     * Get the event manager for the given mod.
     *
     * @param modId the id of the mod to get the event manager for.
     * @return the event manager for the given mod.
     */
    public static ModEvents modEvents(String modId) {
        ModContainer container = ModList.get().getContainerById(modId);
        ModObject modObject;
        if (container != null) {
            modObject = container.getModObject();
            return modObject.getEventBus();
        }
        throw new IllegalArgumentException("No event manager found for mod with id " + modId);
    }

    /**
     * Get the event manager for the given mod.
     *
     * @param mod the mod to get the event manager for.
     * @return the event manager for the given mod.
     */
    @NonNull
    public static ModEvents modEvents(ModInstance mod) {
        ModObject modObject = mod.getModObject();
        return modObject.getEventBus();
    }

    /**
     * Get the event manager for the given mod.
     *
     * @param modObject the mod object to get the event manager for.
     * @return the event manager for the given mod.
     * @param <T> the mod instance type.
     */
    @SuppressWarnings("unused")
    public static <T extends ModInstance> ModEvents modEvents(ModObject modObject) {
        return modObject.getEventBus();
    }
}
