package tk.ultreonteam.bubbles.event.bus;

import tk.ultreonteam.bubbles.event.GameEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GameEvents extends AbstractEvents<GameEvent> {
    private static final GameEvents instance = new GameEvents(LogManager.getLogger("Game-Events"));

    public GameEvents(Logger logger) {
        super(logger);
    }

    public static GameEvents get() {
        return instance;
    }

}
