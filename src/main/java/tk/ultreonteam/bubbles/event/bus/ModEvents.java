package tk.ultreonteam.bubbles.event.bus;

import tk.ultreonteam.bubbles.mod.ModObject;
import tk.ultreonteam.bubbles.event.ModEvent;
import tk.ultreonteam.bubbles.mod.ModList;
import org.apache.logging.log4j.LogManager;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class ModEvents extends AbstractEvents<ModEvent> {
    private final ModObject modObject;

    public ModEvents(Object mod) {
        super(LogManager.getLogger("Mod-Events"));
        this.modObject = (ModObject) ModList.get().getContainerByInstance(mod);
    }

    public ModEvents(ModObject modObject) {
        super(LogManager.getLogger("Mod-Events"));
        this.modObject = modObject;
    }

    public ModObject getModObject() {
        return modObject;
    }

    public Object getMod() {
        return modObject.getMod();
    }
}
