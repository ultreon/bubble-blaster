package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.commons.lang.ICancellable;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Locale;

public class LanguageChangeEvent extends GameEvent implements ICancellable {
    private final Locale from;
    private final Locale to;

    public LanguageChangeEvent(Locale from, Locale to) {
        this.from = from;
        this.to = to;
    }

    @Nullable
    public Locale getFrom() {
        return from;
    }

    @NonNull
    public Locale getTo() {
        return to;
    }
}
