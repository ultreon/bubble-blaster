package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.commons.crash.ApplicationCrash;

public class CrashEvent extends GameEvent {
    private final ApplicationCrash crash;

    public CrashEvent(ApplicationCrash crash) {
        this.crash = crash;
    }

    public ApplicationCrash getCrash() {
        return crash;
    }
}
