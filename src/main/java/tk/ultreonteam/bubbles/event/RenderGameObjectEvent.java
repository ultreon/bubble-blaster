package tk.ultreonteam.bubbles.event;

import tk.ultreonteam.bubbles.GameObject;
import tk.ultreonteam.bubbles.render.Renderer;

public abstract class RenderGameObjectEvent extends RenderEvent {
    private final GameObject gameObject;

    public RenderGameObjectEvent(GameObject gameObject, Renderer graphics) {
        super(graphics);
        this.gameObject = gameObject;
    }

    public GameObject getGameObject() {
        return gameObject;
    }

    public static class Before extends RenderGameObjectEvent {
        public Before(GameObject gameObject, Renderer graphics) {
            super(gameObject, graphics);
        }
    }

    public static class After extends RenderGameObjectEvent {
        public After(GameObject gameObject, Renderer graphics) {
            super(gameObject, graphics);
        }
    }
}
