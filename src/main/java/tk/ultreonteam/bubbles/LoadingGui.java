package tk.ultreonteam.bubbles;

import tk.ultreonteam.bubbles.mod.loader.ModLoader;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.gui.FullscreenGui;
import tk.ultreonteam.bubbles.util.GraphicsUtils;
import tk.ultreonteam.commons.lang.Messenger;
import tk.ultreonteam.commons.lang.Pair;
import tk.ultreonteam.commons.lang.ProgressMessenger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@SuppressWarnings("unused")
public final class LoadingGui extends FullscreenGui {
    private static final Logger LOGGER = LogManager.getLogger("Game-Loader");
    private static LoadingGui instance = null;
    private final List<Pair<String, Long>> messages = new CopyOnWriteArrayList<>();
    private final String title = "";
    private final String description = "";
    private ModLoader modLoader = null;
    private static boolean done;
    ProgressMessenger mainProgress = null;
    ProgressMessenger subProgress = null;
    final Messenger messengerMain = new Messenger(this::logMain);
    Messenger messengerSub = new Messenger(this::logSub1);
    private String mainMessage = "";
    private String subMessage1 = "";
    private BubbleBlaster game;

    public LoadingGui(BubbleBlaster game) {
        super();
        this.game = game;
        instance = this;
    }

    private static ModLoader getModLoader() {
        if (instance == null) {
            return null;
        }

        return instance.modLoader;
    }

    public static LoadingGui get() {
        return done ? null : instance;
    }

    public void init() {
        LOGGER.info("Showing LoadScene");

        BubbleBlaster.getEventBus().subscribe(this);
        BubbleBlaster.getInstance().startLoading();
    }

    @Override
    public void render(Renderer renderer) {
        renderer.color(new Color(72, 72, 72));
        renderer.rect(0, 0, BubbleBlaster.getInstance().getWidth(), BubbleBlaster.getInstance().getHeight());

        int i = 0;

        if (mainProgress != null) {
            int progress = mainProgress.getProgress();
            int max = mainProgress.getMax();

            if (mainMessage != null) {
                renderer.color(new Color(128, 128, 128));
                GraphicsUtils.drawCenteredString(renderer, mainMessage, new Rectangle2D.Double(0, (double) BubbleBlaster.getInstance().getHeight() / 2 - 15, BubbleBlaster.getInstance().getWidth(), 30), new Font(game.getSansFontName(), Font.PLAIN, 20));
            }

            renderer.color(new Color(128, 128, 128));
            renderer.rect(BubbleBlaster.getInstance().getWidth() / 2 - 150, BubbleBlaster.getInstance().getHeight() / 2 + 15, 300, 3);

            setupGradient(renderer);
            renderer.rect(BubbleBlaster.getInstance().getWidth() / 2 - 150, BubbleBlaster.getInstance().getHeight() / 2 + 15, (int) (300d * (double) progress / (double) max), 3);

            if (subProgress != null) {
                int progressSub = subProgress.getProgress();
                int maxSub = subProgress.getMax();

                if (subMessage1 != null) {
                    renderer.color(new Color(128, 128, 128));
                    GraphicsUtils.drawCenteredString(renderer, subMessage1, new Rectangle2D.Double(0, (double) BubbleBlaster.getInstance().getHeight() / 2 + 60, BubbleBlaster.getInstance().getWidth(), 30), new Font(game.getSansFontName(), Font.PLAIN, 20));
                }

                renderer.color(new Color(128, 128, 128));
                renderer.rect(BubbleBlaster.getInstance().getWidth() / 2 - 150, BubbleBlaster.getInstance().getHeight() / 2 + 90, 300, 3);

                setupGradient(renderer);
                renderer.rect(BubbleBlaster.getInstance().getWidth() / 2 - 150, BubbleBlaster.getInstance().getHeight() / 2 + 90, (int) (300d * (double) progressSub / (double) maxSub), 3);
            }
        }

        renderer.color(new Color(127, 127, 127));
    }

    private void setupGradient(Renderer gg) {
        gg.color(new Color(0, 192, 255));
        GradientPaint p = new GradientPaint(0, (float) BubbleBlaster.getInstance().getWidth() / 2 - 150, new Color(0, 192, 255), (float) BubbleBlaster.getInstance().getWidth() / 2 + 150, 0f, new Color(0, 255, 192));
        gg.paint(p);
    }

    void done() {
        LoadingGui.done = true;
        BubbleBlaster.getEventBus().unsubscribe(this);
        BubbleBlaster.getInstance().doneLoading();
    }

    public void logInfo(String description) {
        this.messages.add(new Pair<>(description, System.currentTimeMillis()));
    }

    private void logSub1(String s) {
        this.subMessage1 = s;
    }

    private void logMain(String s) {
        this.mainMessage = s;
    }

    public static boolean isDone() {
        return LoadingGui.done;
    }

    @Override
    public void make() {
        super.make();

        mainProgress = new ProgressMessenger(messengerMain, 12);
    }

    public ProgressMessenger createSubProgress(int size) {
        return new ProgressMessenger(messengerSub, size);
    }
}
