package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.ingame.bubble.*;
import tk.ultreonteam.bubbles.ingame.effect.StatusEffectInstance;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;
import tk.ultreonteam.commons.util.ColorUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.IntRange;
import tk.ultreonteam.bubbles.ingame.bubble.*;

import java.awt.*;
import java.util.function.Supplier;

/**
 * Bubble Initialization
 * Bubble init, used for initialize bubbles.
 * For example, the {@link DefenseBoostBubble} instance is assigned here.
 *
 * @see AbstractBubble
 */
@SuppressWarnings("unused")
//@ObjectHolder(modId = "bubbleblaster")
public class Bubbles {
    private static final DelayedRegister<AbstractBubble> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.BUBBLES);

    // Bubbles
    public static final Registered<AbstractBubble> NORMAL = register("Normal", () -> AbstractBubble.builder()
            .priority(150_000_000L)
            .radius(new IntRange(12, 105))
            .speed(new DoubleRange(4, 8.7))
            .colors(Color.white)
            .score(1f)
            .build());
    public static final Registered<AbstractBubble> DOUBLE = register("Double", () -> AbstractBubble.builder()
            .priority(4_600_000L)
            .radius(new IntRange(24, 75))
            .speed(new DoubleRange(8, 17.4))
            .colors(Color.orange, Color.orange)
            .score(2f)
            .build());
    public static final Registered<AbstractBubble> TRIPLE = register("Triple", () -> AbstractBubble.builder()
            .priority(1_150_000L)
            .radius(new IntRange(48, 60))
            .speed(new DoubleRange(12, 38.8))
            .colors(Color.cyan, Color.cyan, Color.cyan)
            .score(3f)
            .build());
    public static final Registered<AbstractBubble> BOUNCY = register("Bouncy", () -> AbstractBubble.builder()
            .priority(715_000L)
            .radius(new IntRange(15, 85))
            .speed(new DoubleRange(5.215d, 8.845d))
            .score(1.5f)
            .colors(ColorUtils.parseColorString("#ff0000,#ff3f00,#ff7f00,#ffbf00")) // Color.decode("#ff0000"), Color.decode("#ff3f00"), Color.decode("#ff7f00"), Color.decode("#ffaf00"))
            .bounceAmount(10f)
            .build());
    public static final Registered<AbstractBubble> BUBBLE_FREEZE = register("BubbleFreeze", () -> AbstractBubble.builder()
            .priority(52_750L)
            .radius(new IntRange(17, 58))
            .speed(new DoubleRange(4.115d, 6.845d))
            .score(1.3f)
            .effect((source, target) -> (new StatusEffectInstance(Effects.BUBBLE_FREEZE.get(), source.getRadius() / 8, (byte) ((byte) source.getSpeed() * 4))))
            .colors(ColorUtils.parseColorString("#ff0000,#ff7f00,#ffff00,#ffff7f,#ffffff", false))
            .build());
    public static final Registered<AbstractBubble> PARALYZE = register("Paralyze", () -> AbstractBubble.builder()
            .priority(3_325_000L)
            .radius(new IntRange(28, 87))
            .speed(new DoubleRange(1.215d, 2.845d))
            .score(0.325f)
            .effect((source, target) -> (new StatusEffectInstance(Effects.PARALYZE.get(), source.getRadius() / 16, (byte) 1)))
            .colors(ColorUtils.parseColorString("#ffff00,#ffff5f,#ffffdf,#ffffff"))
            .build());
    //    public static final BubbleType DAMAGE_BUBBLE = new BubbleType.Builder().priority(8850000L).radius(new IntRange(15, 85)).speed(new DoubleRange(3.215d, 4.845d)).colors(Color.red, new Color(255, 63, 0), Color.red).attackMod(1d).build();
    public static final Registered<DamageBubble> DAMAGE = register("Damage", DamageBubble::new);

    public static final Registered<AbstractBubble> POISON = register("Poison", () -> AbstractBubble.builder()
            .priority(1_313_131L)
            .radius(new IntRange(34, 83))
            .speed(new DoubleRange(1.0d, 2.7d))
            .defense(0.225f)
            .attack(0.0f)
            .score(0.38f)
            .hardness(1.0d)
            .colors(ColorUtils.parseColorString("#7fff00,#9faf1f,#bf7f3f,#df3f5f,#ff007f")) // new Color[]{new Color(128, 255, 0), new Color(160, 192, 32), new Color(192, 128, 64), new Color(224, 64, 96), new Color(255, 0, 128)})
            .effect((source, target) -> (new StatusEffectInstance(Effects.POISON.get(), source.getRadius() / 8, 4)))
            .build());

    public static final Registered<HealBubble> HEAL = register("Heal", HealBubble::new);
    public static final Registered<UltraBubble> ULTRA = register("Ultra", UltraBubble::new);
    public static final Registered<LevelUpBubble> LEVEL_UP = register("LevelUp", LevelUpBubble::new);
    public static final Registered<HardenedBubble> HARDENED = register("Hardened", HardenedBubble::new);
    public static final Registered<BlindnessBubble> BLINDNESS = register("Blindness", BlindnessBubble::new);
    public static final Registered<AccelerateBubble> ACCELERATE = register("Accelerate", AccelerateBubble::new);
    public static final Registered<SpeedBoostBubble> SPEED_BOOST = register("SpeedBoost", SpeedBoostBubble::new);
    public static final Registered<DoubleStateBubble> DOUBLE_STATE = register("DoubleState", DoubleStateBubble::new);
    public static final Registered<TripleStateBubble> TRIPLE_STATE = register("TripleState", TripleStateBubble::new);
    public static final Registered<AttackBoostBubble> ATTACK_BOOST = register("Attack", AttackBoostBubble::new);
    public static final Registered<DefenseBoostBubble> DEFENSE_BOOST = register("Defense", DefenseBoostBubble::new);

    private static <T extends AbstractBubble> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
