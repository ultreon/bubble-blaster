package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.ingame.event.GameplayEvent;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.ingame.event.BloodMoonGameplayEvent;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;

import java.util.function.Supplier;

/**
 * @see GameplayEvent
 */
@SuppressWarnings("unused")
public class GameplayEvents {
    private static final DelayedRegister<GameplayEvent> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.GAME_EVENTS);

    // Bubbles
    public static final Registered<BloodMoonGameplayEvent> BLOOD_MOON_EVENT = register("BloodMoon", BloodMoonGameplayEvent::new);

    private static <T extends GameplayEvent> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
