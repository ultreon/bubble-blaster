package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.ingame.entity.BubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.BulletEntity;
import tk.ultreonteam.bubbles.ingame.entity.GiantBubbleEntity;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.entity.types.EntityType;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;

import java.util.function.Supplier;

//@ObjectHolder(modId = "bubbleblaster")
public class Entities {
    @SuppressWarnings("rawtypes")
    private static final DelayedRegister<EntityType> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.ENTITIES);

    public static final Registered<EntityType<BulletEntity>> AMMO = register("Ammo", () -> new EntityType<>(BulletEntity::new));
    public static final Registered<EntityType<BubbleEntity>> BUBBLE = register("Bubble", () -> new EntityType<>(BubbleEntity::new));
    public static final Registered<EntityType<GiantBubbleEntity>> GIANT_BUBBLE = register("GiantBubble", () -> new EntityType<>(GiantBubbleEntity::new));
    public static final Registered<EntityType<Player>> PLAYER = register("Player", () -> new EntityType<>(Player::new));

    @SuppressWarnings("SameParameterValue")
    private static <T extends EntityType<?>> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
