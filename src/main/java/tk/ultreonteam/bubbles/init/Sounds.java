package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.media.Sound;

import java.net.URISyntaxException;
import java.util.Objects;

public class Sounds {
    public static final Sound focusChangeSFX;

    static {
        try {
            focusChangeSFX = new Sound(Objects.requireNonNull(Sounds.class.getResource("/Assets/tk.ultreonteam.bubbles/Audio/sfx/ui/button/focus_change.mp3")), "focusChange");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
