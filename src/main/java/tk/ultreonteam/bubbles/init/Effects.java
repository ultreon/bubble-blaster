package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.ingame.effect.*;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;
import tk.ultreonteam.bubbles.ingame.effect.*;

import java.util.function.Supplier;

/**
 * Effect init, used for initialize effects (temporary effects for players / bubbles that do something overtime).
 * For example, the {@link DefenseBoostEffect} instance is assigned here.
 *
 * @see StatusEffect
 * @see DelayedRegister<StatusEffect>
 */
@SuppressWarnings("unused")
public class Effects {
    private static final DelayedRegister<StatusEffect> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.EFFECTS);

    public static final Registered<DefenseBoostEffect> DEFENSE_BOOST = register("Defense", DefenseBoostEffect::new);
    public static final Registered<BubbleFreezeEffect> BUBBLE_FREEZE = register("BubbleFreeze", BubbleFreezeEffect::new);
    public static final Registered<AttackBoostEffect> ATTACK_BOOST = register("Attack", AttackBoostEffect::new);
    public static final Registered<MultiScoreEffect> MULTI_SCORE = register("MultiScore", MultiScoreEffect::new);
    public static final Registered<SpeedBoostEffect> SPEED_BOOST = register("SpeedBoost", SpeedBoostEffect::new);
    public static final Registered<BlindnessEffect> BLINDNESS = register("Blindness", BlindnessEffect::new);
    public static final Registered<ParalyzeEffect> PARALYZE = register("Paralyze", ParalyzeEffect::new);
    public static final Registered<PoisonEffect> POISON = register("Poison", PoisonEffect::new);
    public static final Registered<LuckEffect> LUCK = register("Luck", LuckEffect::new);

    private static <T extends StatusEffect> Registered<T> register(String name, Supplier<T> effectSupplier) {
        return REGISTER.register(name, effectSupplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
