package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.ingame.entity.ammo.AmmoType;
import tk.ultreonteam.bubbles.ingame.entity.ammo.BasicAmmoType;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;

import java.util.function.Supplier;

/**
 * @see AmmoType
 */
public class AmmoTypes {
    private static final DelayedRegister<AmmoType> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.AMMO_TYPES);

    public static final Registered<BasicAmmoType> BASIC = register("Basic", BasicAmmoType::new);

    @SuppressWarnings("SameParameterValue")
    private static <T extends AmmoType> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
