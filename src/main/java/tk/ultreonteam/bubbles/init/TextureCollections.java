package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;
import tk.ultreonteam.bubbles.render.TextureCollection;

import java.util.function.Supplier;

/**
 * Initialization for texture collections.
 *
 * @see Registers#TEXTURE_COLLECTIONS
 * @since 1.0.924-a1
 */
public class TextureCollections {
    private static final DelayedRegister<TextureCollection> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.TEXTURE_COLLECTIONS);

    public static final Registered<TextureCollection> BUBBLE_TEXTURES = register("Bubble", TextureCollection::new);

    @SuppressWarnings("SameParameterValue")
    private static <T extends TextureCollection> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents eventBus) {
        REGISTER.register(eventBus);
    }
}
