package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.ingame.gamemode.ClassicMode;
import tk.ultreonteam.bubbles.ingame.gamemode.Gamemode;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;

import java.util.function.Supplier;

/**
 * @author Qboi
 * @see Gamemode
 * @since 1.0.0
 */
@SuppressWarnings("unused")
//@ObjectHolder(modId = "bubbleblaster", type = GameType.class)
public class Gamemodes {
    private static final DelayedRegister<Gamemode> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.GAME_TYPES);

    public static final Registered<ClassicMode> CLASSIC = register("Classic", ClassicMode::new);

    @SuppressWarnings("SameParameterValue")
    private static <T extends Gamemode> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
