package tk.ultreonteam.bubbles.init;

import tk.ultreonteam.bubbles.InternalMod;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.TeleportAbility;
import tk.ultreonteam.bubbles.ingame.entity.player.ability.AbilityType;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.registry.DelayedRegister;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.registry.object.Registered;

import java.util.function.Supplier;

@SuppressWarnings({"rawtypes", "unused", "SameParameterValue"})
public class Abilities {
    private static final DelayedRegister<AbilityType> REGISTER = DelayedRegister.create(InternalMod.MOD_ID, Registers.ABILITIES);
    public static final Registered<AbilityType<TeleportAbility>> TELEPORT_ABILITY = register("Teleport", () -> new AbilityType<>(TeleportAbility::new));

    private static <T extends AbilityType<?>> Registered<T> register(String name, Supplier<T> supplier) {
        return REGISTER.register(name, supplier);
    }

    /**
     * <b>DO NOT CALL, THIS IS CALLED INTERNALLY</b>
     */
    public static void register(GameEvents events) {
        REGISTER.register(events);
    }
}
