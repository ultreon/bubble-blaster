package tk.ultreonteam.bubbles;

import tk.ultreonteam.bubbles.common.random.PseudoRandom;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.entity.EntityCollisionEvent;
import tk.ultreonteam.bubbles.event.input.MouseClickEvent;
import tk.ultreonteam.bubbles.event.type.MouseEventType;
import tk.ultreonteam.bubbles.ingame.entity.ActiveEntity;
import tk.ultreonteam.bubbles.ingame.entity.Entity;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.bubbles.ingame.entity.damage.DamageSourceType;
import tk.ultreonteam.bubbles.ingame.entity.damage.EntityDamageSource;
import tk.ultreonteam.bubbles.ingame.entity.player.Player;
import tk.ultreonteam.bubbles.ingame.gamemode.Gamemode;
import tk.ultreonteam.bubbles.input.KeyInput;
import tk.ultreonteam.bubbles.media.Sound;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.screen.CommandScreen;
import tk.ultreonteam.bubbles.render.screen.MessengerScreen;
import tk.ultreonteam.bubbles.save.GameSave;
import tk.ultreonteam.bubbles.util.GraphicsUtils;
import tk.ultreonteam.bubbles.util.Util;
import tk.ultreonteam.commons.util.ShapeUtils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * Loaded game save.
 *
 * @author Qboi
 * @since 0.1.0
 */
public class LoadedGame {

    // Fonts.
    private final Font defaultFont = new Font(Util.getGame().getPixelFontName(), Font.PLAIN, 32);

    private static final BubbleBlaster game = BubbleBlaster.getInstance();

    // Types
    private final Gamemode gamemode;

    private final Environment environment;

    // Threads.
    private Thread autoSaveThread;
    private Thread collisionThread;
    private Thread ambientAudioThread;
    private Thread gameEventHandlerThread;

    // Files / folders.
    private final File saveDir;

    // Active messages.
    private final ArrayList<String> activeMessages = new ArrayList<>();
    private final ArrayList<Long> activeMsgTimes = new ArrayList<>();

    // Audio.
    private volatile Sound ambientAudio;
    private long nextAudio;

    // Flags.
    @SuppressWarnings("FieldCanBeLocal")
    private boolean gameActive = false;

    // Save
    private final GameSave gameSave;

    private final AutoSaver autoSaver = new AutoSaver(this);

    /**
     * Constructor for the loaded game save.
     *
     * @since 0.1.0
     * @author Qboi
     */
    public LoadedGame(GameSave gameSave, Environment environment) {
        this.gameSave = gameSave;
        this.gamemode = environment.getGamemode();
        this.environment = environment;
        this.saveDir = gameSave.getDirectory();
    }

    //***********************//
    //     Show and Hide     //
    //***********************//
    /**
     * Run the initialization steps for the loaded game.
     */
    private void run() {
        Util.setCursor(BubbleBlaster.getInstance().getBlankCursor());

        activate();

        this.environment.getGamemode().start();

        autoSaver.start();

        this.collisionThread = new Thread(this::collisionLoop, "Collision");
        this.collisionThread.setDaemon(true);
        this.collisionThread.start();

        this.ambientAudioThread = new Thread(this::audioLoop, "Audio-Thread");
        this.ambientAudioThread.setDaemon(true);
        this.ambientAudioThread.start();
    }

    /**
     * Quit the loaded game.
     * For proper exit, call {@link BubbleBlaster#quitLoadedGame()} instead.
     *
     * @since 0.1.0
     * @author Qboi
     */
    public void quit() {
        game.showScreen(new MessengerScreen("Exiting game environment."));

        game.environment = null;

        // Unbind events.
        gamemode.destroy();
        deactivate();

        ambientAudio.stop();

        if (autoSaveThread != null) autoSaveThread.interrupt();
        if (collisionThread != null) collisionThread.interrupt();
        if (ambientAudioThread != null) ambientAudioThread.interrupt();
        if (gameEventHandlerThread != null) gameEventHandlerThread.interrupt();

        environment.quit();

        // Hide cursor.
        Util.setCursor(BubbleBlaster.getInstance().getDefaultCursor());

        System.gc();
    }

    //************************//
    //     Thread methods     //
    //************************//
    private void audioLoop() {
        // TODO: Rework this with proper API support.

        // Main audio loop.
        while (this.gameActive) {
            // Check if we need to play a new background music track.
            if (this.ambientAudio == null) {
                if (!this.environment.isBloodMoonActive() && this.nextAudio < System.currentTimeMillis()) {
                    if (new PseudoRandom(System.nanoTime()).getNumber(0, 5, -1) == 0) {
                        try {
                            this.ambientAudio = new Sound(Objects.requireNonNull(getClass().getResource("/Assets/tk.ultreonteam.bubbles/Audio/BGM/ArtOfSilence.mp3")), "artOfSilence");
                            this.ambientAudio.setVolume(0.15d);
                            this.ambientAudio.play();
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                            BubbleBlaster.getInstance().shutdown();
                        }
                    } else {
                        this.nextAudio = System.currentTimeMillis() + new Random().nextLong(1000, 2000);
                    }
                } else if (this.environment.isBloodMoonActive()) {
                    try {
                        this.ambientAudio = new Sound(Objects.requireNonNull(getClass().getResource("/Assets/tk.ultreonteam.bubbles/Audio/BGM/BloodMoonState.mp3")), "bloodMoonState");
                        this.ambientAudio.setVolume(0.25d);
                        this.ambientAudio.play();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                        BubbleBlaster.getInstance().shutdown();
                    }
                }
            } else if (this.ambientAudio.isStopped()) {
                this.ambientAudio = null;
            } else if (!this.ambientAudio.getClip().isPlaying()) {
                this.ambientAudio.stop();
                this.ambientAudio = null;
            } else if (this.environment.isBloodMoonActive() && !this.ambientAudio.getName().equals("bloodMoonState")) {
                this.ambientAudio.stop();
                this.ambientAudio = null;

                try {
                    this.ambientAudio = new Sound(Objects.requireNonNull(getClass().getResource("/Assets/tk.ultreonteam.bubbles/Audio/BGM/BloodMoonState.mp3")), "bloodMoonState");
                    this.ambientAudio.setVolume(0.25d);
                    this.ambientAudio.play();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    BubbleBlaster.getInstance().shutdown();
                }
            }
        }
    }

    private void collisionLoop() {
        // Initiate variables for game loop.
        long lastTime = System.nanoTime();
        double amountOfUpdates = 30.0;
        double ns = 1000000000 / amountOfUpdates;
        double delta;

        while (this.gameActive) {
            // Calculate tick delta-time.
            long now = System.nanoTime();
            delta = (now - lastTime) / ns;
            lastTime = now;

            java.util.List<Entity> entities = this.environment.getEntities();

            List<Entity> loopingEntities = new ArrayList<>(entities);

            if (environment.isInitialized()) {
                if (!BubbleBlaster.isPaused()) {
                    for (int a = 0; a < loopingEntities.size(); a++)
                        for (int b = a + 1; b < loopingEntities.size(); b++) {
                            Entity entityA = loopingEntities.get(a);
                            Entity entityB = loopingEntities.get(b);

                            if (!entityA.isCollidableWith(entityB) && !entityB.isCollidableWith(entityA)) {
                                continue;
                            }

                            try {
                                // Check intersection.
                                if (ShapeUtils.checkIntersection(entityA.getShape(), entityB.getShape())) {
                                    if (entityA.isCollidableWith(entityB)) {
                                        // Handling collision by posting collision event, and let the intersected entities attack each other.
                                        BubbleBlaster.getEventBus().publish(new EntityCollisionEvent(BubbleBlaster.getInstance(), delta, entityA, entityB));
                                        entityA.onCollision(entityB, delta);
                                    }

                                    if (entityB.isCollidableWith(entityA)) {
                                        BubbleBlaster.getEventBus().publish(new EntityCollisionEvent(BubbleBlaster.getInstance(), delta, entityB, entityA));
                                        entityB.onCollision(entityA, delta);
                                    }

                                    if (entityA instanceof ActiveEntity && entityB.doesAttack(entityA) && entityA.canBeAttackedBy(entityB)) {
                                        ((ActiveEntity) entityA).damage(entityB.getAttributes().getValue(Attribute.ATTACK) * delta / entityA.getAttributes().getBase(Attribute.DEFENSE), new EntityDamageSource(entityB, DamageSourceType.COLLISION));
                                    }

                                    if (entityB instanceof ActiveEntity && entityA.doesAttack(entityB) && entityB.canBeAttackedBy(entityA)) {
                                        ((ActiveEntity) entityB).damage(entityA.getAttributes().getValue(Attribute.ATTACK) * delta / entityB.getAttributes().getBase(Attribute.DEFENSE), new EntityDamageSource(entityA, DamageSourceType.COLLISION));
                                    }
                                }
                            } catch (ArrayIndexOutOfBoundsException exception) {
                                BubbleBlaster.getLogger().info("Array index was out of bounds! Check check double check!");
                            }
                        }
                }
            }

            try {
                Thread.sleep(8); // Sleep for 8ms to avoid CPU usage.
            } catch (InterruptedException ignored) {
                gameActive = false;
            }
        }

        BubbleBlaster.getLogger().info("Collision thread will die now.");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Event Binding     //
    ///////////////////////////
    public void activate() {
        BubbleBlaster.getEventBus().subscribe(this);
        this.gameActive = true;
    }

    public void deactivate() {
        this.gameActive = false;
    }

    public boolean isGameActive() {
        return this.gameActive;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Getter-Only Properties     //
    ////////////////////////////////////
    public Sound getAmbientAudio() {
        return this.ambientAudio;
    }

    public File getSaveDir() {
        return this.saveDir;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Trigger Methods     //
    /////////////////////////////
    public void receiveMessage(String s) {
        this.activeMessages.add(s);
        this.activeMsgTimes.add(System.currentTimeMillis() + 3000);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Render Methods     //
    ////////////////////////////

    /**
     * Renders the game, such as the HUD, or environment.
     * Should not being called, for internal use only.
     *
     * @param game the game instance.
     * @param gfx  a 2D graphics instance.
     */
    @Deprecated(forRemoval = true, since = "0.0.3047")
    public void render(BubbleBlaster game, Renderer gfx) {
        if (this.environment.isInitialized()) {
            this.gamemode.render(gfx);
        }
        this.renderHUD(game, gfx);
    }

    /**
     * Renders the hud, in this method only the system and chat messages.
     * Should not being called, for internal use only.
     *
     * @param game the game instance.
     * @param gfx  a 2D graphics instance.
     */
    public void renderHUD(@SuppressWarnings({"unused", "RedundantSuppression"}) BubbleBlaster game, Renderer gfx) {
        int i = 0;
        for (String s : activeMessages) {
            Renderer gg2 = gfx.frame(0, 71 + (32 * i), 1000, 32);

            gg2.color(new Color(0, 0, 0, 128));
            gg2.rect(0, 0, 1000, 32);

            gg2.color(new Color(255, 255, 255, 255));
            GraphicsUtils.drawLeftAnchoredString(gg2, s, new Point2D.Double(2, 2), 28, defaultFont);

            gg2.dispose();
            i++;
        }

        for (i = 0; i < this.activeMessages.size(); i++) {
            if (this.activeMsgTimes.get(i) < System.currentTimeMillis()) {
                this.activeMsgTimes.remove(i);
                this.activeMessages.remove(i);
                i--;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Events     //
    ////////////////////

    /**
     * Keyboard Event-Handler
     * Keyboard event-handler, handles screen changing and debug-mode activations.
     */
    public void onKeyPress(int keyCode, int scanCode, int modifiers) {
        if (keyCode == KeyInput.Map.KEY_SLASH && game.hasScreenOpen()) {
            BubbleBlaster.getInstance().showScreen(new CommandScreen());
        }
    }

    public void onKeyHold(int keyCode, int scanCode, int modifiers) {
        Player player = Objects.requireNonNull(this.environment.getPlayer());
        if (BubbleBlaster.isDevMode()) switch (keyCode) {
            case KeyInput.Map.KEY_F1 -> this.environment.triggerBloodMoon();
            case KeyInput.Map.KEY_F3 -> player.destroy();
            case KeyInput.Map.KEY_F4 -> player.levelUp();
            case KeyInput.Map.KEY_F5 -> player.addScore(1000);
            case KeyInput.Map.KEY_F6 -> {
                player.setDamageValue(player.getMaxDamageValue());
                player.healToMax();
            }
        }
    }

    @SubscribeEvent
    public void onMouse(MouseClickEvent evt) {
        if (evt.getType() == MouseEventType.CLICK) {
            if (evt.getButton() == 1) {
                if (KeyInput.isDown(KeyInput.Map.KEY_F1)) {
                    Objects.requireNonNull(this.gamemode.getPlayer()).teleport(evt.getPos().d());
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //     Utility methods     //
    /////////////////////////////

    @Deprecated
    public void tick() {
        this.gamemode.tick(environment);
    }

    public void start() {
        BubbleBlaster.runOnMainThread(this::run);
    }

    public Gamemode getGamemode() {
        return gamemode;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public GameSave getGameSave() {
        return gameSave;
    }
}
