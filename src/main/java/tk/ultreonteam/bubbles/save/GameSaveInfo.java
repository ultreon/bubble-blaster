package tk.ultreonteam.bubbles.save;

import tk.ultreonteam.bubbles.common.Identifier;
import tk.ultreonteam.bubbles.ingame.gamemode.Gamemode;
import tk.ultreonteam.bubbles.init.Gamemodes;
import tk.ultreonteam.bubbles.registry.Registers;
import net.querz.nbt.tag.CompoundTag;

public class GameSaveInfo {
    private final String name;
    private final long savedTime;
    private final long highScore;
    private final Gamemode gamemode;
    private final long seed;

    public GameSaveInfo(CompoundTag tag) {
        this.name = tag.getString("name");
        this.savedTime = tag.getLong("savedTime");
        this.highScore = tag.getLong("highScore");
        this.seed = tag.getLong("seed");
        this.gamemode = Registers.GAME_TYPES.get(Identifier.tryParse(tag.getString("gamemode", Gamemodes.CLASSIC.id().toString())));
    }

    public String getName() {
        return name;
    }

    public long getSavedTime() {
        return savedTime;
    }

    public long getHighScore() {
        return highScore;
    }

    public Gamemode getGamemode() {
        return gamemode;
    }

    public long getSeed() {
        return seed;
    }
}
