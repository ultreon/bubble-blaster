package tk.ultreonteam.bubbles.common;

import net.querz.nbt.tag.CompoundTag;

public interface TagHolder {
    CompoundTag getTag();
}
