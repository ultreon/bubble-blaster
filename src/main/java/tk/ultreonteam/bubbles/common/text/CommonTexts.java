package tk.ultreonteam.bubbles.common.text;

public class CommonTexts {
    public static final TextObject EMPTY = new LiteralText("");
    public static final TextObject TRUE = new TranslationText("tk.ultreonteam.bubbles/Other/True");
    public static final TextObject FALSE = new TranslationText("tk.ultreonteam.bubbles/Other/False");
    public static final TextObject NULL = new TranslationText("tk.ultreonteam.bubbles/Other/Null");
    public static final TextObject UNDEFINED = new TranslationText("tk.ultreonteam.bubbles/Other/Undefined");
    public static final TextObject NAN = new TranslationText("tk.ultreonteam.bubbles/Other/NaN");
    public static final TextObject UNKNOWN_TYPE = new TranslationText("tk.ultreonteam.bubbles/Other/UnknownType");
    public static final TextObject UNKNOWN = new TranslationText("tk.ultreonteam.bubbles/Other/Unknown");
    public static final TextObject ENABLE = new TranslationText("tk.ultreonteam.bubbles/Other/Enable");
    public static final TextObject DISABLE = new TranslationText("tk.ultreonteam.bubbles/Other/Disable");
    public static final TextObject DEFAULT = new TranslationText("tk.ultreonteam.bubbles/Other/Default");
    public static final TextObject ENABLED = new TranslationText("tk.ultreonteam.bubbles/Other/Enabled");
    public static final TextObject DISABLED = new TranslationText("tk.ultreonteam.bubbles/Other/Disabled");
    public static final TextObject DEFAULTED = new TranslationText("tk.ultreonteam.bubbles/Other/Defaulted");
    public static final TextObject ON = new TranslationText("tk.ultreonteam.bubbles/Other/On");
    public static final TextObject OFF = new TranslationText("tk.ultreonteam.bubbles/Other/Off");
    public static final TextObject YES = new TranslationText("tk.ultreonteam.bubbles/Other/Yes");
    public static final TextObject NO = new TranslationText("tk.ultreonteam.bubbles/Other/No");
    public static final TextObject OK = new TranslationText("tk.ultreonteam.bubbles/Other/Ok");
    public static final TextObject APPLY = new TranslationText("tk.ultreonteam.bubbles/Other/Apply");
    public static final TextObject CANCEL = new TranslationText("tk.ultreonteam.bubbles/Other/Cancel");
    public static final TextObject CLOSE = new TranslationText("tk.ultreonteam.bubbles/Other/Close");
    public static final TextObject BACK = new TranslationText("tk.ultreonteam.bubbles/Other/Back");
    public static final TextObject NEXT = new TranslationText("tk.ultreonteam.bubbles/Other/Next");
    public static final TextObject PREVIOUS = new TranslationText("tk.ultreonteam.bubbles/Other/Prev");
}
