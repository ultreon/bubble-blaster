package tk.ultreonteam.bubbles.common.text.translation;

public interface Translatable {
    String getTranslationPath();
}
