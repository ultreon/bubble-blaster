package tk.ultreonteam.bubbles.common.random;

import tk.ultreonteam.bubbles.common.EntityPos;
import tk.ultreonteam.bubbles.ingame.Environment;

import java.awt.geom.Rectangle2D;

/**
 * Abstract Entity Randomizer.
 */
public abstract class EntityRandomizer {
    public abstract EntityPos getRandomProperties(Rectangle2D bounds, long spawnIndex, int retry, Environment gameType);
}
