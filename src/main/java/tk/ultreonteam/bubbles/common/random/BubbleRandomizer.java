package tk.ultreonteam.bubbles.common.random;

import tk.ultreonteam.bubbles.ingame.bubble.AbstractBubble;
import tk.ultreonteam.bubbles.ingame.bubble.BubbleProperties;
import tk.ultreonteam.bubbles.ingame.entity.bubble.BubbleSystem;
import tk.ultreonteam.bubbles.ingame.Environment;
import tk.ultreonteam.commons.annotation.FieldsAreNonnullByDefault;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
import java.awt.geom.Rectangle2D;

/**
 * Bubble randomizer class.
 *
 * @author Qboi
 */
@SuppressWarnings("unused")
@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
@FieldsAreNonnullByDefault
public class BubbleRandomizer extends EntityRandomizer {
    private final PseudoRandom random;
    private final Rng variantRng;
    private final Rng hardnessRng;
    private final Rng speedRng;
    private final Rng radiusRng;
    private final Rng xRng;
    private final Rng yRng;
    private final Rng defenseRng;
    private final Rng attackRng;
    private final Rng scoreMultiplierRng;
    private final Rng bubbleRng;

    /**
     * Create bubble randomizer instance.
     *
     * @param environment the game-type to assign the randomizer to.
     * @param rng         the rng to randomize with.
     */
    public BubbleRandomizer(Environment environment, Rng rng) {
        this.random = new PseudoRandom(rng.getNumber(Integer.MIN_VALUE, (long) Integer.MAX_VALUE, 1));
        this.variantRng = new Rng(this.random, 0, 0);
        this.hardnessRng = new Rng(this.random, 0, 1);
        this.speedRng = new Rng(this.random, 0, 2);
        this.radiusRng = new Rng(this.random, 0, 3);
        this.xRng = new Rng(this.random, 0, 4);
        this.yRng = new Rng(this.random, 0, 5);
        this.defenseRng = new Rng(this.random, 0, 6);
        this.attackRng = new Rng(this.random, 0, 7);
        this.scoreMultiplierRng = new Rng(this.random, 0, 8);
        this.bubbleRng = new Rng(this.random, 0, -1);
    }

    /**
     * Get a random bubble properties instance.
     *
     * @param bounds      game bounds.
     * @param environment game type.
     * @return a random bubble properties instance.
     */
    @Override
    public BubbleProperties getRandomProperties(Rectangle2D bounds, long spawnIndex, int retry, Environment environment) {
        AbstractBubble type = BubbleSystem.random(variantRng, spawnIndex, retry, environment);

        // Properties
        double minHardness = type.getHardness();
        double maxHardness = type.getHardness();
        double minSpeed = type.getMinSpeed();
        double maxSpeed = type.getMaxSpeed();
        int minRad = type.getMinRadius();
        int maxRad = type.getMaxRadius();

        // Randomizing.
        double hardness = hardnessRng.getNumber(minHardness, maxHardness, spawnIndex, retry);
        double speed = speedRng.getNumber(minSpeed, maxSpeed, spawnIndex, retry);
        int radius = radiusRng.getNumber(minRad, maxRad, spawnIndex, retry);

        if (bounds.getMinX() > bounds.getMaxX() || bounds.getMinY() > bounds.getMaxY()) {
            throw new IllegalStateException("Game bounds is invalid: negative size");
        }

        if (bounds.getMinX() == bounds.getMaxX() || bounds.getMinY() == bounds.getMaxY()) {
            throw new IllegalStateException("Game bounds is invalid: zero size");
        }

        int x = xRng.getNumber((int) bounds.getMinX(), (int) bounds.getMaxX(), spawnIndex, retry);
        int y = yRng.getNumber((int) bounds.getMinY(), (int) bounds.getMaxY(), spawnIndex, retry);

        int rad = radius + (type.colors.length * 3) + 4;
        float defense = type.getDefense(environment, defenseRng);
        float attack = type.getAttack(environment, attackRng);
        float score = type.getScore(environment, scoreMultiplierRng);

        return new BubbleProperties(type, radius, speed, rad, x, y, defense, attack, score, bubbleRng);
    }

    public PseudoRandom getRNG() {
        return random;
    }

    public Rng getHardnessRng() {
        return hardnessRng;
    }

    public Rng getSpeedRng() {
        return speedRng;
    }

    public Rng getRadiusRnd() {
        return radiusRng;
    }

    public Rng getXRng() {
        return xRng;
    }

    public Rng getYRng() {
        return yRng;
    }

    public Rng getDefenseRng() {
        return defenseRng;
    }

    public Rng getAttackRng() {
        return attackRng;
    }

    public Rng getScoreMultiplierRng() {
        return scoreMultiplierRng;
    }

    public Rng getVariantRng() {
        return variantRng;
    }
}
