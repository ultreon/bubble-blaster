package tk.ultreonteam.bubbles.common;

public interface TypedTickable<T> {
    void tick(T type);
}
