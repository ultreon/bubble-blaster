package tk.ultreonteam.bubbles.common;

import tk.ultreonteam.bubbles.common.holders.ListDataHolder;
import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.commons.function.primitive.BiDouble2DoubleFunction;
import tk.ultreonteam.commons.function.primitive.Double2DoubleFunction;
import tk.ultreonteam.commons.map.OrderedHashMap;
import net.querz.nbt.tag.CompoundTag;
import net.querz.nbt.tag.ListTag;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@SuppressWarnings({"DuplicatedCode", "unused"})
public class AttributeContainer implements ListDataHolder<CompoundTag> {
    private final HashMap<Attribute, Double> map = new HashMap<>();
    private final OrderedHashMap<Identifier, AttributeModifier> modifiers = new OrderedHashMap<>();

    public AttributeContainer() {

    }

    public void setBase(Attribute attribute, double value) {
        this.map.put(attribute, value);
    }

    public double getBase(Attribute attribute) {
        @Nullable Double value = this.map.get(attribute);
        if (value == null) {
            throw new NoSuchElementException("Attribute \"" + attribute.name() + "\" has no set base value.");
        }
        return value;
    }

    public void addModifier(AttributeModifier modifier) {
        this.modifiers.put(modifier.id(), modifier);
    }

    public void removeModifier(AttributeModifier modifier) {
        this.modifiers.remove(modifier.id());
    }

    public void removeModifier(Identifier id) {
        this.modifiers.remove(id);
    }

    /**
     * Get the value of the attribute.
     * The value is calculated by applying all modifiers to the base value.
     *
     * @param attribute the attribute to get the value of.
     */
    public double getValue(Attribute attribute) {
        double value = getBase(attribute);
        for (AttributeModifier modifier : this.modifiers.values()) {
            if (modifier.attribute() == attribute) {
                value = modifier.apply(value);
            }
        }
        return value;
    }

    /**
     * @deprecated Use {@link #getValue(Attribute)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void getModified(Attribute attribute, BiDouble2DoubleFunction function, List<AttributeContainer> attributeMaps) {
        if (!this.map.containsKey(attribute)) {
            throw new NoSuchElementException("Attribute \"" + attribute.name() + "\" has no set base value.");
        }

        double f = getBase(attribute);
        for (AttributeContainer map : attributeMaps) {
            f *= function.apply(f, map.getBase(attribute));
        }
    }

    /**
     * @deprecated Use {@link #getValue(Attribute)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void getModified(Attribute attribute, BiDouble2DoubleFunction function, AttributeContainer... attributeMaps) {
        if (!this.map.containsKey(attribute)) {
            throw new NoSuchElementException("Attribute \"" + attribute.name() + "\" has no set base value.");
        }

        double f = getBase(attribute);
        for (AttributeContainer map : attributeMaps) {
            f *= function.apply(f, map.getBase(attribute));
        }
    }

    /**
     * @deprecated Use {@link #getValue(Attribute)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void getModified(Attribute attribute, List<Double2DoubleFunction> functions) {
        if (!this.map.containsKey(attribute)) {
            throw new NoSuchElementException("Attribute \"" + attribute.name() + "\" has no set base value.");
        }

        double f = getBase(attribute);
        for (Double2DoubleFunction function : functions) {
            f *= function.apply(f);
        }
    }

    /**
     * @deprecated Use {@link #getValue(Attribute)} instead.
     */
    @Deprecated(since = "0.1.0", forRemoval = true)
    public void getModified(Attribute attribute, Double2DoubleFunction... functions) {
        if (!this.map.containsKey(attribute)) {
            throw new NoSuchElementException("Attribute \"" + attribute.name() + "\" has no set base value.");
        }

        double f = getBase(attribute);
        for (Double2DoubleFunction function : functions) {
            f *= function.apply(f);
        }
    }

    public ListTag<CompoundTag> save() {
        ListTag<CompoundTag> list = new ListTag<>(CompoundTag.class);
        for (Map.Entry<Attribute, Double> entry : this.map.entrySet()) {
            CompoundTag tag = new CompoundTag();
            tag.putString("name", entry.getKey().name());
            tag.putDouble("value", entry.getValue());

            CompoundTag modifiers = new CompoundTag();
            for (AttributeModifier modifier : this.modifiers.values()) {
                if (modifier.attribute() == entry.getKey()) {
                    modifiers.put(modifier.id().toString(), modifier.save());
                }
            }

            list.add(tag);
        }
        return list;
    }

    public void load(ListTag<CompoundTag> list) {
        for (CompoundTag tag : list) {
            Attribute key = Attribute.fromName(tag.getString("name"));
            if (key == null) continue;
            double value = tag.getDouble("value");

            this.map.put(key, value);
        }
    }

    public void setAll(AttributeContainer defaultAttributes) {
        this.map.putAll(defaultAttributes.map);
    }

    public void clearModifiers() {
        this.modifiers.clear();
    }
}
