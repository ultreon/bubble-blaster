package tk.ultreonteam.bubbles.common;

import tk.ultreonteam.commons.annotation.FieldsAreNonnullByDefault;
import tk.ultreonteam.commons.annotation.MethodsReturnNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;

@FieldsAreNonnullByDefault
@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
public interface Translatable {
    String translationPath();
}
