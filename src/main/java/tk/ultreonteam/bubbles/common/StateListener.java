package tk.ultreonteam.bubbles.common;

public interface StateListener {
    void make();

    void destroy();
}
