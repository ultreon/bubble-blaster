package tk.ultreonteam.bubbles.common;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public record MethodCall(@Nullable Object instance, @NonNull Method method) {
    public MethodCall(@Nullable Object instance, @NonNull Method method) {
        this.instance = instance;
        this.method = method;
    }

    @Nullable
    public Object invoke(@NonNull Object... args) throws InvocationTargetException, IllegalAccessException {
        return method.invoke(instance, args);
    }

    public boolean isStaticCall() {
        return instance == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MethodCall that = (MethodCall) o;

        if (instance() != null ? !instance().equals(that.instance()) : that.instance() != null)
            return false;
        return method().equals(that.method());
    }

    @Override
    public int hashCode() {
        int result = instance() != null ? instance().hashCode() : 0;
        result = 31 * result + method().hashCode();
        return result;
    }
}
