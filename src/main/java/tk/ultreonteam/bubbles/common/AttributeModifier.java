package tk.ultreonteam.bubbles.common;

import tk.ultreonteam.bubbles.ingame.entity.attribute.Attribute;
import tk.ultreonteam.commons.function.primitive.BiDouble2DoubleFunction;
import net.querz.nbt.tag.CompoundTag;

/**
 * Modifier for an attribute, used to modify the value of an attribute.
 * 
 * @param id the id of the modifier (used to remove it).
 * @param attribute the attribute to modify.
 * @param value the value to apply to the attribute.
 * @param priority the priority of the modifier.
 * @param mode the calculation mode of the modifier, like add or multiply.
 */
public record AttributeModifier(Identifier id, Attribute attribute, double value, int priority, Mode mode) {
    public double apply(double base) {
        return mode.function.apply(base, value);
    }

    /**
     * Save this modifier to an NBT tag.
     * 
     * @return the saved tag
     */
    public CompoundTag save() {
        CompoundTag tag = new CompoundTag();
        tag.put("id", id.serialize());
        tag.putString("attribute", attribute.name());
        tag.putDouble("value", value);
        tag.putInt("priority", priority);
        tag.putInt("mode", mode.getId());
        return tag;
    }

    /**
     * The calculation mode of an attribute modifier.
     * 
     * @author Qboi
     * @since 0.1.0
     * @see AttributeModifier#apply(double)
     */
    enum Mode {
        ADD("add", 0, (a, b) -> a + b),
        MULTIPLY("multiply", 1, (a, b) -> a * b);

        private final String name;
        private final int id;
        private final BiDouble2DoubleFunction function;

        Mode(String name, int id, BiDouble2DoubleFunction function) {
            this.name = name;
            this.id = id;
            this.function = function;
        }

        /**
         * Get the name of the mode.
         *
         * @return the name of the mode.
         * @since 0.1.0
         * @author Qboi123
         */
        public String getName() {
            return name;
        }

        /**
         * Get the ID of the mode.
         *
         * @return the ID of the mode.
         * @since 0.1.0
         * @author Qboi123
         */
        public int getId() {
            return id;
        }

        /**
         * Get the calculation mode by name.
         *
         * @return the calculation mode.
         * @since 0.1.0
         * @author Qboi123
         */
        public static Mode fromName(String name) {
            for (Mode mode : Mode.values()) {
                if (mode.getName().equals(name)) {
                    return mode;
                }
            }
            throw new IllegalArgumentException("No mode with name \"" + name + "\" found.");
        }

        /**
         * Get the calculation mode by ID.
         *
         * @return the calculation mode.
         * @since 0.1.0
         * @author Qboi123
         */
        public static Mode fromId(int id) {
            for (Mode mode : Mode.values()) {
                if (mode.getId() == id) {
                    return mode;
                }
            }
            throw new IllegalArgumentException("No mode with id \"" + id + "\" found.");
        }
    }
}
