package tk.ultreonteam.bubbles.common;

public interface IRegistrable {
    Identifier id();

    void setId(Identifier rl);
}
