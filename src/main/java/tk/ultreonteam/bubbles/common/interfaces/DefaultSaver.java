package tk.ultreonteam.bubbles.common.interfaces;

import net.querz.nbt.tag.CompoundTag;

public interface DefaultSaver {
    CompoundTag saveDefaults();
}
