package tk.ultreonteam.bubbles.common.interfaces;

import tk.ultreonteam.bubbles.event.SubscribeEvent;

@FunctionalInterface
public interface EventConsumer<T> {
    @SubscribeEvent
    void accept(T evt);
}
