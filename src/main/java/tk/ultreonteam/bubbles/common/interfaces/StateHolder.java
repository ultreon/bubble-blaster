package tk.ultreonteam.bubbles.common.interfaces;

import net.querz.nbt.tag.CompoundTag;
import org.checkerframework.checker.nullness.qual.NonNull;

public interface StateHolder {
    @NonNull
    CompoundTag save();

    void load(CompoundTag tag);
}
