package tk.ultreonteam.bubbles.common.interfaces;

public interface NamespaceHolder {
    String getId();

    void setNamespace(String namespace);
}
