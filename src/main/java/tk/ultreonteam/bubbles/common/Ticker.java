package tk.ultreonteam.bubbles.common;

import org.checkerframework.common.value.qual.IntRange;

/**
 * Ticker, used to count ticks in {@code tick()} methods.
 *
 * @author Qboi
 */
public class Ticker {
    @IntRange(from = 0)
    private int ticks;

    public Ticker() {

    }

    /**
     * Advance 1 tick.
     *
     * @return the new tick count.
     */
    @IntRange(from = 1)
    public int advance() {
        return ++ticks;
    }

    /**
     * Get the current tick count.
     *
     * @return current tick count.
     */
    @IntRange(from = 0)
    public int get() {
        return ticks;
    }

    /**
     * Reset tick count to 0.
     */
    public void reset() {
        ticks = 0;
    }
}
