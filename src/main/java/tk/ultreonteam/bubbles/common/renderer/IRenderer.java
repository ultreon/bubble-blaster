package tk.ultreonteam.bubbles.common.renderer;

import tk.ultreonteam.bubbles.render.Renderer;

public interface IRenderer {
    void render(Renderer gg);
}
