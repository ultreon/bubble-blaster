package tk.ultreonteam.bubbles.common;

import tk.ultreonteam.bubbles.event.AbstractEvent;

import java.lang.reflect.InvocationTargetException;

@FunctionalInterface
public interface EventSubscriber<T extends AbstractEvent> {
    void handle(T event);

    @SuppressWarnings("unchecked")
    default void handle0(AbstractEvent event) throws ClassCastException {
        handle((T) event);
    }

    static EventSubscriber<?> ofCall(MethodCall call) {
        return event -> {
            try {
                call.invoke(event);
            } catch (InvocationTargetException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        };
    }
}
