package tk.ultreonteam.bubbles.common;

import com.google.common.annotations.Beta;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Not yet used.
 */
@Beta
public enum ResourceType {
    ASSETS("Assets"),
    DATA("Data"),
    META_INF("META_INF"),
    OBJECT(null);

    @Nullable
    private final String path;

    ResourceType(@Nullable String path) {
        this.path = path;
    }

    @Nullable
    public String getPath() {
        return path;
    }
}
