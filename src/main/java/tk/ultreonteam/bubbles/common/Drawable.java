package tk.ultreonteam.bubbles.common;

import tk.ultreonteam.bubbles.render.Renderer;

public interface Drawable {
    void draw(Renderer g);
}
