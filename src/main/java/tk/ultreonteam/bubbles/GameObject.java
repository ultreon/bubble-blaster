package tk.ultreonteam.bubbles;

import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.vector.Vec2f;

public abstract class GameObject {
    private float x;
    private float y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    protected void setX(float x) {
        this.x = x;
    }

    protected void setY(float y) {
        this.y = y;
    }

    public Vec2f getPosition() {
        return new Vec2f(x, y);
    }

    public abstract void render(Renderer renderer);
}
