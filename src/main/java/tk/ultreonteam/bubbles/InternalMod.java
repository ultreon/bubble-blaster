package tk.ultreonteam.bubbles;

import tk.ultreonteam.bubbles.ingame.bubble.AbstractBubble;
import tk.ultreonteam.bubbles.mod.Mod;
import tk.ultreonteam.bubbles.mod.ModInstance;
import tk.ultreonteam.bubbles.ingame.EnvironmentRenderer;
import tk.ultreonteam.bubbles.event.CollectTexturesEvent;
import tk.ultreonteam.bubbles.event.SubscribeEvent;
import tk.ultreonteam.bubbles.event.bus.GameEvents;
import tk.ultreonteam.bubbles.event.bus.ModEvents;
import tk.ultreonteam.bubbles.event.load.ModSetupEvent;
import tk.ultreonteam.bubbles.init.*;
import tk.ultreonteam.bubbles.mod.loader.ModLoadingContext;
import tk.ultreonteam.bubbles.registry.Registers;
import tk.ultreonteam.bubbles.render.ITexture;
import tk.ultreonteam.bubbles.render.Renderer;
import tk.ultreonteam.bubbles.render.TextureCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tk.ultreonteam.bubbles.init.*;

import java.util.Collection;

/**
 * Internal mod, just used register stuff, and do some event handeling.
 *
 * @author Qboi
 */
@Mod(modId = InternalMod.MOD_ID)
public class InternalMod extends ModInstance {
    public static final String MOD_ID = BubbleBlaster.NAMESPACE;
    private static final Logger LOGGER = LogManager.getLogger("Bubble-Blaster");

    private final GameEvents gameEvents = GameEvents.get();

    public InternalMod() {
        super(LOGGER, MOD_ID, null);

        ModEvents eventBus = ModLoadingContext.get().getEvents();
        eventBus.<ModSetupEvent>subscribe(ModSetupEvent.class, this::setup);

        gameEvents.subscribe(this);
    }

    private void setup(ModSetupEvent event) {
        Bubbles.register(gameEvents);
        AmmoTypes.register(gameEvents);
        Entities.register(gameEvents);
        Effects.register(gameEvents);
        Abilities.register(gameEvents);
        GameplayEvents.register(gameEvents);
        Gamemodes.register(gameEvents);
        TextureCollections.register(gameEvents);
    }

    /**
     * Event handler for collecting textures.
     *
     * @param event the event we are subscribed to.
     */
    @SubscribeEvent
    public void onCollectTextures(CollectTexturesEvent event) {
        TextureCollection textureCollection = event.getTextureCollection();
        if (textureCollection == TextureCollections.BUBBLE_TEXTURES.get()) {
            Collection<AbstractBubble> bubbles = Registers.BUBBLES.values();
            LoadingGui loadingGui = LoadingGui.get();

            if (loadingGui == null) {
                throw new IllegalStateException("Load scene is not available.");
            }
            for (AbstractBubble bubble : bubbles) {
                for (int i = 0; i <= bubble.getMaxRadius(); i++) {
                    TextureCollection.Index identifier = new TextureCollection.Index(bubble.id().location(), bubble.id().path() + "/" + i);
                    final int finalI = i;
                    textureCollection.set(identifier, new ITexture() {
                        @Override
                        public void render(Renderer gg) {
                            EnvironmentRenderer.drawBubble(gg, 0, 0, finalI, bubble.colors);
                        }

                        @Override
                        public int width() {
                            return finalI + bubble.getColors().length * 2;
                        }

                        @Override
                        public int height() {
                            return finalI + bubble.getColors().length * 2;
                        }
                    });
                }
            }
        }
    }
}
